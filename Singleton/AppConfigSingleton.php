<?php

namespace IiMedias\AdminBundle\Singleton;

use IiMedias\AdminBundle\Model\Config;
use IiMedias\AdminBundle\Model\ConfigQuery;
use Symfony\Component\Config\Definition\Exception\Exception;

class AppConfigSingleton
{
    private static $_instance = null;

    protected $defaults;
    protected $values;
    protected $loadLater;

    protected $name;
    protected $description;
    protected $bundles;
    protected $baseUrl;
    protected $homeUrl;
    protected $mainMail;
    protected $openRegistration;
    protected $defaultUserRole;
    protected $timeZone;
    protected $dateFormat;
    protected $timeFormat;
    protected $weekBegin;
    protected $languages;
    protected $defaultLanguage;
    protected $adminColor;

    public function __construct()
    {
        $this->defaults = array(
            'name'        => 'iiMedias',
            'description' => 'Site fait par iiMedias',
            'bundles'     => json_encode(array('IiMediasAdminBundle')),
            'adminColor'  => 'grey',
        );
        $this->loadLater = true;
//        $values = ConfigQuery::create()
//            ->find();
//        $this->values = array();
//        foreach ($values as $value) {
//            $this->values[$value->getKey()] = $value->getValue();
//        }
    }

    public static function getInstance() {

        if(is_null(self::$_instance)) {
            self::$_instance = new AppConfigSingleton();
        }

        return self::$_instance;
    }

    protected function loadValues() {
        $this->loadLater = false;
        $values = ConfigQuery::create()
            ->find();
        $this->values = array();
        foreach ($values as $value) {
            $this->values[$value->getKey()] = $value->getValue();
        }
    }

    public function getFullConfig()
    {
        if ($this->loadLater == true) $this->loadValues();
        return $this->values;
    }

    public function getValue($key, $default = null)
    {
        if ($this->loadLater == true) $this->loadValues();
        $default = (!is_null($default) ? $default : (isset($this->defaults[$key]) ? $this->defaults[$key] : $default));
        if (!isset($this->values[$key])) {
            if (is_null($default)) {
                throw new Exception('La clé "' . $key . '" n\'a pas de valeur par défaut non nulle.');
            }
            $value = new Config();
            $value
                ->setKey($key)
                ->setValue($default)
                ->save();
            $this->values[$key] = $default;
        }
        return $this->values[$key];
    }

    public function setValue($key, $value)
    {
        if ($this->loadLater == true) $this->loadValues();
        $config = ConfigQuery::create()
            ->filterByKey($key)
            ->findOne();
        $config
            ->setValue($value)
            ->save();
        $this->values[$key] = $value;
        return $this->values[$key];
    }

    public function __get($property)
    {
        if ($this->loadLater == true) $this->loadValues();
        $return = $this->getValue($property);
        return $return;
    }

    public function __set($property, $value)
    {
        if ($this->loadLater == true) $this->loadValues();
        $return = $this->setValue($property, $value);
        return $return;
    }
    
    public function _call($method, $parameters)
    {
        if ($this->loadLater == true) $this->loadValues();
    }
}