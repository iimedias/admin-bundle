<?php

namespace IiMedias\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ii_medias_admin');
        
        $rootNode
            ->children()
                ->arrayNode('nodejs')
                    ->children()
                        ->arrayNode('functions')
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('name')->isRequired()->end()
                                    ->scalarNode('twig_template')->isRequired()->end()
                                    ->arrayNode('dependency_functions')
                                        ->prototype('scalar')->end()
                                    ->end()
                                    ->arrayNode('node_libs')
                                        ->prototype('array')
                                            ->children()
                                                ->scalarNode('name')->isRequired()->end()
                                                ->scalarNode('node_lib')->isRequired()->end()
                                                ->scalarNode('node_sub_lib')->defaultNull()->end()
                                                ->scalarNode('npm_download_module')->defaultNull()->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                    ->booleanNode('start_exec')->defaultFalse()->end()
                                    ->booleanNode('io_emit')->defaultFalse()->end()
                                    ->booleanNode('io_on')->defaultFalse()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
