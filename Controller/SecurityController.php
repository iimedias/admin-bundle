<?php

namespace IiMedias\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\DisabledException;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class SecurityController extends Controller {
    /**
     * Action de login
     *
     * @access public
     * @since 1.0.0 18/07/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request Objet Requête de Symfony
     * @param string $loginMode Mode de login
     * @Route("/login", name="login", defaults={"_locale"="fr", "loginMode"="classic"})
     * @Route("/{_locale}/login", name="iimedias_admin_security_login", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "loginMode"="classic"})
     * @Route("/login", name="iimedias_admin_security_login_noloc", defaults={"_locale"="fr", "loginMode"="classic"})
     * @Route("/admin/{_locale}/login", name="iimedias_admin_security_login_nolocadmin", defaults={"_locale"="fr", "loginMode"="admin"})
     * @Route("/admin/login", name="iimedias_admin_security_login_nolocadmin", defaults={"_locale"="fr", "loginMode"="admin"})
     * @Method({"GET","POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request, $loginMode) {
        $session = $request->getSession();

        if ($request->attributes->has(AuthenticationEvents::AUTHENTICATION_FAILURE)) {
            $error = $request->attributes->get(AuthenticationEvents::AUTHENTICATION_FAILURE);
        }
        elseif (!is_null($session) && $session->has(AuthenticationEvents::AUTHENTICATION_FAILURE)) {
            $error = $session->get(AuthenticationEvents::AUTHENTICATION_FAILURE);
            $session->remove(AuthenticationEvents::AUTHENTICATION_FAILURE);
        }
        else {
            $error = '';
        }

        if ($error instanceof BadCredentialsException) {
            dump('Il y a une erreur de correspondance login/mot de passe.');
            die();
        }
        elseif ($error instanceof DisabledException) {
            dump('Utilisateur désactivé');
            die();
        }
        elseif (is_string($error) && $error !== '') {
            dump($error);
            exit();
        }

        $lastUsername = is_null($session) ? '' : ''; // $session->get(SecurityContext::LAST_USERNAME);
//        $csrfToken    = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');

        return $this->render('IiMediasAdminBundle:Security:login.html.twig', array(
                'last_username' => $lastUsername,
                'error'         => $error,
                'loginMode'     => $loginMode,
//                'csrf_token'    => $csrfToken,
        ));
    }

    /**
     * Vérification de login
     *
     * @access public
     * @since 1.0.0 18/07/2016 Création -- sebii
     * @Route("/login/check", name="login_check", defaults={"_locale"="fr"})
     * @Route("/{_locale}/login/check", name="iimedias_admin_security_logincheck", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Route("/login/check", name="iimedias_admin_security_logincheck_noloc", defaults={"_locale"="fr"})
     * @Route("/admin/login/check", name="iimedias_admin_security_logincheck_nolocadmin", defaults={"_locale"="fr"})
     * @Method({"GET","POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function loginCheck()
    {
        throw new RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }


    /**
     * Echec de login
     *
     * @access public
     * @since 1.0.0 18/07/2016 Création -- sebii
     * @Route("/login/fail", name="login_fail", defaults={"_locale"="fr"})
     * @Route("/{_locale}/login/fail", name="iimedias_admin_security_loginfail", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Route("/login/fail", name="iimedias_admin_security_loginfail_noloc", defaults={"_locale"="fr"})
     * @Route("/admin/login/fail", name="iimedias_admin_security_loginfail_nolocadmin", defaults={"_locale"="fr"})
     * @Method({"GET","POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function loginFail()
    {
        throw new RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    /**
     * Déconnection
     *
     * @access public
     * @since 1.0.0 14/04/2017 Création -- sebii
     * @Route("/logout", name="logout", defaults={"_locale"="fr"})
     * @Route("/{_locale}/logout", name="iimedias_admin_security_logout", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
      @return Symfony\Component\HttpFoundation\Response
     */
    public function logout()
    {
        return $this->redirect($this->generateUrl('admin'));
    }
}
