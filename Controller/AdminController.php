<?php

namespace IiMedias\AdminBundle\Controller;

use IiMedias\AdminBundle\Model\RoleQuery;
use IiMedias\AdminBundle\Singleton\AppConfigSingleton;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use IiMedias\AdminBundle\Form\Type\OptionsType;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AdminController extends Controller
{
    /**
     * Index / Dashboard de l'administration
     *
     * @access public
     * @since 1.0.0 18/07/2016 Création -- sebii
     * @Route("/admin/{_locale}", name="iimedias_admin_admin_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('IiMediasAdminBundle:Admin:index.html.twig', array(
            'currentNav' => 'dashboard',
        ));
    }

    /**
     * Index / Dashboard de l'administration
     *
     * @access public
     * @since 1.0.0 18/07/2016 Création -- sebii
     * @Route("/admin/{_locale}/options/main", name="iimedias_admin_admin_optionsmain", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function optionsMain(Request $request)
    {
        $appConfig = AppConfigSingleton::getInstance();
        $form = $this->createForm(OptionsType::class, $appConfig);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
        }

        return $this->render('IiMediasAdminBundle:Admin:optionsMain.html.twig', array(
            'currentNav' => 'options',
            'form'       => $form->createView(),
        ));
    }

    /**
     * Liste des utilisateurs
     *
     * @access public
     * @since 1.0.0 12/04/2017 Création -- sebii
     * @Route("/admin/{_locale}/users", name="iimedias_admin_admin_users", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function users()
    {
        $users = UserQuery::create()
            ->find();

        return $this->render('IiMediasAdminBundle:Admin:users.html.twig', array(
            'currentNav' => 'users',
            'users'      => $users,
        ));
    }

    /**
     *
     * @access public
     * @since 1.0.0 12/04/2017 Création -- sebii
     * @Route("/admin/{_locale}/user/{userId}/roles", name="iimedias_admin_admin_userroles", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @ParamConverter("user", class="IiMedias\AdminBundle\Model\User", options={"mapping"={"userId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function userRoles(Request $request, User $user)
    {
        $rolesData    = array();
        $fields       = array();
        $fieldsGroups = array();
        $roles        = RoleQuery::create()
            ->orderById()
            ->find();
        foreach ($roles as $role) {
            $rolesData[$role->getKey()] = false;
            $fields[$role->getKey()]    = $role->getLabel();
            if (!isset($fieldsGroups[$role->getRoleModuleId()])) {
                $fieldsGroups[$role->getRoleModuleId()] = array(
                    'label'  => $role->getRoleModule()->getLabel(),
                    'fields' => array(),
                );
            }
            $fieldsGroups[$role->getRoleModuleId()]['fields'][$role->getKey()] = $role->getLabel();
        }

        foreach ($this->getUser()->getRoles() as $userRole) {
            if ($userRole != 'ROLE_USER') {
                $rolesData[$userRole] = true;
            }
        }

        $form = $this->createFormBuilder($rolesData, array('method' => 'post'));
        foreach ($fields as $roleKey => $roleLabel) {
            $form = $form->add($roleKey, CheckboxType::class, array('required' => false));
        }
        $form = $form->add('submit', SubmitType::class);
        $form = $form->getForm();

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            $rolesData = $form->getData();
            $newRoles  = array('ROLE_USER');
            foreach ($rolesData as $roleKey => $roleActive) {
                if ($roleKey == 'ROLE_ROOT' && $roleActive == true) {
                    $newRoles[] = $roleKey;
                    foreach ($roles as $role) {
                        if (!in_array($role->getKey(), $newRoles)) {
                            $newRoles[] = $role->getKey();
                        }
                    }
                }
                elseif ($roleActive == true && !in_array($roleKey, $newRoles)) {
                    $newRoles[] = $roleKey;
                }
            }
            $user->setRoles($newRoles)->save();
            return $this->redirect($this->generateUrl('iimedias_admin_admin_users'));
        }

        return $this->render('IiMediasAdminBundle:Admin:userRoles.html.twig', array(
            'currentNav'   => 'users',
            'user'         => $user,
            'form'         => $form->createView(),
            'fields'       => $fields,
            'fieldsGroups' => $fieldsGroups,
        ));
    }
}
