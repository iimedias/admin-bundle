<?php

namespace IiMedias\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use IiMedias\AdminBundle\Model\MenuGroup;
use IiMedias\AdminBundle\Model\MenuGroupQuery;
use IiMedias\AdminBundle\Form\Type\MenuGroupType;
use IiMedias\AdminBundle\Model\MenuElement;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use IiMedias\AdminBundle\Form\Type\MenuElementType;

/**
 * Class MenuController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class MenuController extends Controller
{
    /**
     * Index / Liste des groupes de menus
     *
     * @access public
     * @since 1.0.0 14/10/2016 Création -- sebii
     * @Route("/admin/{_locale}/menus", name="iimedias_admin_menu_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $menuGroups = MenuGroupQuery::getAll();

        return $this->render(
            'IiMediasAdminBundle:Menu:index.html.twig',
            array(
                'currentNav' => 'menus',
                'menuGroups' => $menuGroups,
            )
        );
    }

    /**
     * Composition d'un groupe de menu
     *
     * @access public
     * @since 1.0.0 16/07/2016 Création -- sebii
     * @param IiMedias\AdminBundle\Model\MenuGroup $menuGroup
     * @Route("/admin/{_locale}/menu/{menuId}", name="iimedias_admin_menu_menu", requirements={"_locale"="\w{2}", "menuId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("menuGroup", class="IiMedias\AdminBundle\Model\MenuGroup", options={"mapping"={"menuId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function menu(MenuGroup $menuGroup)
    {
        return $this->render(
            'IiMediasAdminBundle:Menu:menu.html.twig',
            array(
                'currentNav' => 'menus',
                'menuGroup'  => $menuGroup,
            )
        );
    }

    /**
     * Formulaire d'un groupe de menu
     *
     * @access public
     * @since 1.0.0 27/10/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param IiMedias\AdminBundle\Model\MenuGroup $menuGroup
     * @Route("/admin/{_locale}/menu/{menuId}/organize", name="iimedias_admin_menu_organize", requirements={"_locale"="\w{2}", "menuId"="\d+"}, defaults={"_locale"="fr", "mode"="edit"})
     * @ParamConverter("menuGroup", class="IiMedias\AdminBundle\Model\MenuGroup", options={"mapping"={"menuId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function menuOrganize(Request $request, MenuGroup $menuGroup)
    {
        if ($request->isMethod('post')) {
            $menuOrganizeElements = $request->request->get('menu_organize');
            foreach ($menuOrganizeElements as $elementPosition0 => $elementCode) {
                $menuElement = MenuElementQuery::getOneByMenuGroupAndCode($menuGroup, $elementCode);
                $menuElement
                    ->setPosition($elementPosition0 + 1)
                    ->save()
                ;
            }
            return $this->redirect(
                $this->generateUrl(
                    'iimedias_admin_menu_menu',
                    array(
                        'menuId' => $menuGroup->getId(),
                    )
                )
            );
        }

        return $this->render(
            'IiMediasAdminBundle:Menu:menuOrganize.html.twig',
            array(
                'currentNav' => 'menus',
                'menuGroup'  => $menuGroup,
            )
        );
    }

    /**
     * Vue d'un élément
     *
     * @access public
     * @since 1.0.0 21/10/2016 Création -- sebii
     * @param IiMedias\AdminBundle\Model\MenuGroup $menuGroup
     * @param IiMedias\AdminBundle\Model\MenuElement $menuElement
     * @Route("/admin/{_locale}/menu/{menuId}/{elementId}", name="iimedias_admin_menu_element", requirements={"_locale"="\w{2}", "menuId"="\d+", "elementId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("menuGroup", class="IiMedias\AdminBundle\Model\MenuGroup", options={"mapping"={"menuId": "id"}})
     * @ParamConverter("menuElement", class="IiMedias\AdminBundle\Model\MenuElement", options={"mapping"={"elementId": "id", "menuId": "menuGroupId"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function element(MenuGroup $menuGroup, MenuElement $menuElement)
    {
        return $this->render(
            'IiMediasAdminBundle:Menu:element.html.twig',
            array(
                'currentNav'  => 'menus',
                'menuGroup'   => $menuGroup,
                'menuElement' => $menuElement,
            )
        );
    }

    /**
     * Formulaire d'un groupe de menu
     *
     * @access public
     * @since 1.0.0 16/07/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param string $mode
     * @Route("/admin/{_locale}/menu/create", name="iimedias_admin_menu_create", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "mode"="create"})
     * @Route("/admin/{_locale}/menu/{menuId}/edit", name="iimedias_admin_menu_edit", requirements={"_locale"="\w{2}", "menuId"="\d+"}, defaults={"_locale"="fr", "mode"="edit"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function formMenu(Request $request, $mode)
    {
        switch ($mode) {
            case 'edit':
                $menuGroup = MenuGroupQuery::getOneById($request->get('menuId'));
                break;
            case 'create':
            default:
                $menuGroup = new MenuGroup();
                break;
        }

        $form = $this->createForm(
            MenuGroupType::class,
            $menuGroup,
            array(
                'action' => $request->getUri(),
            )
        );

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            $menuGroup->save();
            return $this->redirect(
                $this->generateUrl(
                    'iimedias_admin_menu_menu',
                    array(
                        'menuId' => $menuGroup->getId(),
                    )
                )
            );
        }

        return $this->render(
            'IiMediasAdminBundle:Menu:formMenu.html.twig',
            array(
                'currentNav' => 'menus',
                'mode'       => $mode,
                'menuGroup'  => $menuGroup,
                'form'       => $form->createView(),
            )
        );
    }

    /**
     * Formulaire d'un élément de menu
     *
     * @access public
     * @since 1.0.0 16/07/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param IiMedias\AdminBundle\Model\MenuGroup $menuGroup
     * @param string $mode
     * @Route("/admin/{_locale}/menu/{menuId}/create", name="iimedias_admin_menu_elementcreate", requirements={"_locale"="\w{2}", "menuId"="\d+"}, defaults={"_locale"="fr", "mode"="create"})
     * @Route("/admin/{_locale}/menu/{menuId}/{elementId}/edit", name="iimedias_admin_menu_elementedit", requirements={"_locale"="\w{2}", "menuId"="\d+", "elementId"="\d+"}, defaults={"_locale"="fr", "mode"="edit"})
     * @ParamConverter("menuGroup", class="IiMedias\AdminBundle\Model\MenuGroup", options={"mapping"={"menuId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function formElement(Request $request, MenuGroup $menuGroup, $mode)
    {
        switch ($mode) {
            case 'edit':
                $menuElement = MenuElementQuery::getOneById($request->get('elementId'));
                break;
            case 'create':
            default:
                $menuElement = new MenuElement();
                $menuElement
                    ->setMenuGroup($menuGroup)
                ;
        }

        $form = $this->createForm(
            MenuElementType::class,
            $menuElement,
            array(
                'action' => $request->getUri(),
            )
        );

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            $menuElement
                ->save();
            return $this->redirect(
                $this->generateUrl(
                    'iimedias_admin_menu_element',
                    array(
                        'menuId'    => $menuGroup->getId(),
                        'elementId' => $menuElement->getId(),
                    )
                )
            );
        }

        return $this->render(
            'IiMediasAdminBundle:Menu:formElement.html.twig',
            array(
                'currentNav'  => 'menus',
                'mode'        => $mode,
                'menuGroup'   => $menuGroup,
                'menuElement' => $menuElement,
                'form'        => $form->createView(),
            )
        );
    }

    /**
     * Monter l'élément d'un cran
     *
     * @access public
     * @since 1.0.0 16/07/2016 Création -- sebii
     * @param IiMedias\AdminBundle\Model\MenuElement $menuElement
     * @Route("/admin/{_locale}/menu/{menuId}/{elementId}/up", name="iimedias_admin_menu_up", requirements={"_locale"="\w{2}", "menuId"="\d+", "elementId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("menuElement", class="IiMedias\AdminBundle\Model\MenuElement", options={"mapping"={"elementId": "id", "menuId": "menuGroupId"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function up(MenuElement $menuElement)
    {
        $otherMenuElement = MenuElementQuery::getOneByMenuGroupAndPosition($menuElement->getMenuGroup(), $menuElement->getPosition() - 1);
        if (!is_null($otherMenuElement)) {
            $menuElement->positionUp();
            $otherMenuElement->positionDown();
        }

        return $this->redirect(
            $this->generateUrl(
                'iimedias_admin_menu_menu',
                array(
                    'menuId' => $menuElement->getMenuGroupId(),
                )
            )
        );
    }

    /**
     * Descendre l'élément d'un cran
     *
     * @access public
     * @since 1.0.0 16/07/2016 Création -- sebii
     * @param IiMedias\AdminBundle\Model\MenuElement $menuElement
     * @Route("/admin/{_locale}/menu/{menuId}/{elementId}/down", name="iimedias_admin_menu_down", requirements={"_locale"="\w{2}", "menuId"="\d+", "elementId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("menuElement", class="IiMedias\AdminBundle\Model\MenuElement", options={"mapping"={"elementId": "id", "menuId": "menuGroupId"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function down(MenuElement $menuElement)
    {
        $otherMenuElement = MenuElementQuery::getOneByMenuGroupAndPosition($menuElement->getMenuGroup(), $menuElement->getPosition() + 1);
        if (!is_null($otherMenuElement)) {
            $menuElement->positionDown();
            $otherMenuElement->positionUp();
        }

        return $this->redirect(
            $this->generateUrl(
                'iimedias_admin_menu_menu',
                array(
                    'menuId' => $menuElement->getMenuGroupId(),
                )
            )
        );
    }
}
