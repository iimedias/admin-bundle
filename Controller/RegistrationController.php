<?php

namespace IiMedias\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Form\Type\UserType;

/**
 * Class RegistrationController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class RegistrationController extends Controller
{
    /**
     * Index / Dashboard de l'administration
     *
     * @access public
     * @since 1.0.0 20/07/2016 Création -- sebii
     * @param Request $request
     * @Route("/{_locale}/register", name="iimedias_admin_registration_register", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Route("/register", name="iimedias_admin_registration_register_noloc", defaults={"_locale"="fr"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->generateSalt();
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user
                ->setPassword($password)
                ->save()
            ;
        }
        return $this->render('IiMediasAdminBundle:Registration:register.html.twig', array(
                'form' => $form->createView(),
        ));
    }
}
