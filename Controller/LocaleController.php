<?php

namespace IiMedias\AdminBundle\Controller;

use IiMedias\AdminBundle\Model\LocaleQuery;
use IiMedias\AdminBundle\Model\MessageLocale;
use IiMedias\AdminBundle\Model\MessageLocaleQuery;
use IiMedias\AdminBundle\Model\MessageQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LocaleController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class LocaleController extends Controller
{
    /**
     * Index / Liste des groupes de menus
     *
     * @access public
     * @since 1.0.0 14/10/2016 Création -- sebii
     * @Route("/admin/{_locale}/locales", name="iimedias_admin_locale_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $locales         = LocaleQuery::getAll();
        $messages        = MessageQuery::getAll();
        $messagesLocales = MessageLocaleQuery::getAll();

        $translationTemplateArray = array();
        foreach ($locales as $locale) {
            $translationTemplateArray[$locale->getId()] = array(
                'id'          => 0,
                'localeId'    => $locale->getId(),
                'localeCode'  => $locale->getCode(),
                'translation' => '',
            );
        }

        $messagesData = array();
        foreach ($messages as $message) {
            $messagesData[$message->getId()] = array(
                'id'           => $message->getId(),
                'code'         => $message->getCode(),
                'translations' => $translationTemplateArray,
            );
        }

        foreach ($messagesLocales as $messageLocale) {
            $messagesData[$messageLocale->getMessageId()]['translations'][$messageLocale->getLocaleId()]['id'] = $messageLocale->getId();
            $messagesData[$messageLocale->getMessageId()]['translations'][$messageLocale->getLocaleId()]['translation'] = $messageLocale->getLabel();
        }

        return $this->render(
            'IiMediasAdminBundle:Locale:index.html.twig',
            array(
                'currentNav'   => 'locales',
                'locales'      => $locales,
                'messagesData' => $messagesData,
            )
        );
    }

    /**
     * Enregistrement d'une traduction
     *
     * @access public
     * @since 1.0.0 16/11/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request
     * @Route("/admin/{_locale}/locale/save", name="iimedias_admin_locale_save", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function save(Request $request)
    {
        $data          = $request->request->all();
        $translationId = intval($data['id']);
        switch ($translationId) {
            case 0:
                $translation = new MessageLocale();
                $translation
                    ->setMessageId($data['messageId'])
                    ->setLocaleId($data['localeId'])
                ;
                break;
            default:
                $translation = MessageLocaleQuery::getOneById($translationId);
        }
        $translation
            ->setLabel($data['translation'])
            ->save()
        ;

        $responseData = array(
            'id'          => $translation->getId(),
            'translation' => $translation->getLabel(),
            'messageId'   => $translation->getMessageId(),
            'localeId'    => $translation->getLocaleId(),
        );

        return new JsonResponse($responseData);
    }
}
