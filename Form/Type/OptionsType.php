<?php

namespace IiMedias\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class OptionsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                    'required' => true,
                    'label'    => 'Nom du site',
                    'attr'     => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Nom du site',
                    ),
            ))
            ->add('description', TextareaType::class, array(
                    'required' => true,
                    'label'    => 'Description du site',
                    'attr'     => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Description du site',
                    ),
            ))
            ->add('adminColor', ChoiceType::class, array(
                    'required' => true,
                    'expanded' => true,
                    'multiple' => false,
                    'choices'  => array(
                        'Gris'          => 'grey',
                        'Rouge'         => 'red',
                        'Rose'          => 'pink',
                        'Violet'        => 'purple',
                        'Violet Sombre' => 'deep-purple',
                        'Indigo'        => 'indigo',
                        'Bleu'          => 'blue',
                        'Bleu Clair'    => 'light-blue',
                        'Cyan'          => 'cyan',
                        'Vert Canard'   => 'teal',
                        'Vert'          => 'green',
                        'Vert Clair'    => 'light-green',
                        'Vert Citron'   => 'lime',
                        'Jaune'         => 'yellow',
                        'Jaune Ambre'   => 'amber',
                        'Orange'        => 'orange',
                        'Orange Sombre' => 'deep-orange',
                        'Marron'        => 'brown',
                        'Bleu Gris'     => 'blue-grey',
                    ),
            ))
            ->add('submit_main', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary',
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'IiMedias\AdminBundle\Singleton\AppConfigSingleton',
            'name'       => 'options',
        ]);
    }
}
