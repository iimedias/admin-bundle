<?php

namespace IiMedias\AdminBundle\Service;

use IiMedias\AdminBundle\Model\MenuGroupQuery;
use IiMedias\AdminBundle\Singleton\AppConfigSingleton;

class AppConfigService
{
    protected $_appConfig = null;

    public function __construct()
    {
        $this->_appConfig = AppConfigSingleton::getInstance();
    }

    public function getFullConfig()
    {
        return $this->_appConfig->getFullConfig();
    }

    public function __call($method, $arguments)
    {
        $return = $this->_appConfig->getValue($method);
        return $return;
    }

    public function __get($property)
    {
        $return = $this->_appConfig->getValue($property);
        return $return;
    }

    public function __set($property, $value)
    {
        $return = $this->_appConfig->setValue($property, $value);
        return $return;
    }

    public function getMenuConfig($menuName)
    {
        $menuElements = MenuGroupQuery::getElementsByMenuGroupName($menuName);

        return $menuElements;
    }
//
//    public function getName()
//    {
//        return $this->_appConfig->getName();
//    }
//
//    public function setName($name)
//    {
//        return $this->_appConfig->setName($name);
//    }
//
//    public function getDescription()
//    {
//        return $this->_appConfig->getDescription();
//    }
//
//    public function setDescription($description)
//    {
//        return $this->_appConfig->setDescription($description);
//    }
//
//    public function getAdminColor()
//    {
//        return $this->_appConfig->getAdminColor();
//    }
//
//    public function setAdminColor($adminColor)
//    {
//        return $this->_appConfig->setAdminColor($adminColor);
//    }
}