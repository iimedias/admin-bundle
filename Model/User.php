<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\User as BaseUser;
use Symfony\Component\Security\Core\User\UserInterface;
use \Serializable;

/**
 * Skeleton subclass for representing a row from the 'user_user_usrusr' table.
 *
 * @package IiMedias\AdminBundle\Model
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 * @see http://symfony.com/doc/current/cookbook/security/entity_provider.html Documentation sur le site officiel de Symfony
 */
class User extends BaseUser implements UserInterface, Serializable
{
    protected $plainPassword;
    protected $termsAccepted;

    /**
     * Classe de construction de l'utilisateur
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->setIsActive(true);
        $this->setRoles(array('ROLE_USER'));
    }

    /**
     * Génère un grain de sel pour le mot de passe
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @return IiMedias\AdminBundle\Model\User
     */
    public function generateSalt()
    {
        $this->setSalt(base_convert(sha1(uniqid(mt_rand(), true)), 16, 36));
        return $this;
    }

    /**
     * Efface les informations d'identification
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @todo Coder cette fonction
     * @return void
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Définit le Mot de Passe en clair
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @param string $plainPassword Mot de passe en clair
     * @return IiMedias\AdminBundle\Model\User
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * Récupère le Mot de Passe en clair
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Définit les Termes acceptés
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @param boolean $termsAccepted Si oui ou non les termes ont été acceptés
     * @return IiMedias\AdminBundle\Model\User
     */
    public function setTermsAccepted($termsAccepted)
    {
        $this->termAccepted;
        return $this;
    }

    /**
     * Récupère les termes acceptés
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @return boolean
     */
    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    /**
     * Sérialize les données de l'objet Utilisateur
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @return string
     * @see Serializable::serialize()
     */
    public function serialize()
    {
        $serializedData = serialize(array(
                $this->getPassword(),
                $this->getSalt(),
                null,//$this->getUsernameCanonical(),
                $this->getUsername(),
                null,//$this->getExpired(),
                null,//$this->getLocked(),
                null,//$this->getCredentialsExpired(),
                null,//$this->getEnabled(),
                $this->getId(),
        ));
        return $serializedData;
    }

    /**
     * Désérialize les données de l'objet Utilisateur
     * 
     * @since 1.0.0 22/07/2016 Création -- sebii
     * @access public
     * @return IiMedias\AdminBundle\Model\User
     * @see Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        $data  = unserialize($serialized);
        $data  = array_merge($data, array_fill(0, 2, null));
        list(
            $password,
            $salt,
            $usernameCanonical,
            $username,
            $expired,
            $locked,
            $credentialExpired,
            $enabled,
            $id
        ) = $data;
        $this
            ->setId($id)
            ->setUsername($username)
            ->setSalt($salt)
            ->setPassword($password)
        ;
    }

    public function __toString()
    {
        return $this->getUsername();
    }
}