<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\ConfigQuery as BaseConfigQuery;
use Zend_Cache;

/**
 * Skeleton subclass for performing query and update operations on the 'admin_config_config' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ConfigQuery extends BaseConfigQuery
{
//    public function cacheContains($key)
//    {
//        return $this->getCacheBackend()->test($key);
//    }
//
//    public function cacheFetch($key)
//    {
//        return $this->getCacheBackend()->load($key);
//    }
//
//    public function cacheStore($key, $value)
//    {
//        return $this->getCacheBackend()->save($key, $value);
//    }
//
//    protected function getCacheBackend()
//    {
//        if (self::$cacheBackend === null) {
//            $frontendOptions = array(
//                'lifetime' => 7200,
//                'automatic_serialization' => true
//            );
//            $backendOptions = array(
//                'servers' => array(
//                    array(
//                        'host' => 'localhost',
//                        'port' => 11211,
//                        'persistent' => true
//                    )
//                )
//            );
//            self::$cacheBackend = Zend_Cache::factory('Core', 'Memcached', $frontendOptions, $backendOptions);
//        }
//
//        return self::$cacheBackend;
//    }
}
