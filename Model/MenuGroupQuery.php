<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\MenuGroupQuery as BaseMenuGroupQuery;
use IiMedias\AdminBundle\Model\MenuGroup;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'menu_group_mengrp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MenuGroupQuery extends BaseMenuGroupQuery
{
    public static function getAll()
    {
        $menuGroups = self::create()
//            ->setQueryKey('MenuGroupGetAll')
            ->orderByName()
            ->find()
        ;

        return $menuGroups;
    }

    public static function getOneById($menuGroupId)
    {
        $menuGroup = self::create()
//            ->setQueryKey('MenuGroupGetOneById')
            ->filterById($menuGroupId)
            ->findOne()
        ;

        return $menuGroup;
    }

    public static function getByNameOrCreate($name)
    {
        $menuGroup = self::create()
//            ->setQueryKey('MenuGroupGetByName')
            ->filterByName($name)
            ->findOne()
        ;

        if (is_null($menuGroup)) {
            $menuGroup = new MenuGroup();
            $menuGroup
                ->setName($name)
                ->save()
            ;
        }

        return $menuGroup;
    }

    public static function countElementsByMenuGroup($menuGroup)
    {
        $menuGroupCount = MenuElementQuery::create()
//            ->setQueryKey('MenuElementCountElementsByMenuGroup')
            ->filterByMenuGroup($menuGroup)
            ->find()
            ->count()
        ;
        return $menuGroupCount;
    }

    public static function getElementsByMenuGroupName($menuGroupName)
    {
        $menuElements = MenuElementQuery::create()
//            ->setQueryKey('MenuElementGetElementsByMenuGroupName')
            ->useMenuGroupQuery()
                ->filterByName($menuGroupName)
            ->endUse()
            ->orderByPosition(Criteria::ASC)
            ->find()
        ;

        return $menuElements;
    }
}
