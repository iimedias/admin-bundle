<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\MenuElementQuery as BaseMenuElementQuery;
use IiMedias\AdminBundle\Model\MenuGroup;
use IiMedias\AdminBundle\Model\MenuGroupQuery;
use IiMedias\AdminBundle\Model\MenuElement;

/**
 * Skeleton subclass for performing query and update operations on the 'menu_element_menelm' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MenuElementQuery extends BaseMenuElementQuery
{
    public static function getOneById($menuGroupId)
    {
        $menuElement = self::create()
            ->filterById($menuGroupId)
            ->findOne()
        ;

        return $menuElement;
    }

    public static function addElement($menuName, $elementCode, $elementName, $elementIcon, $elementRouteName, $elementRouteParams)
    {
        $menuGroup   = MenuGroupQuery::getByNameOrCreate($menuName);
        $menuElement = self::create()
            ->filterByMenuGroup($menuGroup)
            ->filterByCode($elementCode)
            ->findOne()
        ;
        
        if (is_null($menuElement)) {
            $menuNewPosition = MenuGroupQuery::countElementsByMenuGroup($menuGroup) + 1;
            $menuElement     = new MenuElement();
            $menuElement
                ->setMenuGroup($menuGroup)
                ->setCode($elementCode)
                ->setPosition($menuNewPosition)
                ->setMessage($elementName)
                ->setIconFontAwesome($elementIcon)
                ->setRouteName($elementRouteName)
                ->setRouteParams($elementRouteParams)
                ->save()
            ;
        }

        return $menuElement;
    }

    public static function getOneByMenuGroupAndPosition(MenuGroup $menuGroup, $position)
    {
        $menuElement = self::create()
            ->filterByMenuGroup($menuGroup)
            ->filterbyPosition($position)
            ->findOne()
        ;
        
        return $menuElement;
    }

    public static function getOneByMenuGroupAndCode(MenuGroup $menuGroup, $code)
    {
        $menuElement = self::create()
            ->filterByMenuGroup($menuGroup)
            ->filterByCode($code)
            ->findOne()
        ;

        return $menuElement;
    }
}
