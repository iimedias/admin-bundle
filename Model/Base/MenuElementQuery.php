<?php

namespace IiMedias\AdminBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\MenuElement as ChildMenuElement;
use IiMedias\AdminBundle\Model\MenuElementQuery as ChildMenuElementQuery;
use IiMedias\AdminBundle\Model\Map\MenuElementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'menu_element_menelm' table.
 *
 *
 *
 * @method     ChildMenuElementQuery orderById($order = Criteria::ASC) Order by the menelm_id column
 * @method     ChildMenuElementQuery orderByMenuGroupId($order = Criteria::ASC) Order by the menelm_mengrp_id column
 * @method     ChildMenuElementQuery orderByCode($order = Criteria::ASC) Order by the menelm_code column
 * @method     ChildMenuElementQuery orderByPosition($order = Criteria::ASC) Order by the menelm_position column
 * @method     ChildMenuElementQuery orderByMessage($order = Criteria::ASC) Order by the menelm_message column
 * @method     ChildMenuElementQuery orderByIconFontAwesome($order = Criteria::ASC) Order by the menelm_icon_fa column
 * @method     ChildMenuElementQuery orderByRouteName($order = Criteria::ASC) Order by the menelm_route_name column
 * @method     ChildMenuElementQuery orderByRouteParams($order = Criteria::ASC) Order by the menelm_route_params column
 * @method     ChildMenuElementQuery orderByRole($order = Criteria::ASC) Order by the menelm_role column
 * @method     ChildMenuElementQuery orderByCreatedAt($order = Criteria::ASC) Order by the menelm_created_at column
 * @method     ChildMenuElementQuery orderByUpdatedAt($order = Criteria::ASC) Order by the menelm_updated_at column
 *
 * @method     ChildMenuElementQuery groupById() Group by the menelm_id column
 * @method     ChildMenuElementQuery groupByMenuGroupId() Group by the menelm_mengrp_id column
 * @method     ChildMenuElementQuery groupByCode() Group by the menelm_code column
 * @method     ChildMenuElementQuery groupByPosition() Group by the menelm_position column
 * @method     ChildMenuElementQuery groupByMessage() Group by the menelm_message column
 * @method     ChildMenuElementQuery groupByIconFontAwesome() Group by the menelm_icon_fa column
 * @method     ChildMenuElementQuery groupByRouteName() Group by the menelm_route_name column
 * @method     ChildMenuElementQuery groupByRouteParams() Group by the menelm_route_params column
 * @method     ChildMenuElementQuery groupByRole() Group by the menelm_role column
 * @method     ChildMenuElementQuery groupByCreatedAt() Group by the menelm_created_at column
 * @method     ChildMenuElementQuery groupByUpdatedAt() Group by the menelm_updated_at column
 *
 * @method     ChildMenuElementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMenuElementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMenuElementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMenuElementQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMenuElementQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMenuElementQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMenuElementQuery leftJoinMenuGroup($relationAlias = null) Adds a LEFT JOIN clause to the query using the MenuGroup relation
 * @method     ChildMenuElementQuery rightJoinMenuGroup($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MenuGroup relation
 * @method     ChildMenuElementQuery innerJoinMenuGroup($relationAlias = null) Adds a INNER JOIN clause to the query using the MenuGroup relation
 *
 * @method     ChildMenuElementQuery joinWithMenuGroup($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MenuGroup relation
 *
 * @method     ChildMenuElementQuery leftJoinWithMenuGroup() Adds a LEFT JOIN clause and with to the query using the MenuGroup relation
 * @method     ChildMenuElementQuery rightJoinWithMenuGroup() Adds a RIGHT JOIN clause and with to the query using the MenuGroup relation
 * @method     ChildMenuElementQuery innerJoinWithMenuGroup() Adds a INNER JOIN clause and with to the query using the MenuGroup relation
 *
 * @method     \IiMedias\AdminBundle\Model\MenuGroupQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMenuElement findOne(ConnectionInterface $con = null) Return the first ChildMenuElement matching the query
 * @method     ChildMenuElement findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMenuElement matching the query, or a new ChildMenuElement object populated from the query conditions when no match is found
 *
 * @method     ChildMenuElement findOneById(int $menelm_id) Return the first ChildMenuElement filtered by the menelm_id column
 * @method     ChildMenuElement findOneByMenuGroupId(int $menelm_mengrp_id) Return the first ChildMenuElement filtered by the menelm_mengrp_id column
 * @method     ChildMenuElement findOneByCode(string $menelm_code) Return the first ChildMenuElement filtered by the menelm_code column
 * @method     ChildMenuElement findOneByPosition(int $menelm_position) Return the first ChildMenuElement filtered by the menelm_position column
 * @method     ChildMenuElement findOneByMessage(string $menelm_message) Return the first ChildMenuElement filtered by the menelm_message column
 * @method     ChildMenuElement findOneByIconFontAwesome(string $menelm_icon_fa) Return the first ChildMenuElement filtered by the menelm_icon_fa column
 * @method     ChildMenuElement findOneByRouteName(string $menelm_route_name) Return the first ChildMenuElement filtered by the menelm_route_name column
 * @method     ChildMenuElement findOneByRouteParams(string $menelm_route_params) Return the first ChildMenuElement filtered by the menelm_route_params column
 * @method     ChildMenuElement findOneByRole(string $menelm_role) Return the first ChildMenuElement filtered by the menelm_role column
 * @method     ChildMenuElement findOneByCreatedAt(string $menelm_created_at) Return the first ChildMenuElement filtered by the menelm_created_at column
 * @method     ChildMenuElement findOneByUpdatedAt(string $menelm_updated_at) Return the first ChildMenuElement filtered by the menelm_updated_at column *

 * @method     ChildMenuElement requirePk($key, ConnectionInterface $con = null) Return the ChildMenuElement by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOne(ConnectionInterface $con = null) Return the first ChildMenuElement matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMenuElement requireOneById(int $menelm_id) Return the first ChildMenuElement filtered by the menelm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByMenuGroupId(int $menelm_mengrp_id) Return the first ChildMenuElement filtered by the menelm_mengrp_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByCode(string $menelm_code) Return the first ChildMenuElement filtered by the menelm_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByPosition(int $menelm_position) Return the first ChildMenuElement filtered by the menelm_position column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByMessage(string $menelm_message) Return the first ChildMenuElement filtered by the menelm_message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByIconFontAwesome(string $menelm_icon_fa) Return the first ChildMenuElement filtered by the menelm_icon_fa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByRouteName(string $menelm_route_name) Return the first ChildMenuElement filtered by the menelm_route_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByRouteParams(string $menelm_route_params) Return the first ChildMenuElement filtered by the menelm_route_params column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByRole(string $menelm_role) Return the first ChildMenuElement filtered by the menelm_role column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByCreatedAt(string $menelm_created_at) Return the first ChildMenuElement filtered by the menelm_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuElement requireOneByUpdatedAt(string $menelm_updated_at) Return the first ChildMenuElement filtered by the menelm_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMenuElement[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMenuElement objects based on current ModelCriteria
 * @method     ChildMenuElement[]|ObjectCollection findById(int $menelm_id) Return ChildMenuElement objects filtered by the menelm_id column
 * @method     ChildMenuElement[]|ObjectCollection findByMenuGroupId(int $menelm_mengrp_id) Return ChildMenuElement objects filtered by the menelm_mengrp_id column
 * @method     ChildMenuElement[]|ObjectCollection findByCode(string $menelm_code) Return ChildMenuElement objects filtered by the menelm_code column
 * @method     ChildMenuElement[]|ObjectCollection findByPosition(int $menelm_position) Return ChildMenuElement objects filtered by the menelm_position column
 * @method     ChildMenuElement[]|ObjectCollection findByMessage(string $menelm_message) Return ChildMenuElement objects filtered by the menelm_message column
 * @method     ChildMenuElement[]|ObjectCollection findByIconFontAwesome(string $menelm_icon_fa) Return ChildMenuElement objects filtered by the menelm_icon_fa column
 * @method     ChildMenuElement[]|ObjectCollection findByRouteName(string $menelm_route_name) Return ChildMenuElement objects filtered by the menelm_route_name column
 * @method     ChildMenuElement[]|ObjectCollection findByRouteParams(string $menelm_route_params) Return ChildMenuElement objects filtered by the menelm_route_params column
 * @method     ChildMenuElement[]|ObjectCollection findByRole(string $menelm_role) Return ChildMenuElement objects filtered by the menelm_role column
 * @method     ChildMenuElement[]|ObjectCollection findByCreatedAt(string $menelm_created_at) Return ChildMenuElement objects filtered by the menelm_created_at column
 * @method     ChildMenuElement[]|ObjectCollection findByUpdatedAt(string $menelm_updated_at) Return ChildMenuElement objects filtered by the menelm_updated_at column
 * @method     ChildMenuElement[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MenuElementQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AdminBundle\Model\Base\MenuElementQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AdminBundle\\Model\\MenuElement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMenuElementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMenuElementQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMenuElementQuery) {
            return $criteria;
        }
        $query = new ChildMenuElementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMenuElement|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MenuElementTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MenuElementTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMenuElement A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT menelm_id, menelm_mengrp_id, menelm_code, menelm_position, menelm_message, menelm_icon_fa, menelm_route_name, menelm_route_params, menelm_role, menelm_created_at, menelm_updated_at FROM menu_element_menelm WHERE menelm_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMenuElement $obj */
            $obj = new ChildMenuElement();
            $obj->hydrate($row);
            MenuElementTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMenuElement|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the menelm_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE menelm_id = 1234
     * $query->filterById(array(12, 34)); // WHERE menelm_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE menelm_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ID, $id, $comparison);
    }

    /**
     * Filter the query on the menelm_mengrp_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMenuGroupId(1234); // WHERE menelm_mengrp_id = 1234
     * $query->filterByMenuGroupId(array(12, 34)); // WHERE menelm_mengrp_id IN (12, 34)
     * $query->filterByMenuGroupId(array('min' => 12)); // WHERE menelm_mengrp_id > 12
     * </code>
     *
     * @see       filterByMenuGroup()
     *
     * @param     mixed $menuGroupId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByMenuGroupId($menuGroupId = null, $comparison = null)
    {
        if (is_array($menuGroupId)) {
            $useMinMax = false;
            if (isset($menuGroupId['min'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_MENGRP_ID, $menuGroupId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($menuGroupId['max'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_MENGRP_ID, $menuGroupId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_MENGRP_ID, $menuGroupId, $comparison);
    }

    /**
     * Filter the query on the menelm_code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE menelm_code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE menelm_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the menelm_position column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition(1234); // WHERE menelm_position = 1234
     * $query->filterByPosition(array(12, 34)); // WHERE menelm_position IN (12, 34)
     * $query->filterByPosition(array('min' => 12)); // WHERE menelm_position > 12
     * </code>
     *
     * @param     mixed $position The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (is_array($position)) {
            $useMinMax = false;
            if (isset($position['min'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_POSITION, $position['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($position['max'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_POSITION, $position['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_POSITION, $position, $comparison);
    }

    /**
     * Filter the query on the menelm_message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE menelm_message = 'fooValue'
     * $query->filterByMessage('%fooValue%', Criteria::LIKE); // WHERE menelm_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the menelm_icon_fa column
     *
     * Example usage:
     * <code>
     * $query->filterByIconFontAwesome('fooValue');   // WHERE menelm_icon_fa = 'fooValue'
     * $query->filterByIconFontAwesome('%fooValue%', Criteria::LIKE); // WHERE menelm_icon_fa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iconFontAwesome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByIconFontAwesome($iconFontAwesome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iconFontAwesome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ICON_FA, $iconFontAwesome, $comparison);
    }

    /**
     * Filter the query on the menelm_route_name column
     *
     * Example usage:
     * <code>
     * $query->filterByRouteName('fooValue');   // WHERE menelm_route_name = 'fooValue'
     * $query->filterByRouteName('%fooValue%', Criteria::LIKE); // WHERE menelm_route_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $routeName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByRouteName($routeName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($routeName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ROUTE_NAME, $routeName, $comparison);
    }

    /**
     * Filter the query on the menelm_route_params column
     *
     * Example usage:
     * <code>
     * $query->filterByRouteParams('fooValue');   // WHERE menelm_route_params = 'fooValue'
     * $query->filterByRouteParams('%fooValue%', Criteria::LIKE); // WHERE menelm_route_params LIKE '%fooValue%'
     * </code>
     *
     * @param     string $routeParams The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByRouteParams($routeParams = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($routeParams)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ROUTE_PARAMS, $routeParams, $comparison);
    }

    /**
     * Filter the query on the menelm_role column
     *
     * Example usage:
     * <code>
     * $query->filterByRole('fooValue');   // WHERE menelm_role = 'fooValue'
     * $query->filterByRole('%fooValue%', Criteria::LIKE); // WHERE menelm_role LIKE '%fooValue%'
     * </code>
     *
     * @param     string $role The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByRole($role = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($role)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ROLE, $role, $comparison);
    }

    /**
     * Filter the query on the menelm_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE menelm_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE menelm_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE menelm_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the menelm_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE menelm_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE menelm_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE menelm_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MenuElementTableMap::COL_MENELM_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\MenuGroup object
     *
     * @param \IiMedias\AdminBundle\Model\MenuGroup|ObjectCollection $menuGroup The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMenuElementQuery The current query, for fluid interface
     */
    public function filterByMenuGroup($menuGroup, $comparison = null)
    {
        if ($menuGroup instanceof \IiMedias\AdminBundle\Model\MenuGroup) {
            return $this
                ->addUsingAlias(MenuElementTableMap::COL_MENELM_MENGRP_ID, $menuGroup->getId(), $comparison);
        } elseif ($menuGroup instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MenuElementTableMap::COL_MENELM_MENGRP_ID, $menuGroup->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMenuGroup() only accepts arguments of type \IiMedias\AdminBundle\Model\MenuGroup or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MenuGroup relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function joinMenuGroup($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MenuGroup');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MenuGroup');
        }

        return $this;
    }

    /**
     * Use the MenuGroup relation MenuGroup object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\MenuGroupQuery A secondary query class using the current class as primary query
     */
    public function useMenuGroupQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMenuGroup($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MenuGroup', '\IiMedias\AdminBundle\Model\MenuGroupQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMenuElement $menuElement Object to remove from the list of results
     *
     * @return $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function prune($menuElement = null)
    {
        if ($menuElement) {
            $this->addUsingAlias(MenuElementTableMap::COL_MENELM_ID, $menuElement->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the menu_element_menelm table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MenuElementTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MenuElementTableMap::clearInstancePool();
            MenuElementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MenuElementTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MenuElementTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MenuElementTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MenuElementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(MenuElementTableMap::COL_MENELM_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(MenuElementTableMap::COL_MENELM_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(MenuElementTableMap::COL_MENELM_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(MenuElementTableMap::COL_MENELM_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildMenuElementQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(MenuElementTableMap::COL_MENELM_CREATED_AT);
    }

} // MenuElementQuery
