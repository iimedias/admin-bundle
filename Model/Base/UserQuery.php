<?php

namespace IiMedias\AdminBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User as ChildUser;
use IiMedias\AdminBundle\Model\UserQuery as ChildUserQuery;
use IiMedias\AdminBundle\Model\Map\UserTableMap;
use IiMedias\StreamBundle\Model\Avatar;
use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\Rank;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\Stream;
use IiMedias\VideoGamesBundle\Model\ApiCount;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_user_usrusr' table.
 *
 *
 *
 * @method     ChildUserQuery orderById($order = Criteria::ASC) Order by the usrusr_id column
 * @method     ChildUserQuery orderByUsername($order = Criteria::ASC) Order by the usrusr_username column
 * @method     ChildUserQuery orderByEmail($order = Criteria::ASC) Order by the usrusr_email column
 * @method     ChildUserQuery orderByFacebookId($order = Criteria::ASC) Order by the usrusr_facebook_id column
 * @method     ChildUserQuery orderByGoogleId($order = Criteria::ASC) Order by the usrusr_google_id column
 * @method     ChildUserQuery orderByTwitterId($order = Criteria::ASC) Order by the usrusr_twitter_id column
 * @method     ChildUserQuery orderByPassword($order = Criteria::ASC) Order by the usrusr_password column
 * @method     ChildUserQuery orderBySalt($order = Criteria::ASC) Order by the usrusr_salt column
 * @method     ChildUserQuery orderByRoles($order = Criteria::ASC) Order by the usrusr_roles column
 * @method     ChildUserQuery orderByIsActive($order = Criteria::ASC) Order by the usrusr_is_active column
 * @method     ChildUserQuery orderByCreatedAt($order = Criteria::ASC) Order by the usrusr_created_at column
 * @method     ChildUserQuery orderByUpdatedAt($order = Criteria::ASC) Order by the usrusr_updated_at column
 * @method     ChildUserQuery orderByUsrusrSlug($order = Criteria::ASC) Order by the usrusr_slug column
 *
 * @method     ChildUserQuery groupById() Group by the usrusr_id column
 * @method     ChildUserQuery groupByUsername() Group by the usrusr_username column
 * @method     ChildUserQuery groupByEmail() Group by the usrusr_email column
 * @method     ChildUserQuery groupByFacebookId() Group by the usrusr_facebook_id column
 * @method     ChildUserQuery groupByGoogleId() Group by the usrusr_google_id column
 * @method     ChildUserQuery groupByTwitterId() Group by the usrusr_twitter_id column
 * @method     ChildUserQuery groupByPassword() Group by the usrusr_password column
 * @method     ChildUserQuery groupBySalt() Group by the usrusr_salt column
 * @method     ChildUserQuery groupByRoles() Group by the usrusr_roles column
 * @method     ChildUserQuery groupByIsActive() Group by the usrusr_is_active column
 * @method     ChildUserQuery groupByCreatedAt() Group by the usrusr_created_at column
 * @method     ChildUserQuery groupByUpdatedAt() Group by the usrusr_updated_at column
 * @method     ChildUserQuery groupByUsrusrSlug() Group by the usrusr_slug column
 *
 * @method     ChildUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserQuery leftJoinCreatedByUserStsite($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUserStsite relation
 * @method     ChildUserQuery rightJoinCreatedByUserStsite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUserStsite relation
 * @method     ChildUserQuery innerJoinCreatedByUserStsite($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUserStsite relation
 *
 * @method     ChildUserQuery joinWithCreatedByUserStsite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUserStsite relation
 *
 * @method     ChildUserQuery leftJoinWithCreatedByUserStsite() Adds a LEFT JOIN clause and with to the query using the CreatedByUserStsite relation
 * @method     ChildUserQuery rightJoinWithCreatedByUserStsite() Adds a RIGHT JOIN clause and with to the query using the CreatedByUserStsite relation
 * @method     ChildUserQuery innerJoinWithCreatedByUserStsite() Adds a INNER JOIN clause and with to the query using the CreatedByUserStsite relation
 *
 * @method     ChildUserQuery leftJoinUpdatedByUserStsite($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUserStsite relation
 * @method     ChildUserQuery rightJoinUpdatedByUserStsite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUserStsite relation
 * @method     ChildUserQuery innerJoinUpdatedByUserStsite($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUserStsite relation
 *
 * @method     ChildUserQuery joinWithUpdatedByUserStsite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUserStsite relation
 *
 * @method     ChildUserQuery leftJoinWithUpdatedByUserStsite() Adds a LEFT JOIN clause and with to the query using the UpdatedByUserStsite relation
 * @method     ChildUserQuery rightJoinWithUpdatedByUserStsite() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUserStsite relation
 * @method     ChildUserQuery innerJoinWithUpdatedByUserStsite() Adds a INNER JOIN clause and with to the query using the UpdatedByUserStsite relation
 *
 * @method     ChildUserQuery leftJoinCreatedByUserStstrm($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUserStstrm relation
 * @method     ChildUserQuery rightJoinCreatedByUserStstrm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUserStstrm relation
 * @method     ChildUserQuery innerJoinCreatedByUserStstrm($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUserStstrm relation
 *
 * @method     ChildUserQuery joinWithCreatedByUserStstrm($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUserStstrm relation
 *
 * @method     ChildUserQuery leftJoinWithCreatedByUserStstrm() Adds a LEFT JOIN clause and with to the query using the CreatedByUserStstrm relation
 * @method     ChildUserQuery rightJoinWithCreatedByUserStstrm() Adds a RIGHT JOIN clause and with to the query using the CreatedByUserStstrm relation
 * @method     ChildUserQuery innerJoinWithCreatedByUserStstrm() Adds a INNER JOIN clause and with to the query using the CreatedByUserStstrm relation
 *
 * @method     ChildUserQuery leftJoinUpdatedByUserStstrm($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUserStstrm relation
 * @method     ChildUserQuery rightJoinUpdatedByUserStstrm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUserStstrm relation
 * @method     ChildUserQuery innerJoinUpdatedByUserStstrm($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUserStstrm relation
 *
 * @method     ChildUserQuery joinWithUpdatedByUserStstrm($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUserStstrm relation
 *
 * @method     ChildUserQuery leftJoinWithUpdatedByUserStstrm() Adds a LEFT JOIN clause and with to the query using the UpdatedByUserStstrm relation
 * @method     ChildUserQuery rightJoinWithUpdatedByUserStstrm() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUserStstrm relation
 * @method     ChildUserQuery innerJoinWithUpdatedByUserStstrm() Adds a INNER JOIN clause and with to the query using the UpdatedByUserStstrm relation
 *
 * @method     ChildUserQuery leftJoinCreatedByUserStchan($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUserStchan relation
 * @method     ChildUserQuery rightJoinCreatedByUserStchan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUserStchan relation
 * @method     ChildUserQuery innerJoinCreatedByUserStchan($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUserStchan relation
 *
 * @method     ChildUserQuery joinWithCreatedByUserStchan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUserStchan relation
 *
 * @method     ChildUserQuery leftJoinWithCreatedByUserStchan() Adds a LEFT JOIN clause and with to the query using the CreatedByUserStchan relation
 * @method     ChildUserQuery rightJoinWithCreatedByUserStchan() Adds a RIGHT JOIN clause and with to the query using the CreatedByUserStchan relation
 * @method     ChildUserQuery innerJoinWithCreatedByUserStchan() Adds a INNER JOIN clause and with to the query using the CreatedByUserStchan relation
 *
 * @method     ChildUserQuery leftJoinUpdatedByUserStchan($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUserStchan relation
 * @method     ChildUserQuery rightJoinUpdatedByUserStchan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUserStchan relation
 * @method     ChildUserQuery innerJoinUpdatedByUserStchan($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUserStchan relation
 *
 * @method     ChildUserQuery joinWithUpdatedByUserStchan($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUserStchan relation
 *
 * @method     ChildUserQuery leftJoinWithUpdatedByUserStchan() Adds a LEFT JOIN clause and with to the query using the UpdatedByUserStchan relation
 * @method     ChildUserQuery rightJoinWithUpdatedByUserStchan() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUserStchan relation
 * @method     ChildUserQuery innerJoinWithUpdatedByUserStchan() Adds a INNER JOIN clause and with to the query using the UpdatedByUserStchan relation
 *
 * @method     ChildUserQuery leftJoinCreatedByUserStcusr($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUserStcusr relation
 * @method     ChildUserQuery rightJoinCreatedByUserStcusr($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUserStcusr relation
 * @method     ChildUserQuery innerJoinCreatedByUserStcusr($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUserStcusr relation
 *
 * @method     ChildUserQuery joinWithCreatedByUserStcusr($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUserStcusr relation
 *
 * @method     ChildUserQuery leftJoinWithCreatedByUserStcusr() Adds a LEFT JOIN clause and with to the query using the CreatedByUserStcusr relation
 * @method     ChildUserQuery rightJoinWithCreatedByUserStcusr() Adds a RIGHT JOIN clause and with to the query using the CreatedByUserStcusr relation
 * @method     ChildUserQuery innerJoinWithCreatedByUserStcusr() Adds a INNER JOIN clause and with to the query using the CreatedByUserStcusr relation
 *
 * @method     ChildUserQuery leftJoinUpdatedByUserStcusr($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUserStcusr relation
 * @method     ChildUserQuery rightJoinUpdatedByUserStcusr($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUserStcusr relation
 * @method     ChildUserQuery innerJoinUpdatedByUserStcusr($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUserStcusr relation
 *
 * @method     ChildUserQuery joinWithUpdatedByUserStcusr($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUserStcusr relation
 *
 * @method     ChildUserQuery leftJoinWithUpdatedByUserStcusr() Adds a LEFT JOIN clause and with to the query using the UpdatedByUserStcusr relation
 * @method     ChildUserQuery rightJoinWithUpdatedByUserStcusr() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUserStcusr relation
 * @method     ChildUserQuery innerJoinWithUpdatedByUserStcusr() Adds a INNER JOIN clause and with to the query using the UpdatedByUserStcusr relation
 *
 * @method     ChildUserQuery leftJoinCreatedByUserStrank($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUserStrank relation
 * @method     ChildUserQuery rightJoinCreatedByUserStrank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUserStrank relation
 * @method     ChildUserQuery innerJoinCreatedByUserStrank($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUserStrank relation
 *
 * @method     ChildUserQuery joinWithCreatedByUserStrank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUserStrank relation
 *
 * @method     ChildUserQuery leftJoinWithCreatedByUserStrank() Adds a LEFT JOIN clause and with to the query using the CreatedByUserStrank relation
 * @method     ChildUserQuery rightJoinWithCreatedByUserStrank() Adds a RIGHT JOIN clause and with to the query using the CreatedByUserStrank relation
 * @method     ChildUserQuery innerJoinWithCreatedByUserStrank() Adds a INNER JOIN clause and with to the query using the CreatedByUserStrank relation
 *
 * @method     ChildUserQuery leftJoinUpdatedByUserStrank($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUserStrank relation
 * @method     ChildUserQuery rightJoinUpdatedByUserStrank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUserStrank relation
 * @method     ChildUserQuery innerJoinUpdatedByUserStrank($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUserStrank relation
 *
 * @method     ChildUserQuery joinWithUpdatedByUserStrank($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUserStrank relation
 *
 * @method     ChildUserQuery leftJoinWithUpdatedByUserStrank() Adds a LEFT JOIN clause and with to the query using the UpdatedByUserStrank relation
 * @method     ChildUserQuery rightJoinWithUpdatedByUserStrank() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUserStrank relation
 * @method     ChildUserQuery innerJoinWithUpdatedByUserStrank() Adds a INNER JOIN clause and with to the query using the UpdatedByUserStrank relation
 *
 * @method     ChildUserQuery leftJoinCreatedByUserStavtr($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUserStavtr relation
 * @method     ChildUserQuery rightJoinCreatedByUserStavtr($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUserStavtr relation
 * @method     ChildUserQuery innerJoinCreatedByUserStavtr($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUserStavtr relation
 *
 * @method     ChildUserQuery joinWithCreatedByUserStavtr($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUserStavtr relation
 *
 * @method     ChildUserQuery leftJoinWithCreatedByUserStavtr() Adds a LEFT JOIN clause and with to the query using the CreatedByUserStavtr relation
 * @method     ChildUserQuery rightJoinWithCreatedByUserStavtr() Adds a RIGHT JOIN clause and with to the query using the CreatedByUserStavtr relation
 * @method     ChildUserQuery innerJoinWithCreatedByUserStavtr() Adds a INNER JOIN clause and with to the query using the CreatedByUserStavtr relation
 *
 * @method     ChildUserQuery leftJoinUpdatedByUserStavtr($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUserStavtr relation
 * @method     ChildUserQuery rightJoinUpdatedByUserStavtr($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUserStavtr relation
 * @method     ChildUserQuery innerJoinUpdatedByUserStavtr($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUserStavtr relation
 *
 * @method     ChildUserQuery joinWithUpdatedByUserStavtr($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUserStavtr relation
 *
 * @method     ChildUserQuery leftJoinWithUpdatedByUserStavtr() Adds a LEFT JOIN clause and with to the query using the UpdatedByUserStavtr relation
 * @method     ChildUserQuery rightJoinWithUpdatedByUserStavtr() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUserStavtr relation
 * @method     ChildUserQuery innerJoinWithUpdatedByUserStavtr() Adds a INNER JOIN clause and with to the query using the UpdatedByUserStavtr relation
 *
 * @method     ChildUserQuery leftJoinCreatedByUserVideoGamesApiCount($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUserVideoGamesApiCount relation
 * @method     ChildUserQuery rightJoinCreatedByUserVideoGamesApiCount($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUserVideoGamesApiCount relation
 * @method     ChildUserQuery innerJoinCreatedByUserVideoGamesApiCount($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUserVideoGamesApiCount relation
 *
 * @method     ChildUserQuery joinWithCreatedByUserVideoGamesApiCount($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUserVideoGamesApiCount relation
 *
 * @method     ChildUserQuery leftJoinWithCreatedByUserVideoGamesApiCount() Adds a LEFT JOIN clause and with to the query using the CreatedByUserVideoGamesApiCount relation
 * @method     ChildUserQuery rightJoinWithCreatedByUserVideoGamesApiCount() Adds a RIGHT JOIN clause and with to the query using the CreatedByUserVideoGamesApiCount relation
 * @method     ChildUserQuery innerJoinWithCreatedByUserVideoGamesApiCount() Adds a INNER JOIN clause and with to the query using the CreatedByUserVideoGamesApiCount relation
 *
 * @method     ChildUserQuery leftJoinUpdatedByUserVideoGamesApiCount($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUserVideoGamesApiCount relation
 * @method     ChildUserQuery rightJoinUpdatedByUserVideoGamesApiCount($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUserVideoGamesApiCount relation
 * @method     ChildUserQuery innerJoinUpdatedByUserVideoGamesApiCount($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUserVideoGamesApiCount relation
 *
 * @method     ChildUserQuery joinWithUpdatedByUserVideoGamesApiCount($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUserVideoGamesApiCount relation
 *
 * @method     ChildUserQuery leftJoinWithUpdatedByUserVideoGamesApiCount() Adds a LEFT JOIN clause and with to the query using the UpdatedByUserVideoGamesApiCount relation
 * @method     ChildUserQuery rightJoinWithUpdatedByUserVideoGamesApiCount() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUserVideoGamesApiCount relation
 * @method     ChildUserQuery innerJoinWithUpdatedByUserVideoGamesApiCount() Adds a INNER JOIN clause and with to the query using the UpdatedByUserVideoGamesApiCount relation
 *
 * @method     \IiMedias\StreamBundle\Model\SiteQuery|\IiMedias\StreamBundle\Model\StreamQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\ChatUserQuery|\IiMedias\StreamBundle\Model\RankQuery|\IiMedias\StreamBundle\Model\AvatarQuery|\IiMedias\VideoGamesBundle\Model\ApiCountQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUser findOne(ConnectionInterface $con = null) Return the first ChildUser matching the query
 * @method     ChildUser findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUser matching the query, or a new ChildUser object populated from the query conditions when no match is found
 *
 * @method     ChildUser findOneById(int $usrusr_id) Return the first ChildUser filtered by the usrusr_id column
 * @method     ChildUser findOneByUsername(string $usrusr_username) Return the first ChildUser filtered by the usrusr_username column
 * @method     ChildUser findOneByEmail(string $usrusr_email) Return the first ChildUser filtered by the usrusr_email column
 * @method     ChildUser findOneByFacebookId(string $usrusr_facebook_id) Return the first ChildUser filtered by the usrusr_facebook_id column
 * @method     ChildUser findOneByGoogleId(string $usrusr_google_id) Return the first ChildUser filtered by the usrusr_google_id column
 * @method     ChildUser findOneByTwitterId(string $usrusr_twitter_id) Return the first ChildUser filtered by the usrusr_twitter_id column
 * @method     ChildUser findOneByPassword(string $usrusr_password) Return the first ChildUser filtered by the usrusr_password column
 * @method     ChildUser findOneBySalt(string $usrusr_salt) Return the first ChildUser filtered by the usrusr_salt column
 * @method     ChildUser findOneByRoles(array $usrusr_roles) Return the first ChildUser filtered by the usrusr_roles column
 * @method     ChildUser findOneByIsActive(boolean $usrusr_is_active) Return the first ChildUser filtered by the usrusr_is_active column
 * @method     ChildUser findOneByCreatedAt(string $usrusr_created_at) Return the first ChildUser filtered by the usrusr_created_at column
 * @method     ChildUser findOneByUpdatedAt(string $usrusr_updated_at) Return the first ChildUser filtered by the usrusr_updated_at column
 * @method     ChildUser findOneByUsrusrSlug(string $usrusr_slug) Return the first ChildUser filtered by the usrusr_slug column *

 * @method     ChildUser requirePk($key, ConnectionInterface $con = null) Return the ChildUser by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOne(ConnectionInterface $con = null) Return the first ChildUser matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUser requireOneById(int $usrusr_id) Return the first ChildUser filtered by the usrusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByUsername(string $usrusr_username) Return the first ChildUser filtered by the usrusr_username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByEmail(string $usrusr_email) Return the first ChildUser filtered by the usrusr_email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByFacebookId(string $usrusr_facebook_id) Return the first ChildUser filtered by the usrusr_facebook_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByGoogleId(string $usrusr_google_id) Return the first ChildUser filtered by the usrusr_google_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByTwitterId(string $usrusr_twitter_id) Return the first ChildUser filtered by the usrusr_twitter_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByPassword(string $usrusr_password) Return the first ChildUser filtered by the usrusr_password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneBySalt(string $usrusr_salt) Return the first ChildUser filtered by the usrusr_salt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByRoles(array $usrusr_roles) Return the first ChildUser filtered by the usrusr_roles column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByIsActive(boolean $usrusr_is_active) Return the first ChildUser filtered by the usrusr_is_active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByCreatedAt(string $usrusr_created_at) Return the first ChildUser filtered by the usrusr_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByUpdatedAt(string $usrusr_updated_at) Return the first ChildUser filtered by the usrusr_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByUsrusrSlug(string $usrusr_slug) Return the first ChildUser filtered by the usrusr_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUser[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUser objects based on current ModelCriteria
 * @method     ChildUser[]|ObjectCollection findById(int $usrusr_id) Return ChildUser objects filtered by the usrusr_id column
 * @method     ChildUser[]|ObjectCollection findByUsername(string $usrusr_username) Return ChildUser objects filtered by the usrusr_username column
 * @method     ChildUser[]|ObjectCollection findByEmail(string $usrusr_email) Return ChildUser objects filtered by the usrusr_email column
 * @method     ChildUser[]|ObjectCollection findByFacebookId(string $usrusr_facebook_id) Return ChildUser objects filtered by the usrusr_facebook_id column
 * @method     ChildUser[]|ObjectCollection findByGoogleId(string $usrusr_google_id) Return ChildUser objects filtered by the usrusr_google_id column
 * @method     ChildUser[]|ObjectCollection findByTwitterId(string $usrusr_twitter_id) Return ChildUser objects filtered by the usrusr_twitter_id column
 * @method     ChildUser[]|ObjectCollection findByPassword(string $usrusr_password) Return ChildUser objects filtered by the usrusr_password column
 * @method     ChildUser[]|ObjectCollection findBySalt(string $usrusr_salt) Return ChildUser objects filtered by the usrusr_salt column
 * @method     ChildUser[]|ObjectCollection findByRoles(array $usrusr_roles) Return ChildUser objects filtered by the usrusr_roles column
 * @method     ChildUser[]|ObjectCollection findByIsActive(boolean $usrusr_is_active) Return ChildUser objects filtered by the usrusr_is_active column
 * @method     ChildUser[]|ObjectCollection findByCreatedAt(string $usrusr_created_at) Return ChildUser objects filtered by the usrusr_created_at column
 * @method     ChildUser[]|ObjectCollection findByUpdatedAt(string $usrusr_updated_at) Return ChildUser objects filtered by the usrusr_updated_at column
 * @method     ChildUser[]|ObjectCollection findByUsrusrSlug(string $usrusr_slug) Return ChildUser objects filtered by the usrusr_slug column
 * @method     ChildUser[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AdminBundle\Model\Base\UserQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AdminBundle\\Model\\User', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserQuery) {
            return $criteria;
        }
        $query = new ChildUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUser|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUser A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT usrusr_id, usrusr_username, usrusr_email, usrusr_facebook_id, usrusr_google_id, usrusr_twitter_id, usrusr_password, usrusr_salt, usrusr_roles, usrusr_is_active, usrusr_created_at, usrusr_updated_at, usrusr_slug FROM user_user_usrusr WHERE usrusr_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUser $obj */
            $obj = new ChildUser();
            $obj->hydrate($row);
            UserTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUser|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the usrusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE usrusr_id = 1234
     * $query->filterById(array(12, 34)); // WHERE usrusr_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE usrusr_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserTableMap::COL_USRUSR_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserTableMap::COL_USRUSR_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_ID, $id, $comparison);
    }

    /**
     * Filter the query on the usrusr_username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE usrusr_username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE usrusr_username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the usrusr_email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE usrusr_email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE usrusr_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the usrusr_facebook_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFacebookId('fooValue');   // WHERE usrusr_facebook_id = 'fooValue'
     * $query->filterByFacebookId('%fooValue%', Criteria::LIKE); // WHERE usrusr_facebook_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $facebookId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByFacebookId($facebookId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($facebookId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_FACEBOOK_ID, $facebookId, $comparison);
    }

    /**
     * Filter the query on the usrusr_google_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGoogleId('fooValue');   // WHERE usrusr_google_id = 'fooValue'
     * $query->filterByGoogleId('%fooValue%', Criteria::LIKE); // WHERE usrusr_google_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $googleId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByGoogleId($googleId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($googleId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_GOOGLE_ID, $googleId, $comparison);
    }

    /**
     * Filter the query on the usrusr_twitter_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTwitterId('fooValue');   // WHERE usrusr_twitter_id = 'fooValue'
     * $query->filterByTwitterId('%fooValue%', Criteria::LIKE); // WHERE usrusr_twitter_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $twitterId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByTwitterId($twitterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($twitterId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_TWITTER_ID, $twitterId, $comparison);
    }

    /**
     * Filter the query on the usrusr_password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE usrusr_password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE usrusr_password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the usrusr_salt column
     *
     * Example usage:
     * <code>
     * $query->filterBySalt('fooValue');   // WHERE usrusr_salt = 'fooValue'
     * $query->filterBySalt('%fooValue%', Criteria::LIKE); // WHERE usrusr_salt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterBySalt($salt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_SALT, $salt, $comparison);
    }

    /**
     * Filter the query on the usrusr_roles column
     *
     * @param     array $roles The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByRoles($roles = null, $comparison = null)
    {
        $key = $this->getAliasedColName(UserTableMap::COL_USRUSR_ROLES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($roles as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($roles as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($roles as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_ROLES, $roles, $comparison);
    }

    /**
     * Filter the query on the usrusr_roles column
     * @param     mixed $roles The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByRole($roles = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($roles)) {
                $roles = '%| ' . $roles . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $roles = '%| ' . $roles . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(UserTableMap::COL_USRUSR_ROLES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $roles, $comparison);
            } else {
                $this->addAnd($key, $roles, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_ROLES, $roles, $comparison);
    }

    /**
     * Filter the query on the usrusr_is_active column
     *
     * Example usage:
     * <code>
     * $query->filterByIsActive(true); // WHERE usrusr_is_active = true
     * $query->filterByIsActive('yes'); // WHERE usrusr_is_active = true
     * </code>
     *
     * @param     boolean|string $isActive The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByIsActive($isActive = null, $comparison = null)
    {
        if (is_string($isActive)) {
            $isActive = in_array(strtolower($isActive), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_IS_ACTIVE, $isActive, $comparison);
    }

    /**
     * Filter the query on the usrusr_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE usrusr_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE usrusr_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE usrusr_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserTableMap::COL_USRUSR_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserTableMap::COL_USRUSR_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the usrusr_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE usrusr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE usrusr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE usrusr_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserTableMap::COL_USRUSR_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserTableMap::COL_USRUSR_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the usrusr_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByUsrusrSlug('fooValue');   // WHERE usrusr_slug = 'fooValue'
     * $query->filterByUsrusrSlug('%fooValue%', Criteria::LIKE); // WHERE usrusr_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usrusrSlug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByUsrusrSlug($usrusrSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usrusrSlug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_USRUSR_SLUG, $usrusrSlug, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserStsite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $site->getCreatedByUserId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            return $this
                ->useCreatedByUserStsiteQuery()
                ->filterByPrimaryKeys($site->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCreatedByUserStsite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUserStsite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinCreatedByUserStsite($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUserStsite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUserStsite');
        }

        return $this;
    }

    /**
     * Use the CreatedByUserStsite relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserStsiteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUserStsite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUserStsite', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Site object
     *
     * @param \IiMedias\StreamBundle\Model\Site|ObjectCollection $site the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserStsite($site, $comparison = null)
    {
        if ($site instanceof \IiMedias\StreamBundle\Model\Site) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $site->getUpdatedByUserId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            return $this
                ->useUpdatedByUserStsiteQuery()
                ->filterByPrimaryKeys($site->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpdatedByUserStsite() only accepts arguments of type \IiMedias\StreamBundle\Model\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUserStsite relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUpdatedByUserStsite($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUserStsite');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUserStsite');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUserStsite relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\SiteQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserStsiteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUserStsite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUserStsite', '\IiMedias\StreamBundle\Model\SiteQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserStstrm($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $stream->getCreatedByUserId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            return $this
                ->useCreatedByUserStstrmQuery()
                ->filterByPrimaryKeys($stream->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCreatedByUserStstrm() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUserStstrm relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinCreatedByUserStstrm($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUserStstrm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUserStstrm');
        }

        return $this;
    }

    /**
     * Use the CreatedByUserStstrm relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserStstrmQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUserStstrm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUserStstrm', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Stream object
     *
     * @param \IiMedias\StreamBundle\Model\Stream|ObjectCollection $stream the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserStstrm($stream, $comparison = null)
    {
        if ($stream instanceof \IiMedias\StreamBundle\Model\Stream) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $stream->getUpdatedByUserId(), $comparison);
        } elseif ($stream instanceof ObjectCollection) {
            return $this
                ->useUpdatedByUserStstrmQuery()
                ->filterByPrimaryKeys($stream->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpdatedByUserStstrm() only accepts arguments of type \IiMedias\StreamBundle\Model\Stream or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUserStstrm relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUpdatedByUserStstrm($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUserStstrm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUserStstrm');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUserStstrm relation Stream object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\StreamQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserStstrmQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUserStstrm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUserStstrm', '\IiMedias\StreamBundle\Model\StreamQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserStchan($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $channel->getCreatedByUserId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            return $this
                ->useCreatedByUserStchanQuery()
                ->filterByPrimaryKeys($channel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCreatedByUserStchan() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUserStchan relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinCreatedByUserStchan($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUserStchan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUserStchan');
        }

        return $this;
    }

    /**
     * Use the CreatedByUserStchan relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserStchanQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUserStchan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUserStchan', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserStchan($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $channel->getUpdatedByUserId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            return $this
                ->useUpdatedByUserStchanQuery()
                ->filterByPrimaryKeys($channel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpdatedByUserStchan() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUserStchan relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUpdatedByUserStchan($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUserStchan');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUserStchan');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUserStchan relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserStchanQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUserStchan($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUserStchan', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserStcusr($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $chatUser->getCreatedByUserId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            return $this
                ->useCreatedByUserStcusrQuery()
                ->filterByPrimaryKeys($chatUser->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCreatedByUserStcusr() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUserStcusr relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinCreatedByUserStcusr($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUserStcusr');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUserStcusr');
        }

        return $this;
    }

    /**
     * Use the CreatedByUserStcusr relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserStcusrQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUserStcusr($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUserStcusr', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\ChatUser object
     *
     * @param \IiMedias\StreamBundle\Model\ChatUser|ObjectCollection $chatUser the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserStcusr($chatUser, $comparison = null)
    {
        if ($chatUser instanceof \IiMedias\StreamBundle\Model\ChatUser) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $chatUser->getUpdatedByUserId(), $comparison);
        } elseif ($chatUser instanceof ObjectCollection) {
            return $this
                ->useUpdatedByUserStcusrQuery()
                ->filterByPrimaryKeys($chatUser->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpdatedByUserStcusr() only accepts arguments of type \IiMedias\StreamBundle\Model\ChatUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUserStcusr relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUpdatedByUserStcusr($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUserStcusr');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUserStcusr');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUserStcusr relation ChatUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChatUserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserStcusrQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUserStcusr($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUserStcusr', '\IiMedias\StreamBundle\Model\ChatUserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Rank object
     *
     * @param \IiMedias\StreamBundle\Model\Rank|ObjectCollection $rank the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserStrank($rank, $comparison = null)
    {
        if ($rank instanceof \IiMedias\StreamBundle\Model\Rank) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $rank->getCreatedByUserId(), $comparison);
        } elseif ($rank instanceof ObjectCollection) {
            return $this
                ->useCreatedByUserStrankQuery()
                ->filterByPrimaryKeys($rank->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCreatedByUserStrank() only accepts arguments of type \IiMedias\StreamBundle\Model\Rank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUserStrank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinCreatedByUserStrank($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUserStrank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUserStrank');
        }

        return $this;
    }

    /**
     * Use the CreatedByUserStrank relation Rank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\RankQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserStrankQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUserStrank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUserStrank', '\IiMedias\StreamBundle\Model\RankQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Rank object
     *
     * @param \IiMedias\StreamBundle\Model\Rank|ObjectCollection $rank the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserStrank($rank, $comparison = null)
    {
        if ($rank instanceof \IiMedias\StreamBundle\Model\Rank) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $rank->getUpdatedByUserId(), $comparison);
        } elseif ($rank instanceof ObjectCollection) {
            return $this
                ->useUpdatedByUserStrankQuery()
                ->filterByPrimaryKeys($rank->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpdatedByUserStrank() only accepts arguments of type \IiMedias\StreamBundle\Model\Rank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUserStrank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUpdatedByUserStrank($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUserStrank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUserStrank');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUserStrank relation Rank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\RankQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserStrankQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUserStrank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUserStrank', '\IiMedias\StreamBundle\Model\RankQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Avatar object
     *
     * @param \IiMedias\StreamBundle\Model\Avatar|ObjectCollection $avatar the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserStavtr($avatar, $comparison = null)
    {
        if ($avatar instanceof \IiMedias\StreamBundle\Model\Avatar) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $avatar->getCreatedByUserId(), $comparison);
        } elseif ($avatar instanceof ObjectCollection) {
            return $this
                ->useCreatedByUserStavtrQuery()
                ->filterByPrimaryKeys($avatar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCreatedByUserStavtr() only accepts arguments of type \IiMedias\StreamBundle\Model\Avatar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUserStavtr relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinCreatedByUserStavtr($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUserStavtr');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUserStavtr');
        }

        return $this;
    }

    /**
     * Use the CreatedByUserStavtr relation Avatar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\AvatarQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserStavtrQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUserStavtr($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUserStavtr', '\IiMedias\StreamBundle\Model\AvatarQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Avatar object
     *
     * @param \IiMedias\StreamBundle\Model\Avatar|ObjectCollection $avatar the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserStavtr($avatar, $comparison = null)
    {
        if ($avatar instanceof \IiMedias\StreamBundle\Model\Avatar) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $avatar->getUpdatedByUserId(), $comparison);
        } elseif ($avatar instanceof ObjectCollection) {
            return $this
                ->useUpdatedByUserStavtrQuery()
                ->filterByPrimaryKeys($avatar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpdatedByUserStavtr() only accepts arguments of type \IiMedias\StreamBundle\Model\Avatar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUserStavtr relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUpdatedByUserStavtr($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUserStavtr');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUserStavtr');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUserStavtr relation Avatar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\AvatarQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserStavtrQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUserStavtr($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUserStavtr', '\IiMedias\StreamBundle\Model\AvatarQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiCount object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiCount|ObjectCollection $apiCount the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserVideoGamesApiCount($apiCount, $comparison = null)
    {
        if ($apiCount instanceof \IiMedias\VideoGamesBundle\Model\ApiCount) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $apiCount->getCreatedByUserId(), $comparison);
        } elseif ($apiCount instanceof ObjectCollection) {
            return $this
                ->useCreatedByUserVideoGamesApiCountQuery()
                ->filterByPrimaryKeys($apiCount->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCreatedByUserVideoGamesApiCount() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiCount or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUserVideoGamesApiCount relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinCreatedByUserVideoGamesApiCount($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUserVideoGamesApiCount');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUserVideoGamesApiCount');
        }

        return $this;
    }

    /**
     * Use the CreatedByUserVideoGamesApiCount relation ApiCount object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiCountQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserVideoGamesApiCountQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUserVideoGamesApiCount($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUserVideoGamesApiCount', '\IiMedias\VideoGamesBundle\Model\ApiCountQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiCount object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiCount|ObjectCollection $apiCount the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserVideoGamesApiCount($apiCount, $comparison = null)
    {
        if ($apiCount instanceof \IiMedias\VideoGamesBundle\Model\ApiCount) {
            return $this
                ->addUsingAlias(UserTableMap::COL_USRUSR_ID, $apiCount->getUpdatedByUserId(), $comparison);
        } elseif ($apiCount instanceof ObjectCollection) {
            return $this
                ->useUpdatedByUserVideoGamesApiCountQuery()
                ->filterByPrimaryKeys($apiCount->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpdatedByUserVideoGamesApiCount() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiCount or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUserVideoGamesApiCount relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUpdatedByUserVideoGamesApiCount($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUserVideoGamesApiCount');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUserVideoGamesApiCount');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUserVideoGamesApiCount relation ApiCount object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiCountQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserVideoGamesApiCountQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUserVideoGamesApiCount($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUserVideoGamesApiCount', '\IiMedias\VideoGamesBundle\Model\ApiCountQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUser $user Object to remove from the list of results
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function prune($user = null)
    {
        if ($user) {
            $this->addUsingAlias(UserTableMap::COL_USRUSR_ID, $user->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user_user_usrusr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserTableMap::clearInstancePool();
            UserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildUserQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(UserTableMap::COL_USRUSR_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildUserQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserTableMap::COL_USRUSR_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildUserQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserTableMap::COL_USRUSR_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildUserQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserTableMap::COL_USRUSR_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildUserQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(UserTableMap::COL_USRUSR_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildUserQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserTableMap::COL_USRUSR_CREATED_AT);
    }

    // sluggable behavior

    /**
     * Filter the query on the slug column
     *
     * @param     string $slug The value to use as filter.
     *
     * @return    $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterBySlug($slug)
    {
        return $this->addUsingAlias(UserTableMap::COL_USRUSR_SLUG, $slug, Criteria::EQUAL);
    }

    /**
     * Find one object based on its slug
     *
     * @param     string $slug The value to use as filter.
     * @param     ConnectionInterface $con The optional connection object
     *
     * @return    ChildUser the result, formatted by the current formatter
     */
    public function findOneBySlug($slug, $con = null)
    {
        return $this->filterBySlug($slug)->findOne($con);
    }

} // UserQuery
