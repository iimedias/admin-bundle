<?php

namespace IiMedias\AdminBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\RoleModule as ChildRoleModule;
use IiMedias\AdminBundle\Model\RoleModuleQuery as ChildRoleModuleQuery;
use IiMedias\AdminBundle\Model\Map\RoleModuleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_role_module_usrmrl' table.
 *
 *
 *
 * @method     ChildRoleModuleQuery orderById($order = Criteria::ASC) Order by the usrmrl_id column
 * @method     ChildRoleModuleQuery orderByLabel($order = Criteria::ASC) Order by the usrmrl_label column
 * @method     ChildRoleModuleQuery orderByBundle($order = Criteria::ASC) Order by the usrmrl_bundle column
 * @method     ChildRoleModuleQuery orderByCreatedAt($order = Criteria::ASC) Order by the usrmrl_created_at column
 * @method     ChildRoleModuleQuery orderByUpdatedAt($order = Criteria::ASC) Order by the usrmrl_updated_at column
 *
 * @method     ChildRoleModuleQuery groupById() Group by the usrmrl_id column
 * @method     ChildRoleModuleQuery groupByLabel() Group by the usrmrl_label column
 * @method     ChildRoleModuleQuery groupByBundle() Group by the usrmrl_bundle column
 * @method     ChildRoleModuleQuery groupByCreatedAt() Group by the usrmrl_created_at column
 * @method     ChildRoleModuleQuery groupByUpdatedAt() Group by the usrmrl_updated_at column
 *
 * @method     ChildRoleModuleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRoleModuleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRoleModuleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRoleModuleQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRoleModuleQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRoleModuleQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRoleModuleQuery leftJoinRole($relationAlias = null) Adds a LEFT JOIN clause to the query using the Role relation
 * @method     ChildRoleModuleQuery rightJoinRole($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Role relation
 * @method     ChildRoleModuleQuery innerJoinRole($relationAlias = null) Adds a INNER JOIN clause to the query using the Role relation
 *
 * @method     ChildRoleModuleQuery joinWithRole($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Role relation
 *
 * @method     ChildRoleModuleQuery leftJoinWithRole() Adds a LEFT JOIN clause and with to the query using the Role relation
 * @method     ChildRoleModuleQuery rightJoinWithRole() Adds a RIGHT JOIN clause and with to the query using the Role relation
 * @method     ChildRoleModuleQuery innerJoinWithRole() Adds a INNER JOIN clause and with to the query using the Role relation
 *
 * @method     \IiMedias\AdminBundle\Model\RoleQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRoleModule findOne(ConnectionInterface $con = null) Return the first ChildRoleModule matching the query
 * @method     ChildRoleModule findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRoleModule matching the query, or a new ChildRoleModule object populated from the query conditions when no match is found
 *
 * @method     ChildRoleModule findOneById(int $usrmrl_id) Return the first ChildRoleModule filtered by the usrmrl_id column
 * @method     ChildRoleModule findOneByLabel(string $usrmrl_label) Return the first ChildRoleModule filtered by the usrmrl_label column
 * @method     ChildRoleModule findOneByBundle(string $usrmrl_bundle) Return the first ChildRoleModule filtered by the usrmrl_bundle column
 * @method     ChildRoleModule findOneByCreatedAt(string $usrmrl_created_at) Return the first ChildRoleModule filtered by the usrmrl_created_at column
 * @method     ChildRoleModule findOneByUpdatedAt(string $usrmrl_updated_at) Return the first ChildRoleModule filtered by the usrmrl_updated_at column *

 * @method     ChildRoleModule requirePk($key, ConnectionInterface $con = null) Return the ChildRoleModule by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoleModule requireOne(ConnectionInterface $con = null) Return the first ChildRoleModule matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRoleModule requireOneById(int $usrmrl_id) Return the first ChildRoleModule filtered by the usrmrl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoleModule requireOneByLabel(string $usrmrl_label) Return the first ChildRoleModule filtered by the usrmrl_label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoleModule requireOneByBundle(string $usrmrl_bundle) Return the first ChildRoleModule filtered by the usrmrl_bundle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoleModule requireOneByCreatedAt(string $usrmrl_created_at) Return the first ChildRoleModule filtered by the usrmrl_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRoleModule requireOneByUpdatedAt(string $usrmrl_updated_at) Return the first ChildRoleModule filtered by the usrmrl_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRoleModule[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRoleModule objects based on current ModelCriteria
 * @method     ChildRoleModule[]|ObjectCollection findById(int $usrmrl_id) Return ChildRoleModule objects filtered by the usrmrl_id column
 * @method     ChildRoleModule[]|ObjectCollection findByLabel(string $usrmrl_label) Return ChildRoleModule objects filtered by the usrmrl_label column
 * @method     ChildRoleModule[]|ObjectCollection findByBundle(string $usrmrl_bundle) Return ChildRoleModule objects filtered by the usrmrl_bundle column
 * @method     ChildRoleModule[]|ObjectCollection findByCreatedAt(string $usrmrl_created_at) Return ChildRoleModule objects filtered by the usrmrl_created_at column
 * @method     ChildRoleModule[]|ObjectCollection findByUpdatedAt(string $usrmrl_updated_at) Return ChildRoleModule objects filtered by the usrmrl_updated_at column
 * @method     ChildRoleModule[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RoleModuleQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AdminBundle\Model\Base\RoleModuleQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AdminBundle\\Model\\RoleModule', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRoleModuleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRoleModuleQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRoleModuleQuery) {
            return $criteria;
        }
        $query = new ChildRoleModuleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRoleModule|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RoleModuleTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RoleModuleTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRoleModule A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT usrmrl_id, usrmrl_label, usrmrl_bundle, usrmrl_created_at, usrmrl_updated_at FROM user_role_module_usrmrl WHERE usrmrl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRoleModule $obj */
            $obj = new ChildRoleModule();
            $obj->hydrate($row);
            RoleModuleTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRoleModule|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the usrmrl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE usrmrl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE usrmrl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE usrmrl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the usrmrl_label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE usrmrl_label = 'fooValue'
     * $query->filterByLabel('%fooValue%', Criteria::LIKE); // WHERE usrmrl_label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the usrmrl_bundle column
     *
     * Example usage:
     * <code>
     * $query->filterByBundle('fooValue');   // WHERE usrmrl_bundle = 'fooValue'
     * $query->filterByBundle('%fooValue%', Criteria::LIKE); // WHERE usrmrl_bundle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bundle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function filterByBundle($bundle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bundle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_BUNDLE, $bundle, $comparison);
    }

    /**
     * Filter the query on the usrmrl_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE usrmrl_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE usrmrl_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE usrmrl_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the usrmrl_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE usrmrl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE usrmrl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE usrmrl_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\Role object
     *
     * @param \IiMedias\AdminBundle\Model\Role|ObjectCollection $role the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRoleModuleQuery The current query, for fluid interface
     */
    public function filterByRole($role, $comparison = null)
    {
        if ($role instanceof \IiMedias\AdminBundle\Model\Role) {
            return $this
                ->addUsingAlias(RoleModuleTableMap::COL_USRMRL_ID, $role->getRoleModuleId(), $comparison);
        } elseif ($role instanceof ObjectCollection) {
            return $this
                ->useRoleQuery()
                ->filterByPrimaryKeys($role->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRole() only accepts arguments of type \IiMedias\AdminBundle\Model\Role or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Role relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function joinRole($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Role');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Role');
        }

        return $this;
    }

    /**
     * Use the Role relation Role object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\RoleQuery A secondary query class using the current class as primary query
     */
    public function useRoleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRole($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Role', '\IiMedias\AdminBundle\Model\RoleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRoleModule $roleModule Object to remove from the list of results
     *
     * @return $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function prune($roleModule = null)
    {
        if ($roleModule) {
            $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_ID, $roleModule->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user_role_module_usrmrl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RoleModuleTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RoleModuleTableMap::clearInstancePool();
            RoleModuleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RoleModuleTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RoleModuleTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RoleModuleTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RoleModuleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(RoleModuleTableMap::COL_USRMRL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(RoleModuleTableMap::COL_USRMRL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(RoleModuleTableMap::COL_USRMRL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(RoleModuleTableMap::COL_USRMRL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildRoleModuleQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(RoleModuleTableMap::COL_USRMRL_CREATED_AT);
    }

} // RoleModuleQuery
