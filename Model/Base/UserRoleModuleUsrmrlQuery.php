<?php

namespace IiMedias\AdminBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\UserRoleModuleUsrmrl as ChildUserRoleModuleUsrmrl;
use IiMedias\AdminBundle\Model\UserRoleModuleUsrmrlQuery as ChildUserRoleModuleUsrmrlQuery;
use IiMedias\AdminBundle\Model\Map\UserRoleModuleUsrmrlTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_role_module_usrmrl' table.
 *
 *
 *
 * @method     ChildUserRoleModuleUsrmrlQuery orderById($order = Criteria::ASC) Order by the usrmrl_id column
 * @method     ChildUserRoleModuleUsrmrlQuery orderByLabel($order = Criteria::ASC) Order by the usrmrl_label column
 * @method     ChildUserRoleModuleUsrmrlQuery orderByBundle($order = Criteria::ASC) Order by the usrmrl_bundle column
 * @method     ChildUserRoleModuleUsrmrlQuery orderByCreatedAt($order = Criteria::ASC) Order by the usrmrl_created_at column
 * @method     ChildUserRoleModuleUsrmrlQuery orderByUpdatedAt($order = Criteria::ASC) Order by the usrmrl_updated_at column
 *
 * @method     ChildUserRoleModuleUsrmrlQuery groupById() Group by the usrmrl_id column
 * @method     ChildUserRoleModuleUsrmrlQuery groupByLabel() Group by the usrmrl_label column
 * @method     ChildUserRoleModuleUsrmrlQuery groupByBundle() Group by the usrmrl_bundle column
 * @method     ChildUserRoleModuleUsrmrlQuery groupByCreatedAt() Group by the usrmrl_created_at column
 * @method     ChildUserRoleModuleUsrmrlQuery groupByUpdatedAt() Group by the usrmrl_updated_at column
 *
 * @method     ChildUserRoleModuleUsrmrlQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserRoleModuleUsrmrlQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserRoleModuleUsrmrlQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserRoleModuleUsrmrlQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserRoleModuleUsrmrlQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserRoleModuleUsrmrlQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserRoleModuleUsrmrl findOne(ConnectionInterface $con = null) Return the first ChildUserRoleModuleUsrmrl matching the query
 * @method     ChildUserRoleModuleUsrmrl findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserRoleModuleUsrmrl matching the query, or a new ChildUserRoleModuleUsrmrl object populated from the query conditions when no match is found
 *
 * @method     ChildUserRoleModuleUsrmrl findOneById(int $usrmrl_id) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_id column
 * @method     ChildUserRoleModuleUsrmrl findOneByLabel(string $usrmrl_label) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_label column
 * @method     ChildUserRoleModuleUsrmrl findOneByBundle(string $usrmrl_bundle) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_bundle column
 * @method     ChildUserRoleModuleUsrmrl findOneByCreatedAt(string $usrmrl_created_at) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_created_at column
 * @method     ChildUserRoleModuleUsrmrl findOneByUpdatedAt(string $usrmrl_updated_at) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_updated_at column *

 * @method     ChildUserRoleModuleUsrmrl requirePk($key, ConnectionInterface $con = null) Return the ChildUserRoleModuleUsrmrl by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserRoleModuleUsrmrl requireOne(ConnectionInterface $con = null) Return the first ChildUserRoleModuleUsrmrl matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserRoleModuleUsrmrl requireOneById(int $usrmrl_id) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserRoleModuleUsrmrl requireOneByLabel(string $usrmrl_label) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserRoleModuleUsrmrl requireOneByBundle(string $usrmrl_bundle) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_bundle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserRoleModuleUsrmrl requireOneByCreatedAt(string $usrmrl_created_at) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserRoleModuleUsrmrl requireOneByUpdatedAt(string $usrmrl_updated_at) Return the first ChildUserRoleModuleUsrmrl filtered by the usrmrl_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserRoleModuleUsrmrl[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserRoleModuleUsrmrl objects based on current ModelCriteria
 * @method     ChildUserRoleModuleUsrmrl[]|ObjectCollection findById(int $usrmrl_id) Return ChildUserRoleModuleUsrmrl objects filtered by the usrmrl_id column
 * @method     ChildUserRoleModuleUsrmrl[]|ObjectCollection findByLabel(string $usrmrl_label) Return ChildUserRoleModuleUsrmrl objects filtered by the usrmrl_label column
 * @method     ChildUserRoleModuleUsrmrl[]|ObjectCollection findByBundle(string $usrmrl_bundle) Return ChildUserRoleModuleUsrmrl objects filtered by the usrmrl_bundle column
 * @method     ChildUserRoleModuleUsrmrl[]|ObjectCollection findByCreatedAt(string $usrmrl_created_at) Return ChildUserRoleModuleUsrmrl objects filtered by the usrmrl_created_at column
 * @method     ChildUserRoleModuleUsrmrl[]|ObjectCollection findByUpdatedAt(string $usrmrl_updated_at) Return ChildUserRoleModuleUsrmrl objects filtered by the usrmrl_updated_at column
 * @method     ChildUserRoleModuleUsrmrl[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserRoleModuleUsrmrlQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AdminBundle\Model\Base\UserRoleModuleUsrmrlQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AdminBundle\\Model\\UserRoleModuleUsrmrl', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserRoleModuleUsrmrlQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserRoleModuleUsrmrlQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserRoleModuleUsrmrlQuery) {
            return $criteria;
        }
        $query = new ChildUserRoleModuleUsrmrlQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserRoleModuleUsrmrl|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserRoleModuleUsrmrlTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserRoleModuleUsrmrlTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserRoleModuleUsrmrl A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT usrmrl_id, usrmrl_label, usrmrl_bundle, usrmrl_created_at, usrmrl_updated_at FROM user_role_module_usrmrl WHERE usrmrl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserRoleModuleUsrmrl $obj */
            $obj = new ChildUserRoleModuleUsrmrl();
            $obj->hydrate($row);
            UserRoleModuleUsrmrlTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserRoleModuleUsrmrl|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserRoleModuleUsrmrlQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserRoleModuleUsrmrlQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the usrmrl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE usrmrl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE usrmrl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE usrmrl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserRoleModuleUsrmrlQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the usrmrl_label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE usrmrl_label = 'fooValue'
     * $query->filterByLabel('%fooValue%'); // WHERE usrmrl_label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserRoleModuleUsrmrlQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the usrmrl_bundle column
     *
     * Example usage:
     * <code>
     * $query->filterByBundle('fooValue');   // WHERE usrmrl_bundle = 'fooValue'
     * $query->filterByBundle('%fooValue%'); // WHERE usrmrl_bundle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bundle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserRoleModuleUsrmrlQuery The current query, for fluid interface
     */
    public function filterByBundle($bundle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bundle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_BUNDLE, $bundle, $comparison);
    }

    /**
     * Filter the query on the usrmrl_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE usrmrl_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE usrmrl_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE usrmrl_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserRoleModuleUsrmrlQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the usrmrl_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE usrmrl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE usrmrl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE usrmrl_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserRoleModuleUsrmrlQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserRoleModuleUsrmrl $userRoleModuleUsrmrl Object to remove from the list of results
     *
     * @return $this|ChildUserRoleModuleUsrmrlQuery The current query, for fluid interface
     */
    public function prune($userRoleModuleUsrmrl = null)
    {
        if ($userRoleModuleUsrmrl) {
            $this->addUsingAlias(UserRoleModuleUsrmrlTableMap::COL_USRMRL_ID, $userRoleModuleUsrmrl->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user_role_module_usrmrl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserRoleModuleUsrmrlTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserRoleModuleUsrmrlTableMap::clearInstancePool();
            UserRoleModuleUsrmrlTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserRoleModuleUsrmrlTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserRoleModuleUsrmrlTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserRoleModuleUsrmrlTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserRoleModuleUsrmrlTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserRoleModuleUsrmrlQuery
