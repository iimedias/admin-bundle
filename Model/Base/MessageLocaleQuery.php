<?php

namespace IiMedias\AdminBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\MessageLocale as ChildMessageLocale;
use IiMedias\AdminBundle\Model\MessageLocaleQuery as ChildMessageLocaleQuery;
use IiMedias\AdminBundle\Model\Map\MessageLocaleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'lang_message_label_lamsgl' table.
 *
 *
 *
 * @method     ChildMessageLocaleQuery orderById($order = Criteria::ASC) Order by the lamsgl_id column
 * @method     ChildMessageLocaleQuery orderByMessageId($order = Criteria::ASC) Order by the lamsgl_lanmsg_id column
 * @method     ChildMessageLocaleQuery orderByLocaleId($order = Criteria::ASC) Order by the lamsgl_lanloc_id column
 * @method     ChildMessageLocaleQuery orderByLabel($order = Criteria::ASC) Order by the lamsgl_label column
 * @method     ChildMessageLocaleQuery orderByCreatedAt($order = Criteria::ASC) Order by the lamsgl_created_at column
 * @method     ChildMessageLocaleQuery orderByUpdatedAt($order = Criteria::ASC) Order by the lamsgl_updated_at column
 *
 * @method     ChildMessageLocaleQuery groupById() Group by the lamsgl_id column
 * @method     ChildMessageLocaleQuery groupByMessageId() Group by the lamsgl_lanmsg_id column
 * @method     ChildMessageLocaleQuery groupByLocaleId() Group by the lamsgl_lanloc_id column
 * @method     ChildMessageLocaleQuery groupByLabel() Group by the lamsgl_label column
 * @method     ChildMessageLocaleQuery groupByCreatedAt() Group by the lamsgl_created_at column
 * @method     ChildMessageLocaleQuery groupByUpdatedAt() Group by the lamsgl_updated_at column
 *
 * @method     ChildMessageLocaleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMessageLocaleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMessageLocaleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMessageLocaleQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMessageLocaleQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMessageLocaleQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMessageLocaleQuery leftJoinMessage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Message relation
 * @method     ChildMessageLocaleQuery rightJoinMessage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Message relation
 * @method     ChildMessageLocaleQuery innerJoinMessage($relationAlias = null) Adds a INNER JOIN clause to the query using the Message relation
 *
 * @method     ChildMessageLocaleQuery joinWithMessage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Message relation
 *
 * @method     ChildMessageLocaleQuery leftJoinWithMessage() Adds a LEFT JOIN clause and with to the query using the Message relation
 * @method     ChildMessageLocaleQuery rightJoinWithMessage() Adds a RIGHT JOIN clause and with to the query using the Message relation
 * @method     ChildMessageLocaleQuery innerJoinWithMessage() Adds a INNER JOIN clause and with to the query using the Message relation
 *
 * @method     ChildMessageLocaleQuery leftJoinLocale($relationAlias = null) Adds a LEFT JOIN clause to the query using the Locale relation
 * @method     ChildMessageLocaleQuery rightJoinLocale($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Locale relation
 * @method     ChildMessageLocaleQuery innerJoinLocale($relationAlias = null) Adds a INNER JOIN clause to the query using the Locale relation
 *
 * @method     ChildMessageLocaleQuery joinWithLocale($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Locale relation
 *
 * @method     ChildMessageLocaleQuery leftJoinWithLocale() Adds a LEFT JOIN clause and with to the query using the Locale relation
 * @method     ChildMessageLocaleQuery rightJoinWithLocale() Adds a RIGHT JOIN clause and with to the query using the Locale relation
 * @method     ChildMessageLocaleQuery innerJoinWithLocale() Adds a INNER JOIN clause and with to the query using the Locale relation
 *
 * @method     \IiMedias\AdminBundle\Model\MessageQuery|\IiMedias\AdminBundle\Model\LocaleQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMessageLocale findOne(ConnectionInterface $con = null) Return the first ChildMessageLocale matching the query
 * @method     ChildMessageLocale findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMessageLocale matching the query, or a new ChildMessageLocale object populated from the query conditions when no match is found
 *
 * @method     ChildMessageLocale findOneById(int $lamsgl_id) Return the first ChildMessageLocale filtered by the lamsgl_id column
 * @method     ChildMessageLocale findOneByMessageId(int $lamsgl_lanmsg_id) Return the first ChildMessageLocale filtered by the lamsgl_lanmsg_id column
 * @method     ChildMessageLocale findOneByLocaleId(int $lamsgl_lanloc_id) Return the first ChildMessageLocale filtered by the lamsgl_lanloc_id column
 * @method     ChildMessageLocale findOneByLabel(string $lamsgl_label) Return the first ChildMessageLocale filtered by the lamsgl_label column
 * @method     ChildMessageLocale findOneByCreatedAt(string $lamsgl_created_at) Return the first ChildMessageLocale filtered by the lamsgl_created_at column
 * @method     ChildMessageLocale findOneByUpdatedAt(string $lamsgl_updated_at) Return the first ChildMessageLocale filtered by the lamsgl_updated_at column *

 * @method     ChildMessageLocale requirePk($key, ConnectionInterface $con = null) Return the ChildMessageLocale by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageLocale requireOne(ConnectionInterface $con = null) Return the first ChildMessageLocale matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMessageLocale requireOneById(int $lamsgl_id) Return the first ChildMessageLocale filtered by the lamsgl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageLocale requireOneByMessageId(int $lamsgl_lanmsg_id) Return the first ChildMessageLocale filtered by the lamsgl_lanmsg_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageLocale requireOneByLocaleId(int $lamsgl_lanloc_id) Return the first ChildMessageLocale filtered by the lamsgl_lanloc_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageLocale requireOneByLabel(string $lamsgl_label) Return the first ChildMessageLocale filtered by the lamsgl_label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageLocale requireOneByCreatedAt(string $lamsgl_created_at) Return the first ChildMessageLocale filtered by the lamsgl_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessageLocale requireOneByUpdatedAt(string $lamsgl_updated_at) Return the first ChildMessageLocale filtered by the lamsgl_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMessageLocale[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMessageLocale objects based on current ModelCriteria
 * @method     ChildMessageLocale[]|ObjectCollection findById(int $lamsgl_id) Return ChildMessageLocale objects filtered by the lamsgl_id column
 * @method     ChildMessageLocale[]|ObjectCollection findByMessageId(int $lamsgl_lanmsg_id) Return ChildMessageLocale objects filtered by the lamsgl_lanmsg_id column
 * @method     ChildMessageLocale[]|ObjectCollection findByLocaleId(int $lamsgl_lanloc_id) Return ChildMessageLocale objects filtered by the lamsgl_lanloc_id column
 * @method     ChildMessageLocale[]|ObjectCollection findByLabel(string $lamsgl_label) Return ChildMessageLocale objects filtered by the lamsgl_label column
 * @method     ChildMessageLocale[]|ObjectCollection findByCreatedAt(string $lamsgl_created_at) Return ChildMessageLocale objects filtered by the lamsgl_created_at column
 * @method     ChildMessageLocale[]|ObjectCollection findByUpdatedAt(string $lamsgl_updated_at) Return ChildMessageLocale objects filtered by the lamsgl_updated_at column
 * @method     ChildMessageLocale[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MessageLocaleQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AdminBundle\Model\Base\MessageLocaleQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AdminBundle\\Model\\MessageLocale', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMessageLocaleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMessageLocaleQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMessageLocaleQuery) {
            return $criteria;
        }
        $query = new ChildMessageLocaleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMessageLocale|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MessageLocaleTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MessageLocaleTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageLocale A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT lamsgl_id, lamsgl_lanmsg_id, lamsgl_lanloc_id, lamsgl_label, lamsgl_created_at, lamsgl_updated_at FROM lang_message_label_lamsgl WHERE lamsgl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMessageLocale $obj */
            $obj = new ChildMessageLocale();
            $obj->hydrate($row);
            MessageLocaleTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMessageLocale|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the lamsgl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE lamsgl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE lamsgl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE lamsgl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the lamsgl_lanmsg_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageId(1234); // WHERE lamsgl_lanmsg_id = 1234
     * $query->filterByMessageId(array(12, 34)); // WHERE lamsgl_lanmsg_id IN (12, 34)
     * $query->filterByMessageId(array('min' => 12)); // WHERE lamsgl_lanmsg_id > 12
     * </code>
     *
     * @see       filterByMessage()
     *
     * @param     mixed $messageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByMessageId($messageId = null, $comparison = null)
    {
        if (is_array($messageId)) {
            $useMinMax = false;
            if (isset($messageId['min'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANMSG_ID, $messageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($messageId['max'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANMSG_ID, $messageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANMSG_ID, $messageId, $comparison);
    }

    /**
     * Filter the query on the lamsgl_lanloc_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLocaleId(1234); // WHERE lamsgl_lanloc_id = 1234
     * $query->filterByLocaleId(array(12, 34)); // WHERE lamsgl_lanloc_id IN (12, 34)
     * $query->filterByLocaleId(array('min' => 12)); // WHERE lamsgl_lanloc_id > 12
     * </code>
     *
     * @see       filterByLocale()
     *
     * @param     mixed $localeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByLocaleId($localeId = null, $comparison = null)
    {
        if (is_array($localeId)) {
            $useMinMax = false;
            if (isset($localeId['min'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANLOC_ID, $localeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($localeId['max'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANLOC_ID, $localeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANLOC_ID, $localeId, $comparison);
    }

    /**
     * Filter the query on the lamsgl_label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE lamsgl_label = 'fooValue'
     * $query->filterByLabel('%fooValue%', Criteria::LIKE); // WHERE lamsgl_label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the lamsgl_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE lamsgl_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE lamsgl_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE lamsgl_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the lamsgl_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE lamsgl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE lamsgl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE lamsgl_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\Message object
     *
     * @param \IiMedias\AdminBundle\Model\Message|ObjectCollection $message The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByMessage($message, $comparison = null)
    {
        if ($message instanceof \IiMedias\AdminBundle\Model\Message) {
            return $this
                ->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANMSG_ID, $message->getId(), $comparison);
        } elseif ($message instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANMSG_ID, $message->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMessage() only accepts arguments of type \IiMedias\AdminBundle\Model\Message or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Message relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function joinMessage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Message');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Message');
        }

        return $this;
    }

    /**
     * Use the Message relation Message object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\MessageQuery A secondary query class using the current class as primary query
     */
    public function useMessageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMessage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Message', '\IiMedias\AdminBundle\Model\MessageQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\Locale object
     *
     * @param \IiMedias\AdminBundle\Model\Locale|ObjectCollection $locale The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function filterByLocale($locale, $comparison = null)
    {
        if ($locale instanceof \IiMedias\AdminBundle\Model\Locale) {
            return $this
                ->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANLOC_ID, $locale->getId(), $comparison);
        } elseif ($locale instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_LANLOC_ID, $locale->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLocale() only accepts arguments of type \IiMedias\AdminBundle\Model\Locale or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Locale relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function joinLocale($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Locale');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Locale');
        }

        return $this;
    }

    /**
     * Use the Locale relation Locale object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\LocaleQuery A secondary query class using the current class as primary query
     */
    public function useLocaleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLocale($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Locale', '\IiMedias\AdminBundle\Model\LocaleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMessageLocale $messageLocale Object to remove from the list of results
     *
     * @return $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function prune($messageLocale = null)
    {
        if ($messageLocale) {
            $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_ID, $messageLocale->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the lang_message_label_lamsgl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MessageLocaleTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MessageLocaleTableMap::clearInstancePool();
            MessageLocaleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MessageLocaleTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MessageLocaleTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MessageLocaleTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MessageLocaleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(MessageLocaleTableMap::COL_LAMSGL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(MessageLocaleTableMap::COL_LAMSGL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(MessageLocaleTableMap::COL_LAMSGL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(MessageLocaleTableMap::COL_LAMSGL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildMessageLocaleQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(MessageLocaleTableMap::COL_LAMSGL_CREATED_AT);
    }

} // MessageLocaleQuery
