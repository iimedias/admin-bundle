<?php

namespace IiMedias\AdminBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\MenuGroup as ChildMenuGroup;
use IiMedias\AdminBundle\Model\MenuGroupQuery as ChildMenuGroupQuery;
use IiMedias\AdminBundle\Model\Map\MenuGroupTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'menu_group_mengrp' table.
 *
 *
 *
 * @method     ChildMenuGroupQuery orderById($order = Criteria::ASC) Order by the mengrp_id column
 * @method     ChildMenuGroupQuery orderByName($order = Criteria::ASC) Order by the mengrp_name column
 * @method     ChildMenuGroupQuery orderByCreatedAt($order = Criteria::ASC) Order by the mengrp_created_at column
 * @method     ChildMenuGroupQuery orderByUpdatedAt($order = Criteria::ASC) Order by the mengrp_updated_at column
 *
 * @method     ChildMenuGroupQuery groupById() Group by the mengrp_id column
 * @method     ChildMenuGroupQuery groupByName() Group by the mengrp_name column
 * @method     ChildMenuGroupQuery groupByCreatedAt() Group by the mengrp_created_at column
 * @method     ChildMenuGroupQuery groupByUpdatedAt() Group by the mengrp_updated_at column
 *
 * @method     ChildMenuGroupQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMenuGroupQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMenuGroupQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMenuGroupQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMenuGroupQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMenuGroupQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMenuGroupQuery leftJoinMenuElement($relationAlias = null) Adds a LEFT JOIN clause to the query using the MenuElement relation
 * @method     ChildMenuGroupQuery rightJoinMenuElement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MenuElement relation
 * @method     ChildMenuGroupQuery innerJoinMenuElement($relationAlias = null) Adds a INNER JOIN clause to the query using the MenuElement relation
 *
 * @method     ChildMenuGroupQuery joinWithMenuElement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MenuElement relation
 *
 * @method     ChildMenuGroupQuery leftJoinWithMenuElement() Adds a LEFT JOIN clause and with to the query using the MenuElement relation
 * @method     ChildMenuGroupQuery rightJoinWithMenuElement() Adds a RIGHT JOIN clause and with to the query using the MenuElement relation
 * @method     ChildMenuGroupQuery innerJoinWithMenuElement() Adds a INNER JOIN clause and with to the query using the MenuElement relation
 *
 * @method     \IiMedias\AdminBundle\Model\MenuElementQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMenuGroup findOne(ConnectionInterface $con = null) Return the first ChildMenuGroup matching the query
 * @method     ChildMenuGroup findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMenuGroup matching the query, or a new ChildMenuGroup object populated from the query conditions when no match is found
 *
 * @method     ChildMenuGroup findOneById(int $mengrp_id) Return the first ChildMenuGroup filtered by the mengrp_id column
 * @method     ChildMenuGroup findOneByName(string $mengrp_name) Return the first ChildMenuGroup filtered by the mengrp_name column
 * @method     ChildMenuGroup findOneByCreatedAt(string $mengrp_created_at) Return the first ChildMenuGroup filtered by the mengrp_created_at column
 * @method     ChildMenuGroup findOneByUpdatedAt(string $mengrp_updated_at) Return the first ChildMenuGroup filtered by the mengrp_updated_at column *

 * @method     ChildMenuGroup requirePk($key, ConnectionInterface $con = null) Return the ChildMenuGroup by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuGroup requireOne(ConnectionInterface $con = null) Return the first ChildMenuGroup matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMenuGroup requireOneById(int $mengrp_id) Return the first ChildMenuGroup filtered by the mengrp_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuGroup requireOneByName(string $mengrp_name) Return the first ChildMenuGroup filtered by the mengrp_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuGroup requireOneByCreatedAt(string $mengrp_created_at) Return the first ChildMenuGroup filtered by the mengrp_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMenuGroup requireOneByUpdatedAt(string $mengrp_updated_at) Return the first ChildMenuGroup filtered by the mengrp_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMenuGroup[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMenuGroup objects based on current ModelCriteria
 * @method     ChildMenuGroup[]|ObjectCollection findById(int $mengrp_id) Return ChildMenuGroup objects filtered by the mengrp_id column
 * @method     ChildMenuGroup[]|ObjectCollection findByName(string $mengrp_name) Return ChildMenuGroup objects filtered by the mengrp_name column
 * @method     ChildMenuGroup[]|ObjectCollection findByCreatedAt(string $mengrp_created_at) Return ChildMenuGroup objects filtered by the mengrp_created_at column
 * @method     ChildMenuGroup[]|ObjectCollection findByUpdatedAt(string $mengrp_updated_at) Return ChildMenuGroup objects filtered by the mengrp_updated_at column
 * @method     ChildMenuGroup[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MenuGroupQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AdminBundle\Model\Base\MenuGroupQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AdminBundle\\Model\\MenuGroup', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMenuGroupQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMenuGroupQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMenuGroupQuery) {
            return $criteria;
        }
        $query = new ChildMenuGroupQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMenuGroup|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MenuGroupTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MenuGroupTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMenuGroup A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT mengrp_id, mengrp_name, mengrp_created_at, mengrp_updated_at FROM menu_group_mengrp WHERE mengrp_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMenuGroup $obj */
            $obj = new ChildMenuGroup();
            $obj->hydrate($row);
            MenuGroupTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMenuGroup|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the mengrp_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE mengrp_id = 1234
     * $query->filterById(array(12, 34)); // WHERE mengrp_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE mengrp_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_ID, $id, $comparison);
    }

    /**
     * Filter the query on the mengrp_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE mengrp_name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE mengrp_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the mengrp_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE mengrp_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE mengrp_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE mengrp_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the mengrp_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE mengrp_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE mengrp_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE mengrp_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\MenuElement object
     *
     * @param \IiMedias\AdminBundle\Model\MenuElement|ObjectCollection $menuElement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMenuGroupQuery The current query, for fluid interface
     */
    public function filterByMenuElement($menuElement, $comparison = null)
    {
        if ($menuElement instanceof \IiMedias\AdminBundle\Model\MenuElement) {
            return $this
                ->addUsingAlias(MenuGroupTableMap::COL_MENGRP_ID, $menuElement->getMenuGroupId(), $comparison);
        } elseif ($menuElement instanceof ObjectCollection) {
            return $this
                ->useMenuElementQuery()
                ->filterByPrimaryKeys($menuElement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMenuElement() only accepts arguments of type \IiMedias\AdminBundle\Model\MenuElement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MenuElement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function joinMenuElement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MenuElement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MenuElement');
        }

        return $this;
    }

    /**
     * Use the MenuElement relation MenuElement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\MenuElementQuery A secondary query class using the current class as primary query
     */
    public function useMenuElementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMenuElement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MenuElement', '\IiMedias\AdminBundle\Model\MenuElementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMenuGroup $menuGroup Object to remove from the list of results
     *
     * @return $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function prune($menuGroup = null)
    {
        if ($menuGroup) {
            $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_ID, $menuGroup->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the menu_group_mengrp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MenuGroupTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MenuGroupTableMap::clearInstancePool();
            MenuGroupTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MenuGroupTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MenuGroupTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MenuGroupTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MenuGroupTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(MenuGroupTableMap::COL_MENGRP_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(MenuGroupTableMap::COL_MENGRP_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(MenuGroupTableMap::COL_MENGRP_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(MenuGroupTableMap::COL_MENGRP_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildMenuGroupQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(MenuGroupTableMap::COL_MENGRP_CREATED_AT);
    }

} // MenuGroupQuery
