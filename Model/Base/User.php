<?php

namespace IiMedias\AdminBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User as ChildUser;
use IiMedias\AdminBundle\Model\UserQuery as ChildUserQuery;
use IiMedias\AdminBundle\Model\Map\UserTableMap;
use IiMedias\StreamBundle\Model\Avatar;
use IiMedias\StreamBundle\Model\AvatarQuery;
use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\ChannelQuery;
use IiMedias\StreamBundle\Model\ChatUser;
use IiMedias\StreamBundle\Model\ChatUserQuery;
use IiMedias\StreamBundle\Model\Rank;
use IiMedias\StreamBundle\Model\RankQuery;
use IiMedias\StreamBundle\Model\Site;
use IiMedias\StreamBundle\Model\SiteQuery;
use IiMedias\StreamBundle\Model\Stream;
use IiMedias\StreamBundle\Model\StreamQuery;
use IiMedias\StreamBundle\Model\Base\Avatar as BaseAvatar;
use IiMedias\StreamBundle\Model\Base\Channel as BaseChannel;
use IiMedias\StreamBundle\Model\Base\ChatUser as BaseChatUser;
use IiMedias\StreamBundle\Model\Base\Rank as BaseRank;
use IiMedias\StreamBundle\Model\Base\Site as BaseSite;
use IiMedias\StreamBundle\Model\Base\Stream as BaseStream;
use IiMedias\StreamBundle\Model\Map\AvatarTableMap;
use IiMedias\StreamBundle\Model\Map\ChannelTableMap;
use IiMedias\StreamBundle\Model\Map\ChatUserTableMap;
use IiMedias\StreamBundle\Model\Map\RankTableMap;
use IiMedias\StreamBundle\Model\Map\SiteTableMap;
use IiMedias\StreamBundle\Model\Map\StreamTableMap;
use IiMedias\VideoGamesBundle\Model\ApiCount;
use IiMedias\VideoGamesBundle\Model\ApiCountQuery;
use IiMedias\VideoGamesBundle\Model\Base\ApiCount as BaseApiCount;
use IiMedias\VideoGamesBundle\Model\Map\ApiCountTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'user_user_usrusr' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.AdminBundle.Model.Base
 */
abstract class User implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\AdminBundle\\Model\\Map\\UserTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the usrusr_id field.
     *
     * @var        int
     */
    protected $usrusr_id;

    /**
     * The value for the usrusr_username field.
     *
     * @var        string
     */
    protected $usrusr_username;

    /**
     * The value for the usrusr_email field.
     *
     * @var        string
     */
    protected $usrusr_email;

    /**
     * The value for the usrusr_facebook_id field.
     *
     * @var        string
     */
    protected $usrusr_facebook_id;

    /**
     * The value for the usrusr_google_id field.
     *
     * @var        string
     */
    protected $usrusr_google_id;

    /**
     * The value for the usrusr_twitter_id field.
     *
     * @var        string
     */
    protected $usrusr_twitter_id;

    /**
     * The value for the usrusr_password field.
     *
     * @var        string
     */
    protected $usrusr_password;

    /**
     * The value for the usrusr_salt field.
     *
     * @var        string
     */
    protected $usrusr_salt;

    /**
     * The value for the usrusr_roles field.
     *
     * @var        array
     */
    protected $usrusr_roles;

    /**
     * The unserialized $usrusr_roles value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $usrusr_roles_unserialized;

    /**
     * The value for the usrusr_is_active field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $usrusr_is_active;

    /**
     * The value for the usrusr_created_at field.
     *
     * @var        DateTime
     */
    protected $usrusr_created_at;

    /**
     * The value for the usrusr_updated_at field.
     *
     * @var        DateTime
     */
    protected $usrusr_updated_at;

    /**
     * The value for the usrusr_slug field.
     *
     * @var        string
     */
    protected $usrusr_slug;

    /**
     * @var        ObjectCollection|Site[] Collection to store aggregation of Site objects.
     */
    protected $collCreatedByUserStsites;
    protected $collCreatedByUserStsitesPartial;

    /**
     * @var        ObjectCollection|Site[] Collection to store aggregation of Site objects.
     */
    protected $collUpdatedByUserStsites;
    protected $collUpdatedByUserStsitesPartial;

    /**
     * @var        ObjectCollection|Stream[] Collection to store aggregation of Stream objects.
     */
    protected $collCreatedByUserStstrms;
    protected $collCreatedByUserStstrmsPartial;

    /**
     * @var        ObjectCollection|Stream[] Collection to store aggregation of Stream objects.
     */
    protected $collUpdatedByUserStstrms;
    protected $collUpdatedByUserStstrmsPartial;

    /**
     * @var        ObjectCollection|Channel[] Collection to store aggregation of Channel objects.
     */
    protected $collCreatedByUserStchans;
    protected $collCreatedByUserStchansPartial;

    /**
     * @var        ObjectCollection|Channel[] Collection to store aggregation of Channel objects.
     */
    protected $collUpdatedByUserStchans;
    protected $collUpdatedByUserStchansPartial;

    /**
     * @var        ObjectCollection|ChatUser[] Collection to store aggregation of ChatUser objects.
     */
    protected $collCreatedByUserStcusrs;
    protected $collCreatedByUserStcusrsPartial;

    /**
     * @var        ObjectCollection|ChatUser[] Collection to store aggregation of ChatUser objects.
     */
    protected $collUpdatedByUserStcusrs;
    protected $collUpdatedByUserStcusrsPartial;

    /**
     * @var        ObjectCollection|Rank[] Collection to store aggregation of Rank objects.
     */
    protected $collCreatedByUserStranks;
    protected $collCreatedByUserStranksPartial;

    /**
     * @var        ObjectCollection|Rank[] Collection to store aggregation of Rank objects.
     */
    protected $collUpdatedByUserStranks;
    protected $collUpdatedByUserStranksPartial;

    /**
     * @var        ObjectCollection|Avatar[] Collection to store aggregation of Avatar objects.
     */
    protected $collCreatedByUserStavtrs;
    protected $collCreatedByUserStavtrsPartial;

    /**
     * @var        ObjectCollection|Avatar[] Collection to store aggregation of Avatar objects.
     */
    protected $collUpdatedByUserStavtrs;
    protected $collUpdatedByUserStavtrsPartial;

    /**
     * @var        ObjectCollection|ApiCount[] Collection to store aggregation of ApiCount objects.
     */
    protected $collCreatedByUserVideoGamesApiCounts;
    protected $collCreatedByUserVideoGamesApiCountsPartial;

    /**
     * @var        ObjectCollection|ApiCount[] Collection to store aggregation of ApiCount objects.
     */
    protected $collUpdatedByUserVideoGamesApiCounts;
    protected $collUpdatedByUserVideoGamesApiCountsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Site[]
     */
    protected $createdByUserStsitesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Site[]
     */
    protected $updatedByUserStsitesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Stream[]
     */
    protected $createdByUserStstrmsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Stream[]
     */
    protected $updatedByUserStstrmsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Channel[]
     */
    protected $createdByUserStchansScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Channel[]
     */
    protected $updatedByUserStchansScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChatUser[]
     */
    protected $createdByUserStcusrsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChatUser[]
     */
    protected $updatedByUserStcusrsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Rank[]
     */
    protected $createdByUserStranksScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Rank[]
     */
    protected $updatedByUserStranksScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Avatar[]
     */
    protected $createdByUserStavtrsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Avatar[]
     */
    protected $updatedByUserStavtrsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ApiCount[]
     */
    protected $createdByUserVideoGamesApiCountsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ApiCount[]
     */
    protected $updatedByUserVideoGamesApiCountsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->usrusr_is_active = false;
    }

    /**
     * Initializes internal state of IiMedias\AdminBundle\Model\Base\User object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>User</code> instance.  If
     * <code>obj</code> is an instance of <code>User</code>, delegates to
     * <code>equals(User)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|User The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [usrusr_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->usrusr_id;
    }

    /**
     * Get the [usrusr_username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->usrusr_username;
    }

    /**
     * Get the [usrusr_email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->usrusr_email;
    }

    /**
     * Get the [usrusr_facebook_id] column value.
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->usrusr_facebook_id;
    }

    /**
     * Get the [usrusr_google_id] column value.
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->usrusr_google_id;
    }

    /**
     * Get the [usrusr_twitter_id] column value.
     *
     * @return string
     */
    public function getTwitterId()
    {
        return $this->usrusr_twitter_id;
    }

    /**
     * Get the [usrusr_password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->usrusr_password;
    }

    /**
     * Get the [usrusr_salt] column value.
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->usrusr_salt;
    }

    /**
     * Get the [usrusr_roles] column value.
     *
     * @return array
     */
    public function getRoles()
    {
        if (null === $this->usrusr_roles_unserialized) {
            $this->usrusr_roles_unserialized = array();
        }
        if (!$this->usrusr_roles_unserialized && null !== $this->usrusr_roles) {
            $usrusr_roles_unserialized = substr($this->usrusr_roles, 2, -2);
            $this->usrusr_roles_unserialized = '' !== $usrusr_roles_unserialized ? explode(' | ', $usrusr_roles_unserialized) : array();
        }

        return $this->usrusr_roles_unserialized;
    }

    /**
     * Test the presence of a value in the [usrusr_roles] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasRole($value)
    {
        return in_array($value, $this->getRoles());
    } // hasRole()

    /**
     * Get the [usrusr_is_active] column value.
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->usrusr_is_active;
    }

    /**
     * Get the [usrusr_is_active] column value.
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->getIsActive();
    }

    /**
     * Get the [optionally formatted] temporal [usrusr_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->usrusr_created_at;
        } else {
            return $this->usrusr_created_at instanceof \DateTimeInterface ? $this->usrusr_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [usrusr_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->usrusr_updated_at;
        } else {
            return $this->usrusr_updated_at instanceof \DateTimeInterface ? $this->usrusr_updated_at->format($format) : null;
        }
    }

    /**
     * Get the [usrusr_slug] column value.
     *
     * @return string
     */
    public function getUsrusrSlug()
    {
        return $this->usrusr_slug;
    }

    /**
     * Set the value of [usrusr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usrusr_id !== $v) {
            $this->usrusr_id = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [usrusr_username] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usrusr_username !== $v) {
            $this->usrusr_username = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [usrusr_email] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usrusr_email !== $v) {
            $this->usrusr_email = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [usrusr_facebook_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setFacebookId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usrusr_facebook_id !== $v) {
            $this->usrusr_facebook_id = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_FACEBOOK_ID] = true;
        }

        return $this;
    } // setFacebookId()

    /**
     * Set the value of [usrusr_google_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setGoogleId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usrusr_google_id !== $v) {
            $this->usrusr_google_id = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_GOOGLE_ID] = true;
        }

        return $this;
    } // setGoogleId()

    /**
     * Set the value of [usrusr_twitter_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setTwitterId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usrusr_twitter_id !== $v) {
            $this->usrusr_twitter_id = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_TWITTER_ID] = true;
        }

        return $this;
    } // setTwitterId()

    /**
     * Set the value of [usrusr_password] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usrusr_password !== $v) {
            $this->usrusr_password = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [usrusr_salt] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setSalt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usrusr_salt !== $v) {
            $this->usrusr_salt = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_SALT] = true;
        }

        return $this;
    } // setSalt()

    /**
     * Set the value of [usrusr_roles] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setRoles($v)
    {
        if ($this->usrusr_roles_unserialized !== $v) {
            $this->usrusr_roles_unserialized = $v;
            $this->usrusr_roles = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[UserTableMap::COL_USRUSR_ROLES] = true;
        }

        return $this;
    } // setRoles()

    /**
     * Adds a value to the [usrusr_roles] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addRole($value)
    {
        $currentArray = $this->getRoles();
        $currentArray []= $value;
        $this->setRoles($currentArray);

        return $this;
    } // addRole()

    /**
     * Removes a value from the [usrusr_roles] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function removeRole($value)
    {
        $targetArray = array();
        foreach ($this->getRoles() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setRoles($targetArray);

        return $this;
    } // removeRole()

    /**
     * Sets the value of the [usrusr_is_active] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setIsActive($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->usrusr_is_active !== $v) {
            $this->usrusr_is_active = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_IS_ACTIVE] = true;
        }

        return $this;
    } // setIsActive()

    /**
     * Sets the value of [usrusr_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->usrusr_created_at !== null || $dt !== null) {
            if ($this->usrusr_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->usrusr_created_at->format("Y-m-d H:i:s.u")) {
                $this->usrusr_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_USRUSR_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [usrusr_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->usrusr_updated_at !== null || $dt !== null) {
            if ($this->usrusr_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->usrusr_updated_at->format("Y-m-d H:i:s.u")) {
                $this->usrusr_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_USRUSR_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Set the value of [usrusr_slug] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function setUsrusrSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usrusr_slug !== $v) {
            $this->usrusr_slug = $v;
            $this->modifiedColumns[UserTableMap::COL_USRUSR_SLUG] = true;
        }

        return $this;
    } // setUsrusrSlug()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->usrusr_is_active !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UserTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UserTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UserTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UserTableMap::translateFieldName('FacebookId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_facebook_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UserTableMap::translateFieldName('GoogleId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_google_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UserTableMap::translateFieldName('TwitterId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_twitter_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UserTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UserTableMap::translateFieldName('Salt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_salt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UserTableMap::translateFieldName('Roles', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_roles = $col;
            $this->usrusr_roles_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UserTableMap::translateFieldName('IsActive', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_is_active = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : UserTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->usrusr_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : UserTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->usrusr_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : UserTableMap::translateFieldName('UsrusrSlug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usrusr_slug = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = UserTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\AdminBundle\\Model\\User'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUserQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCreatedByUserStsites = null;

            $this->collUpdatedByUserStsites = null;

            $this->collCreatedByUserStstrms = null;

            $this->collUpdatedByUserStstrms = null;

            $this->collCreatedByUserStchans = null;

            $this->collUpdatedByUserStchans = null;

            $this->collCreatedByUserStcusrs = null;

            $this->collUpdatedByUserStcusrs = null;

            $this->collCreatedByUserStranks = null;

            $this->collUpdatedByUserStranks = null;

            $this->collCreatedByUserStavtrs = null;

            $this->collUpdatedByUserStavtrs = null;

            $this->collCreatedByUserVideoGamesApiCounts = null;

            $this->collUpdatedByUserVideoGamesApiCounts = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see User::setDeleted()
     * @see User::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            // sluggable behavior

            if ($this->isColumnModified(UserTableMap::COL_USRUSR_SLUG) && $this->getUsrusrSlug()) {
                $this->setUsrusrSlug($this->makeSlugUnique($this->getUsrusrSlug()));
            } elseif (!$this->getUsrusrSlug()) {
                $this->setUsrusrSlug($this->createSlug());
            }
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(UserTableMap::COL_USRUSR_CREATED_AT)) {
                    $this->setCreatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
                if (!$this->isColumnModified(UserTableMap::COL_USRUSR_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(UserTableMap::COL_USRUSR_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->createdByUserStsitesScheduledForDeletion !== null) {
                if (!$this->createdByUserStsitesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\SiteQuery::create()
                        ->filterByPrimaryKeys($this->createdByUserStsitesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->createdByUserStsitesScheduledForDeletion = null;
                }
            }

            if ($this->collCreatedByUserStsites !== null) {
                foreach ($this->collCreatedByUserStsites as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->updatedByUserStsitesScheduledForDeletion !== null) {
                if (!$this->updatedByUserStsitesScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\SiteQuery::create()
                        ->filterByPrimaryKeys($this->updatedByUserStsitesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->updatedByUserStsitesScheduledForDeletion = null;
                }
            }

            if ($this->collUpdatedByUserStsites !== null) {
                foreach ($this->collUpdatedByUserStsites as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->createdByUserStstrmsScheduledForDeletion !== null) {
                if (!$this->createdByUserStstrmsScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\StreamQuery::create()
                        ->filterByPrimaryKeys($this->createdByUserStstrmsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->createdByUserStstrmsScheduledForDeletion = null;
                }
            }

            if ($this->collCreatedByUserStstrms !== null) {
                foreach ($this->collCreatedByUserStstrms as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->updatedByUserStstrmsScheduledForDeletion !== null) {
                if (!$this->updatedByUserStstrmsScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\StreamQuery::create()
                        ->filterByPrimaryKeys($this->updatedByUserStstrmsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->updatedByUserStstrmsScheduledForDeletion = null;
                }
            }

            if ($this->collUpdatedByUserStstrms !== null) {
                foreach ($this->collUpdatedByUserStstrms as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->createdByUserStchansScheduledForDeletion !== null) {
                if (!$this->createdByUserStchansScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChannelQuery::create()
                        ->filterByPrimaryKeys($this->createdByUserStchansScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->createdByUserStchansScheduledForDeletion = null;
                }
            }

            if ($this->collCreatedByUserStchans !== null) {
                foreach ($this->collCreatedByUserStchans as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->updatedByUserStchansScheduledForDeletion !== null) {
                if (!$this->updatedByUserStchansScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChannelQuery::create()
                        ->filterByPrimaryKeys($this->updatedByUserStchansScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->updatedByUserStchansScheduledForDeletion = null;
                }
            }

            if ($this->collUpdatedByUserStchans !== null) {
                foreach ($this->collUpdatedByUserStchans as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->createdByUserStcusrsScheduledForDeletion !== null) {
                if (!$this->createdByUserStcusrsScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChatUserQuery::create()
                        ->filterByPrimaryKeys($this->createdByUserStcusrsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->createdByUserStcusrsScheduledForDeletion = null;
                }
            }

            if ($this->collCreatedByUserStcusrs !== null) {
                foreach ($this->collCreatedByUserStcusrs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->updatedByUserStcusrsScheduledForDeletion !== null) {
                if (!$this->updatedByUserStcusrsScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\ChatUserQuery::create()
                        ->filterByPrimaryKeys($this->updatedByUserStcusrsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->updatedByUserStcusrsScheduledForDeletion = null;
                }
            }

            if ($this->collUpdatedByUserStcusrs !== null) {
                foreach ($this->collUpdatedByUserStcusrs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->createdByUserStranksScheduledForDeletion !== null) {
                if (!$this->createdByUserStranksScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\RankQuery::create()
                        ->filterByPrimaryKeys($this->createdByUserStranksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->createdByUserStranksScheduledForDeletion = null;
                }
            }

            if ($this->collCreatedByUserStranks !== null) {
                foreach ($this->collCreatedByUserStranks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->updatedByUserStranksScheduledForDeletion !== null) {
                if (!$this->updatedByUserStranksScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\RankQuery::create()
                        ->filterByPrimaryKeys($this->updatedByUserStranksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->updatedByUserStranksScheduledForDeletion = null;
                }
            }

            if ($this->collUpdatedByUserStranks !== null) {
                foreach ($this->collUpdatedByUserStranks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->createdByUserStavtrsScheduledForDeletion !== null) {
                if (!$this->createdByUserStavtrsScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\AvatarQuery::create()
                        ->filterByPrimaryKeys($this->createdByUserStavtrsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->createdByUserStavtrsScheduledForDeletion = null;
                }
            }

            if ($this->collCreatedByUserStavtrs !== null) {
                foreach ($this->collCreatedByUserStavtrs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->updatedByUserStavtrsScheduledForDeletion !== null) {
                if (!$this->updatedByUserStavtrsScheduledForDeletion->isEmpty()) {
                    \IiMedias\StreamBundle\Model\AvatarQuery::create()
                        ->filterByPrimaryKeys($this->updatedByUserStavtrsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->updatedByUserStavtrsScheduledForDeletion = null;
                }
            }

            if ($this->collUpdatedByUserStavtrs !== null) {
                foreach ($this->collUpdatedByUserStavtrs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->createdByUserVideoGamesApiCountsScheduledForDeletion !== null) {
                if (!$this->createdByUserVideoGamesApiCountsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiCountQuery::create()
                        ->filterByPrimaryKeys($this->createdByUserVideoGamesApiCountsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->createdByUserVideoGamesApiCountsScheduledForDeletion = null;
                }
            }

            if ($this->collCreatedByUserVideoGamesApiCounts !== null) {
                foreach ($this->collCreatedByUserVideoGamesApiCounts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->updatedByUserVideoGamesApiCountsScheduledForDeletion !== null) {
                if (!$this->updatedByUserVideoGamesApiCountsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiCountQuery::create()
                        ->filterByPrimaryKeys($this->updatedByUserVideoGamesApiCountsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->updatedByUserVideoGamesApiCountsScheduledForDeletion = null;
                }
            }

            if ($this->collUpdatedByUserVideoGamesApiCounts !== null) {
                foreach ($this->collUpdatedByUserVideoGamesApiCounts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UserTableMap::COL_USRUSR_ID] = true;
        if (null !== $this->usrusr_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserTableMap::COL_USRUSR_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_id';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_username';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_email';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_FACEBOOK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_facebook_id';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_GOOGLE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_google_id';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_TWITTER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_twitter_id';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_password';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_SALT)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_salt';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_ROLES)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_roles';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_IS_ACTIVE)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_is_active';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_created_at';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_updated_at';
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'usrusr_slug';
        }

        $sql = sprintf(
            'INSERT INTO user_user_usrusr (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'usrusr_id':
                        $stmt->bindValue($identifier, $this->usrusr_id, PDO::PARAM_INT);
                        break;
                    case 'usrusr_username':
                        $stmt->bindValue($identifier, $this->usrusr_username, PDO::PARAM_STR);
                        break;
                    case 'usrusr_email':
                        $stmt->bindValue($identifier, $this->usrusr_email, PDO::PARAM_STR);
                        break;
                    case 'usrusr_facebook_id':
                        $stmt->bindValue($identifier, $this->usrusr_facebook_id, PDO::PARAM_STR);
                        break;
                    case 'usrusr_google_id':
                        $stmt->bindValue($identifier, $this->usrusr_google_id, PDO::PARAM_STR);
                        break;
                    case 'usrusr_twitter_id':
                        $stmt->bindValue($identifier, $this->usrusr_twitter_id, PDO::PARAM_STR);
                        break;
                    case 'usrusr_password':
                        $stmt->bindValue($identifier, $this->usrusr_password, PDO::PARAM_STR);
                        break;
                    case 'usrusr_salt':
                        $stmt->bindValue($identifier, $this->usrusr_salt, PDO::PARAM_STR);
                        break;
                    case 'usrusr_roles':
                        $stmt->bindValue($identifier, $this->usrusr_roles, PDO::PARAM_STR);
                        break;
                    case 'usrusr_is_active':
                        $stmt->bindValue($identifier, (int) $this->usrusr_is_active, PDO::PARAM_INT);
                        break;
                    case 'usrusr_created_at':
                        $stmt->bindValue($identifier, $this->usrusr_created_at ? $this->usrusr_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usrusr_updated_at':
                        $stmt->bindValue($identifier, $this->usrusr_updated_at ? $this->usrusr_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usrusr_slug':
                        $stmt->bindValue($identifier, $this->usrusr_slug, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getEmail();
                break;
            case 3:
                return $this->getFacebookId();
                break;
            case 4:
                return $this->getGoogleId();
                break;
            case 5:
                return $this->getTwitterId();
                break;
            case 6:
                return $this->getPassword();
                break;
            case 7:
                return $this->getSalt();
                break;
            case 8:
                return $this->getRoles();
                break;
            case 9:
                return $this->getIsActive();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            case 12:
                return $this->getUsrusrSlug();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['User'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['User'][$this->hashCode()] = true;
        $keys = UserTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getEmail(),
            $keys[3] => $this->getFacebookId(),
            $keys[4] => $this->getGoogleId(),
            $keys[5] => $this->getTwitterId(),
            $keys[6] => $this->getPassword(),
            $keys[7] => $this->getSalt(),
            $keys[8] => $this->getRoles(),
            $keys[9] => $this->getIsActive(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
            $keys[12] => $this->getUsrusrSlug(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collCreatedByUserStsites) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sites';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_site_stsites';
                        break;
                    default:
                        $key = 'CreatedByUserStsites';
                }

                $result[$key] = $this->collCreatedByUserStsites->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpdatedByUserStsites) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sites';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_site_stsites';
                        break;
                    default:
                        $key = 'UpdatedByUserStsites';
                }

                $result[$key] = $this->collUpdatedByUserStsites->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCreatedByUserStstrms) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'streams';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_stream_ststrms';
                        break;
                    default:
                        $key = 'CreatedByUserStstrms';
                }

                $result[$key] = $this->collCreatedByUserStstrms->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpdatedByUserStstrms) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'streams';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_stream_ststrms';
                        break;
                    default:
                        $key = 'UpdatedByUserStstrms';
                }

                $result[$key] = $this->collUpdatedByUserStstrms->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCreatedByUserStchans) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'channels';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_channel_stchans';
                        break;
                    default:
                        $key = 'CreatedByUserStchans';
                }

                $result[$key] = $this->collCreatedByUserStchans->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpdatedByUserStchans) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'channels';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_channel_stchans';
                        break;
                    default:
                        $key = 'UpdatedByUserStchans';
                }

                $result[$key] = $this->collUpdatedByUserStchans->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCreatedByUserStcusrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chatUsers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_chat_user_stcusrs';
                        break;
                    default:
                        $key = 'CreatedByUserStcusrs';
                }

                $result[$key] = $this->collCreatedByUserStcusrs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpdatedByUserStcusrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chatUsers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_chat_user_stcusrs';
                        break;
                    default:
                        $key = 'UpdatedByUserStcusrs';
                }

                $result[$key] = $this->collUpdatedByUserStcusrs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCreatedByUserStranks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ranks';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_rank_stranks';
                        break;
                    default:
                        $key = 'CreatedByUserStranks';
                }

                $result[$key] = $this->collCreatedByUserStranks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpdatedByUserStranks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ranks';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_rank_stranks';
                        break;
                    default:
                        $key = 'UpdatedByUserStranks';
                }

                $result[$key] = $this->collUpdatedByUserStranks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCreatedByUserStavtrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'avatars';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_avatar_stavtrs';
                        break;
                    default:
                        $key = 'CreatedByUserStavtrs';
                }

                $result[$key] = $this->collCreatedByUserStavtrs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpdatedByUserStavtrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'avatars';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'stream_avatar_stavtrs';
                        break;
                    default:
                        $key = 'UpdatedByUserStavtrs';
                }

                $result[$key] = $this->collUpdatedByUserStavtrs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCreatedByUserVideoGamesApiCounts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiCounts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_count_vgapics';
                        break;
                    default:
                        $key = 'CreatedByUserVideoGamesApiCounts';
                }

                $result[$key] = $this->collCreatedByUserVideoGamesApiCounts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpdatedByUserVideoGamesApiCounts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiCounts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_count_vgapics';
                        break;
                    default:
                        $key = 'UpdatedByUserVideoGamesApiCounts';
                }

                $result[$key] = $this->collUpdatedByUserVideoGamesApiCounts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\AdminBundle\Model\User
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\AdminBundle\Model\User
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setEmail($value);
                break;
            case 3:
                $this->setFacebookId($value);
                break;
            case 4:
                $this->setGoogleId($value);
                break;
            case 5:
                $this->setTwitterId($value);
                break;
            case 6:
                $this->setPassword($value);
                break;
            case 7:
                $this->setSalt($value);
                break;
            case 8:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setRoles($value);
                break;
            case 9:
                $this->setIsActive($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
            case 12:
                $this->setUsrusrSlug($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UserTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUsername($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setEmail($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFacebookId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setGoogleId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setTwitterId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPassword($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setSalt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setRoles($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIsActive($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUsrusrSlug($arr[$keys[12]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\AdminBundle\Model\User The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UserTableMap::COL_USRUSR_ID)) {
            $criteria->add(UserTableMap::COL_USRUSR_ID, $this->usrusr_id);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_USERNAME)) {
            $criteria->add(UserTableMap::COL_USRUSR_USERNAME, $this->usrusr_username);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_EMAIL)) {
            $criteria->add(UserTableMap::COL_USRUSR_EMAIL, $this->usrusr_email);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_FACEBOOK_ID)) {
            $criteria->add(UserTableMap::COL_USRUSR_FACEBOOK_ID, $this->usrusr_facebook_id);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_GOOGLE_ID)) {
            $criteria->add(UserTableMap::COL_USRUSR_GOOGLE_ID, $this->usrusr_google_id);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_TWITTER_ID)) {
            $criteria->add(UserTableMap::COL_USRUSR_TWITTER_ID, $this->usrusr_twitter_id);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_PASSWORD)) {
            $criteria->add(UserTableMap::COL_USRUSR_PASSWORD, $this->usrusr_password);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_SALT)) {
            $criteria->add(UserTableMap::COL_USRUSR_SALT, $this->usrusr_salt);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_ROLES)) {
            $criteria->add(UserTableMap::COL_USRUSR_ROLES, $this->usrusr_roles);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_IS_ACTIVE)) {
            $criteria->add(UserTableMap::COL_USRUSR_IS_ACTIVE, $this->usrusr_is_active);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_CREATED_AT)) {
            $criteria->add(UserTableMap::COL_USRUSR_CREATED_AT, $this->usrusr_created_at);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_UPDATED_AT)) {
            $criteria->add(UserTableMap::COL_USRUSR_UPDATED_AT, $this->usrusr_updated_at);
        }
        if ($this->isColumnModified(UserTableMap::COL_USRUSR_SLUG)) {
            $criteria->add(UserTableMap::COL_USRUSR_SLUG, $this->usrusr_slug);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUserQuery::create();
        $criteria->add(UserTableMap::COL_USRUSR_ID, $this->usrusr_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (usrusr_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\AdminBundle\Model\User (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setFacebookId($this->getFacebookId());
        $copyObj->setGoogleId($this->getGoogleId());
        $copyObj->setTwitterId($this->getTwitterId());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setSalt($this->getSalt());
        $copyObj->setRoles($this->getRoles());
        $copyObj->setIsActive($this->getIsActive());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setUsrusrSlug($this->getUsrusrSlug());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCreatedByUserStsites() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCreatedByUserStsite($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpdatedByUserStsites() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpdatedByUserStsite($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCreatedByUserStstrms() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCreatedByUserStstrm($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpdatedByUserStstrms() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpdatedByUserStstrm($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCreatedByUserStchans() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCreatedByUserStchan($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpdatedByUserStchans() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpdatedByUserStchan($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCreatedByUserStcusrs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCreatedByUserStcusr($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpdatedByUserStcusrs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpdatedByUserStcusr($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCreatedByUserStranks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCreatedByUserStrank($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpdatedByUserStranks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpdatedByUserStrank($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCreatedByUserStavtrs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCreatedByUserStavtr($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpdatedByUserStavtrs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpdatedByUserStavtr($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCreatedByUserVideoGamesApiCounts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCreatedByUserVideoGamesApiCount($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpdatedByUserVideoGamesApiCounts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpdatedByUserVideoGamesApiCount($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\AdminBundle\Model\User Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CreatedByUserStsite' == $relationName) {
            $this->initCreatedByUserStsites();
            return;
        }
        if ('UpdatedByUserStsite' == $relationName) {
            $this->initUpdatedByUserStsites();
            return;
        }
        if ('CreatedByUserStstrm' == $relationName) {
            $this->initCreatedByUserStstrms();
            return;
        }
        if ('UpdatedByUserStstrm' == $relationName) {
            $this->initUpdatedByUserStstrms();
            return;
        }
        if ('CreatedByUserStchan' == $relationName) {
            $this->initCreatedByUserStchans();
            return;
        }
        if ('UpdatedByUserStchan' == $relationName) {
            $this->initUpdatedByUserStchans();
            return;
        }
        if ('CreatedByUserStcusr' == $relationName) {
            $this->initCreatedByUserStcusrs();
            return;
        }
        if ('UpdatedByUserStcusr' == $relationName) {
            $this->initUpdatedByUserStcusrs();
            return;
        }
        if ('CreatedByUserStrank' == $relationName) {
            $this->initCreatedByUserStranks();
            return;
        }
        if ('UpdatedByUserStrank' == $relationName) {
            $this->initUpdatedByUserStranks();
            return;
        }
        if ('CreatedByUserStavtr' == $relationName) {
            $this->initCreatedByUserStavtrs();
            return;
        }
        if ('UpdatedByUserStavtr' == $relationName) {
            $this->initUpdatedByUserStavtrs();
            return;
        }
        if ('CreatedByUserVideoGamesApiCount' == $relationName) {
            $this->initCreatedByUserVideoGamesApiCounts();
            return;
        }
        if ('UpdatedByUserVideoGamesApiCount' == $relationName) {
            $this->initUpdatedByUserVideoGamesApiCounts();
            return;
        }
    }

    /**
     * Clears out the collCreatedByUserStsites collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCreatedByUserStsites()
     */
    public function clearCreatedByUserStsites()
    {
        $this->collCreatedByUserStsites = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCreatedByUserStsites collection loaded partially.
     */
    public function resetPartialCreatedByUserStsites($v = true)
    {
        $this->collCreatedByUserStsitesPartial = $v;
    }

    /**
     * Initializes the collCreatedByUserStsites collection.
     *
     * By default this just sets the collCreatedByUserStsites collection to an empty array (like clearcollCreatedByUserStsites());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCreatedByUserStsites($overrideExisting = true)
    {
        if (null !== $this->collCreatedByUserStsites && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteTableMap::getTableMap()->getCollectionClassName();

        $this->collCreatedByUserStsites = new $collectionClassName;
        $this->collCreatedByUserStsites->setModel('\IiMedias\StreamBundle\Model\Site');
    }

    /**
     * Gets an array of Site objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Site[] List of Site objects
     * @throws PropelException
     */
    public function getCreatedByUserStsites(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStsitesPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStsites || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStsites) {
                // return empty collection
                $this->initCreatedByUserStsites();
            } else {
                $collCreatedByUserStsites = SiteQuery::create(null, $criteria)
                    ->filterByCreatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCreatedByUserStsitesPartial && count($collCreatedByUserStsites)) {
                        $this->initCreatedByUserStsites(false);

                        foreach ($collCreatedByUserStsites as $obj) {
                            if (false == $this->collCreatedByUserStsites->contains($obj)) {
                                $this->collCreatedByUserStsites->append($obj);
                            }
                        }

                        $this->collCreatedByUserStsitesPartial = true;
                    }

                    return $collCreatedByUserStsites;
                }

                if ($partial && $this->collCreatedByUserStsites) {
                    foreach ($this->collCreatedByUserStsites as $obj) {
                        if ($obj->isNew()) {
                            $collCreatedByUserStsites[] = $obj;
                        }
                    }
                }

                $this->collCreatedByUserStsites = $collCreatedByUserStsites;
                $this->collCreatedByUserStsitesPartial = false;
            }
        }

        return $this->collCreatedByUserStsites;
    }

    /**
     * Sets a collection of Site objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $createdByUserStsites A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setCreatedByUserStsites(Collection $createdByUserStsites, ConnectionInterface $con = null)
    {
        /** @var Site[] $createdByUserStsitesToDelete */
        $createdByUserStsitesToDelete = $this->getCreatedByUserStsites(new Criteria(), $con)->diff($createdByUserStsites);


        $this->createdByUserStsitesScheduledForDeletion = $createdByUserStsitesToDelete;

        foreach ($createdByUserStsitesToDelete as $createdByUserStsiteRemoved) {
            $createdByUserStsiteRemoved->setCreatedByUser(null);
        }

        $this->collCreatedByUserStsites = null;
        foreach ($createdByUserStsites as $createdByUserStsite) {
            $this->addCreatedByUserStsite($createdByUserStsite);
        }

        $this->collCreatedByUserStsites = $createdByUserStsites;
        $this->collCreatedByUserStsitesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSite objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSite objects.
     * @throws PropelException
     */
    public function countCreatedByUserStsites(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStsitesPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStsites || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStsites) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCreatedByUserStsites());
            }

            $query = SiteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCreatedByUser($this)
                ->count($con);
        }

        return count($this->collCreatedByUserStsites);
    }

    /**
     * Method called to associate a Site object to this object
     * through the Site foreign key attribute.
     *
     * @param  Site $l Site
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addCreatedByUserStsite(Site $l)
    {
        if ($this->collCreatedByUserStsites === null) {
            $this->initCreatedByUserStsites();
            $this->collCreatedByUserStsitesPartial = true;
        }

        if (!$this->collCreatedByUserStsites->contains($l)) {
            $this->doAddCreatedByUserStsite($l);

            if ($this->createdByUserStsitesScheduledForDeletion and $this->createdByUserStsitesScheduledForDeletion->contains($l)) {
                $this->createdByUserStsitesScheduledForDeletion->remove($this->createdByUserStsitesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Site $createdByUserStsite The Site object to add.
     */
    protected function doAddCreatedByUserStsite(Site $createdByUserStsite)
    {
        $this->collCreatedByUserStsites[]= $createdByUserStsite;
        $createdByUserStsite->setCreatedByUser($this);
    }

    /**
     * @param  Site $createdByUserStsite The Site object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeCreatedByUserStsite(Site $createdByUserStsite)
    {
        if ($this->getCreatedByUserStsites()->contains($createdByUserStsite)) {
            $pos = $this->collCreatedByUserStsites->search($createdByUserStsite);
            $this->collCreatedByUserStsites->remove($pos);
            if (null === $this->createdByUserStsitesScheduledForDeletion) {
                $this->createdByUserStsitesScheduledForDeletion = clone $this->collCreatedByUserStsites;
                $this->createdByUserStsitesScheduledForDeletion->clear();
            }
            $this->createdByUserStsitesScheduledForDeletion[]= $createdByUserStsite;
            $createdByUserStsite->setCreatedByUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collUpdatedByUserStsites collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpdatedByUserStsites()
     */
    public function clearUpdatedByUserStsites()
    {
        $this->collUpdatedByUserStsites = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpdatedByUserStsites collection loaded partially.
     */
    public function resetPartialUpdatedByUserStsites($v = true)
    {
        $this->collUpdatedByUserStsitesPartial = $v;
    }

    /**
     * Initializes the collUpdatedByUserStsites collection.
     *
     * By default this just sets the collUpdatedByUserStsites collection to an empty array (like clearcollUpdatedByUserStsites());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpdatedByUserStsites($overrideExisting = true)
    {
        if (null !== $this->collUpdatedByUserStsites && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteTableMap::getTableMap()->getCollectionClassName();

        $this->collUpdatedByUserStsites = new $collectionClassName;
        $this->collUpdatedByUserStsites->setModel('\IiMedias\StreamBundle\Model\Site');
    }

    /**
     * Gets an array of Site objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Site[] List of Site objects
     * @throws PropelException
     */
    public function getUpdatedByUserStsites(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStsitesPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStsites || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStsites) {
                // return empty collection
                $this->initUpdatedByUserStsites();
            } else {
                $collUpdatedByUserStsites = SiteQuery::create(null, $criteria)
                    ->filterByUpdatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpdatedByUserStsitesPartial && count($collUpdatedByUserStsites)) {
                        $this->initUpdatedByUserStsites(false);

                        foreach ($collUpdatedByUserStsites as $obj) {
                            if (false == $this->collUpdatedByUserStsites->contains($obj)) {
                                $this->collUpdatedByUserStsites->append($obj);
                            }
                        }

                        $this->collUpdatedByUserStsitesPartial = true;
                    }

                    return $collUpdatedByUserStsites;
                }

                if ($partial && $this->collUpdatedByUserStsites) {
                    foreach ($this->collUpdatedByUserStsites as $obj) {
                        if ($obj->isNew()) {
                            $collUpdatedByUserStsites[] = $obj;
                        }
                    }
                }

                $this->collUpdatedByUserStsites = $collUpdatedByUserStsites;
                $this->collUpdatedByUserStsitesPartial = false;
            }
        }

        return $this->collUpdatedByUserStsites;
    }

    /**
     * Sets a collection of Site objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $updatedByUserStsites A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUpdatedByUserStsites(Collection $updatedByUserStsites, ConnectionInterface $con = null)
    {
        /** @var Site[] $updatedByUserStsitesToDelete */
        $updatedByUserStsitesToDelete = $this->getUpdatedByUserStsites(new Criteria(), $con)->diff($updatedByUserStsites);


        $this->updatedByUserStsitesScheduledForDeletion = $updatedByUserStsitesToDelete;

        foreach ($updatedByUserStsitesToDelete as $updatedByUserStsiteRemoved) {
            $updatedByUserStsiteRemoved->setUpdatedByUser(null);
        }

        $this->collUpdatedByUserStsites = null;
        foreach ($updatedByUserStsites as $updatedByUserStsite) {
            $this->addUpdatedByUserStsite($updatedByUserStsite);
        }

        $this->collUpdatedByUserStsites = $updatedByUserStsites;
        $this->collUpdatedByUserStsitesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSite objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSite objects.
     * @throws PropelException
     */
    public function countUpdatedByUserStsites(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStsitesPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStsites || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStsites) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpdatedByUserStsites());
            }

            $query = SiteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUpdatedByUser($this)
                ->count($con);
        }

        return count($this->collUpdatedByUserStsites);
    }

    /**
     * Method called to associate a Site object to this object
     * through the Site foreign key attribute.
     *
     * @param  Site $l Site
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addUpdatedByUserStsite(Site $l)
    {
        if ($this->collUpdatedByUserStsites === null) {
            $this->initUpdatedByUserStsites();
            $this->collUpdatedByUserStsitesPartial = true;
        }

        if (!$this->collUpdatedByUserStsites->contains($l)) {
            $this->doAddUpdatedByUserStsite($l);

            if ($this->updatedByUserStsitesScheduledForDeletion and $this->updatedByUserStsitesScheduledForDeletion->contains($l)) {
                $this->updatedByUserStsitesScheduledForDeletion->remove($this->updatedByUserStsitesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Site $updatedByUserStsite The Site object to add.
     */
    protected function doAddUpdatedByUserStsite(Site $updatedByUserStsite)
    {
        $this->collUpdatedByUserStsites[]= $updatedByUserStsite;
        $updatedByUserStsite->setUpdatedByUser($this);
    }

    /**
     * @param  Site $updatedByUserStsite The Site object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUpdatedByUserStsite(Site $updatedByUserStsite)
    {
        if ($this->getUpdatedByUserStsites()->contains($updatedByUserStsite)) {
            $pos = $this->collUpdatedByUserStsites->search($updatedByUserStsite);
            $this->collUpdatedByUserStsites->remove($pos);
            if (null === $this->updatedByUserStsitesScheduledForDeletion) {
                $this->updatedByUserStsitesScheduledForDeletion = clone $this->collUpdatedByUserStsites;
                $this->updatedByUserStsitesScheduledForDeletion->clear();
            }
            $this->updatedByUserStsitesScheduledForDeletion[]= $updatedByUserStsite;
            $updatedByUserStsite->setUpdatedByUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collCreatedByUserStstrms collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCreatedByUserStstrms()
     */
    public function clearCreatedByUserStstrms()
    {
        $this->collCreatedByUserStstrms = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCreatedByUserStstrms collection loaded partially.
     */
    public function resetPartialCreatedByUserStstrms($v = true)
    {
        $this->collCreatedByUserStstrmsPartial = $v;
    }

    /**
     * Initializes the collCreatedByUserStstrms collection.
     *
     * By default this just sets the collCreatedByUserStstrms collection to an empty array (like clearcollCreatedByUserStstrms());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCreatedByUserStstrms($overrideExisting = true)
    {
        if (null !== $this->collCreatedByUserStstrms && !$overrideExisting) {
            return;
        }

        $collectionClassName = StreamTableMap::getTableMap()->getCollectionClassName();

        $this->collCreatedByUserStstrms = new $collectionClassName;
        $this->collCreatedByUserStstrms->setModel('\IiMedias\StreamBundle\Model\Stream');
    }

    /**
     * Gets an array of Stream objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Stream[] List of Stream objects
     * @throws PropelException
     */
    public function getCreatedByUserStstrms(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStstrmsPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStstrms || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStstrms) {
                // return empty collection
                $this->initCreatedByUserStstrms();
            } else {
                $collCreatedByUserStstrms = StreamQuery::create(null, $criteria)
                    ->filterByCreatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCreatedByUserStstrmsPartial && count($collCreatedByUserStstrms)) {
                        $this->initCreatedByUserStstrms(false);

                        foreach ($collCreatedByUserStstrms as $obj) {
                            if (false == $this->collCreatedByUserStstrms->contains($obj)) {
                                $this->collCreatedByUserStstrms->append($obj);
                            }
                        }

                        $this->collCreatedByUserStstrmsPartial = true;
                    }

                    return $collCreatedByUserStstrms;
                }

                if ($partial && $this->collCreatedByUserStstrms) {
                    foreach ($this->collCreatedByUserStstrms as $obj) {
                        if ($obj->isNew()) {
                            $collCreatedByUserStstrms[] = $obj;
                        }
                    }
                }

                $this->collCreatedByUserStstrms = $collCreatedByUserStstrms;
                $this->collCreatedByUserStstrmsPartial = false;
            }
        }

        return $this->collCreatedByUserStstrms;
    }

    /**
     * Sets a collection of Stream objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $createdByUserStstrms A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setCreatedByUserStstrms(Collection $createdByUserStstrms, ConnectionInterface $con = null)
    {
        /** @var Stream[] $createdByUserStstrmsToDelete */
        $createdByUserStstrmsToDelete = $this->getCreatedByUserStstrms(new Criteria(), $con)->diff($createdByUserStstrms);


        $this->createdByUserStstrmsScheduledForDeletion = $createdByUserStstrmsToDelete;

        foreach ($createdByUserStstrmsToDelete as $createdByUserStstrmRemoved) {
            $createdByUserStstrmRemoved->setCreatedByUser(null);
        }

        $this->collCreatedByUserStstrms = null;
        foreach ($createdByUserStstrms as $createdByUserStstrm) {
            $this->addCreatedByUserStstrm($createdByUserStstrm);
        }

        $this->collCreatedByUserStstrms = $createdByUserStstrms;
        $this->collCreatedByUserStstrmsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseStream objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseStream objects.
     * @throws PropelException
     */
    public function countCreatedByUserStstrms(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStstrmsPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStstrms || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStstrms) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCreatedByUserStstrms());
            }

            $query = StreamQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCreatedByUser($this)
                ->count($con);
        }

        return count($this->collCreatedByUserStstrms);
    }

    /**
     * Method called to associate a Stream object to this object
     * through the Stream foreign key attribute.
     *
     * @param  Stream $l Stream
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addCreatedByUserStstrm(Stream $l)
    {
        if ($this->collCreatedByUserStstrms === null) {
            $this->initCreatedByUserStstrms();
            $this->collCreatedByUserStstrmsPartial = true;
        }

        if (!$this->collCreatedByUserStstrms->contains($l)) {
            $this->doAddCreatedByUserStstrm($l);

            if ($this->createdByUserStstrmsScheduledForDeletion and $this->createdByUserStstrmsScheduledForDeletion->contains($l)) {
                $this->createdByUserStstrmsScheduledForDeletion->remove($this->createdByUserStstrmsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Stream $createdByUserStstrm The Stream object to add.
     */
    protected function doAddCreatedByUserStstrm(Stream $createdByUserStstrm)
    {
        $this->collCreatedByUserStstrms[]= $createdByUserStstrm;
        $createdByUserStstrm->setCreatedByUser($this);
    }

    /**
     * @param  Stream $createdByUserStstrm The Stream object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeCreatedByUserStstrm(Stream $createdByUserStstrm)
    {
        if ($this->getCreatedByUserStstrms()->contains($createdByUserStstrm)) {
            $pos = $this->collCreatedByUserStstrms->search($createdByUserStstrm);
            $this->collCreatedByUserStstrms->remove($pos);
            if (null === $this->createdByUserStstrmsScheduledForDeletion) {
                $this->createdByUserStstrmsScheduledForDeletion = clone $this->collCreatedByUserStstrms;
                $this->createdByUserStstrmsScheduledForDeletion->clear();
            }
            $this->createdByUserStstrmsScheduledForDeletion[]= $createdByUserStstrm;
            $createdByUserStstrm->setCreatedByUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collUpdatedByUserStstrms collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpdatedByUserStstrms()
     */
    public function clearUpdatedByUserStstrms()
    {
        $this->collUpdatedByUserStstrms = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpdatedByUserStstrms collection loaded partially.
     */
    public function resetPartialUpdatedByUserStstrms($v = true)
    {
        $this->collUpdatedByUserStstrmsPartial = $v;
    }

    /**
     * Initializes the collUpdatedByUserStstrms collection.
     *
     * By default this just sets the collUpdatedByUserStstrms collection to an empty array (like clearcollUpdatedByUserStstrms());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpdatedByUserStstrms($overrideExisting = true)
    {
        if (null !== $this->collUpdatedByUserStstrms && !$overrideExisting) {
            return;
        }

        $collectionClassName = StreamTableMap::getTableMap()->getCollectionClassName();

        $this->collUpdatedByUserStstrms = new $collectionClassName;
        $this->collUpdatedByUserStstrms->setModel('\IiMedias\StreamBundle\Model\Stream');
    }

    /**
     * Gets an array of Stream objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Stream[] List of Stream objects
     * @throws PropelException
     */
    public function getUpdatedByUserStstrms(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStstrmsPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStstrms || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStstrms) {
                // return empty collection
                $this->initUpdatedByUserStstrms();
            } else {
                $collUpdatedByUserStstrms = StreamQuery::create(null, $criteria)
                    ->filterByUpdatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpdatedByUserStstrmsPartial && count($collUpdatedByUserStstrms)) {
                        $this->initUpdatedByUserStstrms(false);

                        foreach ($collUpdatedByUserStstrms as $obj) {
                            if (false == $this->collUpdatedByUserStstrms->contains($obj)) {
                                $this->collUpdatedByUserStstrms->append($obj);
                            }
                        }

                        $this->collUpdatedByUserStstrmsPartial = true;
                    }

                    return $collUpdatedByUserStstrms;
                }

                if ($partial && $this->collUpdatedByUserStstrms) {
                    foreach ($this->collUpdatedByUserStstrms as $obj) {
                        if ($obj->isNew()) {
                            $collUpdatedByUserStstrms[] = $obj;
                        }
                    }
                }

                $this->collUpdatedByUserStstrms = $collUpdatedByUserStstrms;
                $this->collUpdatedByUserStstrmsPartial = false;
            }
        }

        return $this->collUpdatedByUserStstrms;
    }

    /**
     * Sets a collection of Stream objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $updatedByUserStstrms A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUpdatedByUserStstrms(Collection $updatedByUserStstrms, ConnectionInterface $con = null)
    {
        /** @var Stream[] $updatedByUserStstrmsToDelete */
        $updatedByUserStstrmsToDelete = $this->getUpdatedByUserStstrms(new Criteria(), $con)->diff($updatedByUserStstrms);


        $this->updatedByUserStstrmsScheduledForDeletion = $updatedByUserStstrmsToDelete;

        foreach ($updatedByUserStstrmsToDelete as $updatedByUserStstrmRemoved) {
            $updatedByUserStstrmRemoved->setUpdatedByUser(null);
        }

        $this->collUpdatedByUserStstrms = null;
        foreach ($updatedByUserStstrms as $updatedByUserStstrm) {
            $this->addUpdatedByUserStstrm($updatedByUserStstrm);
        }

        $this->collUpdatedByUserStstrms = $updatedByUserStstrms;
        $this->collUpdatedByUserStstrmsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseStream objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseStream objects.
     * @throws PropelException
     */
    public function countUpdatedByUserStstrms(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStstrmsPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStstrms || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStstrms) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpdatedByUserStstrms());
            }

            $query = StreamQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUpdatedByUser($this)
                ->count($con);
        }

        return count($this->collUpdatedByUserStstrms);
    }

    /**
     * Method called to associate a Stream object to this object
     * through the Stream foreign key attribute.
     *
     * @param  Stream $l Stream
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addUpdatedByUserStstrm(Stream $l)
    {
        if ($this->collUpdatedByUserStstrms === null) {
            $this->initUpdatedByUserStstrms();
            $this->collUpdatedByUserStstrmsPartial = true;
        }

        if (!$this->collUpdatedByUserStstrms->contains($l)) {
            $this->doAddUpdatedByUserStstrm($l);

            if ($this->updatedByUserStstrmsScheduledForDeletion and $this->updatedByUserStstrmsScheduledForDeletion->contains($l)) {
                $this->updatedByUserStstrmsScheduledForDeletion->remove($this->updatedByUserStstrmsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Stream $updatedByUserStstrm The Stream object to add.
     */
    protected function doAddUpdatedByUserStstrm(Stream $updatedByUserStstrm)
    {
        $this->collUpdatedByUserStstrms[]= $updatedByUserStstrm;
        $updatedByUserStstrm->setUpdatedByUser($this);
    }

    /**
     * @param  Stream $updatedByUserStstrm The Stream object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUpdatedByUserStstrm(Stream $updatedByUserStstrm)
    {
        if ($this->getUpdatedByUserStstrms()->contains($updatedByUserStstrm)) {
            $pos = $this->collUpdatedByUserStstrms->search($updatedByUserStstrm);
            $this->collUpdatedByUserStstrms->remove($pos);
            if (null === $this->updatedByUserStstrmsScheduledForDeletion) {
                $this->updatedByUserStstrmsScheduledForDeletion = clone $this->collUpdatedByUserStstrms;
                $this->updatedByUserStstrmsScheduledForDeletion->clear();
            }
            $this->updatedByUserStstrmsScheduledForDeletion[]= $updatedByUserStstrm;
            $updatedByUserStstrm->setUpdatedByUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collCreatedByUserStchans collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCreatedByUserStchans()
     */
    public function clearCreatedByUserStchans()
    {
        $this->collCreatedByUserStchans = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCreatedByUserStchans collection loaded partially.
     */
    public function resetPartialCreatedByUserStchans($v = true)
    {
        $this->collCreatedByUserStchansPartial = $v;
    }

    /**
     * Initializes the collCreatedByUserStchans collection.
     *
     * By default this just sets the collCreatedByUserStchans collection to an empty array (like clearcollCreatedByUserStchans());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCreatedByUserStchans($overrideExisting = true)
    {
        if (null !== $this->collCreatedByUserStchans && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChannelTableMap::getTableMap()->getCollectionClassName();

        $this->collCreatedByUserStchans = new $collectionClassName;
        $this->collCreatedByUserStchans->setModel('\IiMedias\StreamBundle\Model\Channel');
    }

    /**
     * Gets an array of Channel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Channel[] List of Channel objects
     * @throws PropelException
     */
    public function getCreatedByUserStchans(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStchansPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStchans || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStchans) {
                // return empty collection
                $this->initCreatedByUserStchans();
            } else {
                $collCreatedByUserStchans = ChannelQuery::create(null, $criteria)
                    ->filterByCreatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCreatedByUserStchansPartial && count($collCreatedByUserStchans)) {
                        $this->initCreatedByUserStchans(false);

                        foreach ($collCreatedByUserStchans as $obj) {
                            if (false == $this->collCreatedByUserStchans->contains($obj)) {
                                $this->collCreatedByUserStchans->append($obj);
                            }
                        }

                        $this->collCreatedByUserStchansPartial = true;
                    }

                    return $collCreatedByUserStchans;
                }

                if ($partial && $this->collCreatedByUserStchans) {
                    foreach ($this->collCreatedByUserStchans as $obj) {
                        if ($obj->isNew()) {
                            $collCreatedByUserStchans[] = $obj;
                        }
                    }
                }

                $this->collCreatedByUserStchans = $collCreatedByUserStchans;
                $this->collCreatedByUserStchansPartial = false;
            }
        }

        return $this->collCreatedByUserStchans;
    }

    /**
     * Sets a collection of Channel objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $createdByUserStchans A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setCreatedByUserStchans(Collection $createdByUserStchans, ConnectionInterface $con = null)
    {
        /** @var Channel[] $createdByUserStchansToDelete */
        $createdByUserStchansToDelete = $this->getCreatedByUserStchans(new Criteria(), $con)->diff($createdByUserStchans);


        $this->createdByUserStchansScheduledForDeletion = $createdByUserStchansToDelete;

        foreach ($createdByUserStchansToDelete as $createdByUserStchanRemoved) {
            $createdByUserStchanRemoved->setCreatedByUser(null);
        }

        $this->collCreatedByUserStchans = null;
        foreach ($createdByUserStchans as $createdByUserStchan) {
            $this->addCreatedByUserStchan($createdByUserStchan);
        }

        $this->collCreatedByUserStchans = $createdByUserStchans;
        $this->collCreatedByUserStchansPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseChannel objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseChannel objects.
     * @throws PropelException
     */
    public function countCreatedByUserStchans(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStchansPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStchans || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStchans) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCreatedByUserStchans());
            }

            $query = ChannelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCreatedByUser($this)
                ->count($con);
        }

        return count($this->collCreatedByUserStchans);
    }

    /**
     * Method called to associate a Channel object to this object
     * through the Channel foreign key attribute.
     *
     * @param  Channel $l Channel
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addCreatedByUserStchan(Channel $l)
    {
        if ($this->collCreatedByUserStchans === null) {
            $this->initCreatedByUserStchans();
            $this->collCreatedByUserStchansPartial = true;
        }

        if (!$this->collCreatedByUserStchans->contains($l)) {
            $this->doAddCreatedByUserStchan($l);

            if ($this->createdByUserStchansScheduledForDeletion and $this->createdByUserStchansScheduledForDeletion->contains($l)) {
                $this->createdByUserStchansScheduledForDeletion->remove($this->createdByUserStchansScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Channel $createdByUserStchan The Channel object to add.
     */
    protected function doAddCreatedByUserStchan(Channel $createdByUserStchan)
    {
        $this->collCreatedByUserStchans[]= $createdByUserStchan;
        $createdByUserStchan->setCreatedByUser($this);
    }

    /**
     * @param  Channel $createdByUserStchan The Channel object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeCreatedByUserStchan(Channel $createdByUserStchan)
    {
        if ($this->getCreatedByUserStchans()->contains($createdByUserStchan)) {
            $pos = $this->collCreatedByUserStchans->search($createdByUserStchan);
            $this->collCreatedByUserStchans->remove($pos);
            if (null === $this->createdByUserStchansScheduledForDeletion) {
                $this->createdByUserStchansScheduledForDeletion = clone $this->collCreatedByUserStchans;
                $this->createdByUserStchansScheduledForDeletion->clear();
            }
            $this->createdByUserStchansScheduledForDeletion[]= $createdByUserStchan;
            $createdByUserStchan->setCreatedByUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStchans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Channel[] List of Channel objects
     */
    public function getCreatedByUserStchansJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChannelQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getCreatedByUserStchans($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStchans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Channel[] List of Channel objects
     */
    public function getCreatedByUserStchansJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChannelQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getCreatedByUserStchans($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStchans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Channel[] List of Channel objects
     */
    public function getCreatedByUserStchansJoinBot(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChannelQuery::create(null, $criteria);
        $query->joinWith('Bot', $joinBehavior);

        return $this->getCreatedByUserStchans($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStchans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Channel[] List of Channel objects
     */
    public function getCreatedByUserStchansJoinApiTwitchGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChannelQuery::create(null, $criteria);
        $query->joinWith('ApiTwitchGame', $joinBehavior);

        return $this->getCreatedByUserStchans($query, $con);
    }

    /**
     * Clears out the collUpdatedByUserStchans collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpdatedByUserStchans()
     */
    public function clearUpdatedByUserStchans()
    {
        $this->collUpdatedByUserStchans = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpdatedByUserStchans collection loaded partially.
     */
    public function resetPartialUpdatedByUserStchans($v = true)
    {
        $this->collUpdatedByUserStchansPartial = $v;
    }

    /**
     * Initializes the collUpdatedByUserStchans collection.
     *
     * By default this just sets the collUpdatedByUserStchans collection to an empty array (like clearcollUpdatedByUserStchans());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpdatedByUserStchans($overrideExisting = true)
    {
        if (null !== $this->collUpdatedByUserStchans && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChannelTableMap::getTableMap()->getCollectionClassName();

        $this->collUpdatedByUserStchans = new $collectionClassName;
        $this->collUpdatedByUserStchans->setModel('\IiMedias\StreamBundle\Model\Channel');
    }

    /**
     * Gets an array of Channel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Channel[] List of Channel objects
     * @throws PropelException
     */
    public function getUpdatedByUserStchans(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStchansPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStchans || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStchans) {
                // return empty collection
                $this->initUpdatedByUserStchans();
            } else {
                $collUpdatedByUserStchans = ChannelQuery::create(null, $criteria)
                    ->filterByUpdatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpdatedByUserStchansPartial && count($collUpdatedByUserStchans)) {
                        $this->initUpdatedByUserStchans(false);

                        foreach ($collUpdatedByUserStchans as $obj) {
                            if (false == $this->collUpdatedByUserStchans->contains($obj)) {
                                $this->collUpdatedByUserStchans->append($obj);
                            }
                        }

                        $this->collUpdatedByUserStchansPartial = true;
                    }

                    return $collUpdatedByUserStchans;
                }

                if ($partial && $this->collUpdatedByUserStchans) {
                    foreach ($this->collUpdatedByUserStchans as $obj) {
                        if ($obj->isNew()) {
                            $collUpdatedByUserStchans[] = $obj;
                        }
                    }
                }

                $this->collUpdatedByUserStchans = $collUpdatedByUserStchans;
                $this->collUpdatedByUserStchansPartial = false;
            }
        }

        return $this->collUpdatedByUserStchans;
    }

    /**
     * Sets a collection of Channel objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $updatedByUserStchans A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUpdatedByUserStchans(Collection $updatedByUserStchans, ConnectionInterface $con = null)
    {
        /** @var Channel[] $updatedByUserStchansToDelete */
        $updatedByUserStchansToDelete = $this->getUpdatedByUserStchans(new Criteria(), $con)->diff($updatedByUserStchans);


        $this->updatedByUserStchansScheduledForDeletion = $updatedByUserStchansToDelete;

        foreach ($updatedByUserStchansToDelete as $updatedByUserStchanRemoved) {
            $updatedByUserStchanRemoved->setUpdatedByUser(null);
        }

        $this->collUpdatedByUserStchans = null;
        foreach ($updatedByUserStchans as $updatedByUserStchan) {
            $this->addUpdatedByUserStchan($updatedByUserStchan);
        }

        $this->collUpdatedByUserStchans = $updatedByUserStchans;
        $this->collUpdatedByUserStchansPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseChannel objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseChannel objects.
     * @throws PropelException
     */
    public function countUpdatedByUserStchans(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStchansPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStchans || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStchans) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpdatedByUserStchans());
            }

            $query = ChannelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUpdatedByUser($this)
                ->count($con);
        }

        return count($this->collUpdatedByUserStchans);
    }

    /**
     * Method called to associate a Channel object to this object
     * through the Channel foreign key attribute.
     *
     * @param  Channel $l Channel
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addUpdatedByUserStchan(Channel $l)
    {
        if ($this->collUpdatedByUserStchans === null) {
            $this->initUpdatedByUserStchans();
            $this->collUpdatedByUserStchansPartial = true;
        }

        if (!$this->collUpdatedByUserStchans->contains($l)) {
            $this->doAddUpdatedByUserStchan($l);

            if ($this->updatedByUserStchansScheduledForDeletion and $this->updatedByUserStchansScheduledForDeletion->contains($l)) {
                $this->updatedByUserStchansScheduledForDeletion->remove($this->updatedByUserStchansScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Channel $updatedByUserStchan The Channel object to add.
     */
    protected function doAddUpdatedByUserStchan(Channel $updatedByUserStchan)
    {
        $this->collUpdatedByUserStchans[]= $updatedByUserStchan;
        $updatedByUserStchan->setUpdatedByUser($this);
    }

    /**
     * @param  Channel $updatedByUserStchan The Channel object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUpdatedByUserStchan(Channel $updatedByUserStchan)
    {
        if ($this->getUpdatedByUserStchans()->contains($updatedByUserStchan)) {
            $pos = $this->collUpdatedByUserStchans->search($updatedByUserStchan);
            $this->collUpdatedByUserStchans->remove($pos);
            if (null === $this->updatedByUserStchansScheduledForDeletion) {
                $this->updatedByUserStchansScheduledForDeletion = clone $this->collUpdatedByUserStchans;
                $this->updatedByUserStchansScheduledForDeletion->clear();
            }
            $this->updatedByUserStchansScheduledForDeletion[]= $updatedByUserStchan;
            $updatedByUserStchan->setUpdatedByUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStchans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Channel[] List of Channel objects
     */
    public function getUpdatedByUserStchansJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChannelQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getUpdatedByUserStchans($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStchans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Channel[] List of Channel objects
     */
    public function getUpdatedByUserStchansJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChannelQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getUpdatedByUserStchans($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStchans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Channel[] List of Channel objects
     */
    public function getUpdatedByUserStchansJoinBot(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChannelQuery::create(null, $criteria);
        $query->joinWith('Bot', $joinBehavior);

        return $this->getUpdatedByUserStchans($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStchans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Channel[] List of Channel objects
     */
    public function getUpdatedByUserStchansJoinApiTwitchGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChannelQuery::create(null, $criteria);
        $query->joinWith('ApiTwitchGame', $joinBehavior);

        return $this->getUpdatedByUserStchans($query, $con);
    }

    /**
     * Clears out the collCreatedByUserStcusrs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCreatedByUserStcusrs()
     */
    public function clearCreatedByUserStcusrs()
    {
        $this->collCreatedByUserStcusrs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCreatedByUserStcusrs collection loaded partially.
     */
    public function resetPartialCreatedByUserStcusrs($v = true)
    {
        $this->collCreatedByUserStcusrsPartial = $v;
    }

    /**
     * Initializes the collCreatedByUserStcusrs collection.
     *
     * By default this just sets the collCreatedByUserStcusrs collection to an empty array (like clearcollCreatedByUserStcusrs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCreatedByUserStcusrs($overrideExisting = true)
    {
        if (null !== $this->collCreatedByUserStcusrs && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChatUserTableMap::getTableMap()->getCollectionClassName();

        $this->collCreatedByUserStcusrs = new $collectionClassName;
        $this->collCreatedByUserStcusrs->setModel('\IiMedias\StreamBundle\Model\ChatUser');
    }

    /**
     * Gets an array of ChatUser objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChatUser[] List of ChatUser objects
     * @throws PropelException
     */
    public function getCreatedByUserStcusrs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStcusrsPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStcusrs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStcusrs) {
                // return empty collection
                $this->initCreatedByUserStcusrs();
            } else {
                $collCreatedByUserStcusrs = ChatUserQuery::create(null, $criteria)
                    ->filterByCreatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCreatedByUserStcusrsPartial && count($collCreatedByUserStcusrs)) {
                        $this->initCreatedByUserStcusrs(false);

                        foreach ($collCreatedByUserStcusrs as $obj) {
                            if (false == $this->collCreatedByUserStcusrs->contains($obj)) {
                                $this->collCreatedByUserStcusrs->append($obj);
                            }
                        }

                        $this->collCreatedByUserStcusrsPartial = true;
                    }

                    return $collCreatedByUserStcusrs;
                }

                if ($partial && $this->collCreatedByUserStcusrs) {
                    foreach ($this->collCreatedByUserStcusrs as $obj) {
                        if ($obj->isNew()) {
                            $collCreatedByUserStcusrs[] = $obj;
                        }
                    }
                }

                $this->collCreatedByUserStcusrs = $collCreatedByUserStcusrs;
                $this->collCreatedByUserStcusrsPartial = false;
            }
        }

        return $this->collCreatedByUserStcusrs;
    }

    /**
     * Sets a collection of ChatUser objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $createdByUserStcusrs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setCreatedByUserStcusrs(Collection $createdByUserStcusrs, ConnectionInterface $con = null)
    {
        /** @var ChatUser[] $createdByUserStcusrsToDelete */
        $createdByUserStcusrsToDelete = $this->getCreatedByUserStcusrs(new Criteria(), $con)->diff($createdByUserStcusrs);


        $this->createdByUserStcusrsScheduledForDeletion = $createdByUserStcusrsToDelete;

        foreach ($createdByUserStcusrsToDelete as $createdByUserStcusrRemoved) {
            $createdByUserStcusrRemoved->setCreatedByUser(null);
        }

        $this->collCreatedByUserStcusrs = null;
        foreach ($createdByUserStcusrs as $createdByUserStcusr) {
            $this->addCreatedByUserStcusr($createdByUserStcusr);
        }

        $this->collCreatedByUserStcusrs = $createdByUserStcusrs;
        $this->collCreatedByUserStcusrsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseChatUser objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseChatUser objects.
     * @throws PropelException
     */
    public function countCreatedByUserStcusrs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStcusrsPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStcusrs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStcusrs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCreatedByUserStcusrs());
            }

            $query = ChatUserQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCreatedByUser($this)
                ->count($con);
        }

        return count($this->collCreatedByUserStcusrs);
    }

    /**
     * Method called to associate a ChatUser object to this object
     * through the ChatUser foreign key attribute.
     *
     * @param  ChatUser $l ChatUser
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addCreatedByUserStcusr(ChatUser $l)
    {
        if ($this->collCreatedByUserStcusrs === null) {
            $this->initCreatedByUserStcusrs();
            $this->collCreatedByUserStcusrsPartial = true;
        }

        if (!$this->collCreatedByUserStcusrs->contains($l)) {
            $this->doAddCreatedByUserStcusr($l);

            if ($this->createdByUserStcusrsScheduledForDeletion and $this->createdByUserStcusrsScheduledForDeletion->contains($l)) {
                $this->createdByUserStcusrsScheduledForDeletion->remove($this->createdByUserStcusrsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChatUser $createdByUserStcusr The ChatUser object to add.
     */
    protected function doAddCreatedByUserStcusr(ChatUser $createdByUserStcusr)
    {
        $this->collCreatedByUserStcusrs[]= $createdByUserStcusr;
        $createdByUserStcusr->setCreatedByUser($this);
    }

    /**
     * @param  ChatUser $createdByUserStcusr The ChatUser object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeCreatedByUserStcusr(ChatUser $createdByUserStcusr)
    {
        if ($this->getCreatedByUserStcusrs()->contains($createdByUserStcusr)) {
            $pos = $this->collCreatedByUserStcusrs->search($createdByUserStcusr);
            $this->collCreatedByUserStcusrs->remove($pos);
            if (null === $this->createdByUserStcusrsScheduledForDeletion) {
                $this->createdByUserStcusrsScheduledForDeletion = clone $this->collCreatedByUserStcusrs;
                $this->createdByUserStcusrsScheduledForDeletion->clear();
            }
            $this->createdByUserStcusrsScheduledForDeletion[]= $createdByUserStcusr;
            $createdByUserStcusr->setCreatedByUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStcusrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChatUser[] List of ChatUser objects
     */
    public function getCreatedByUserStcusrsJoinParent(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChatUserQuery::create(null, $criteria);
        $query->joinWith('Parent', $joinBehavior);

        return $this->getCreatedByUserStcusrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStcusrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChatUser[] List of ChatUser objects
     */
    public function getCreatedByUserStcusrsJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChatUserQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getCreatedByUserStcusrs($query, $con);
    }

    /**
     * Clears out the collUpdatedByUserStcusrs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpdatedByUserStcusrs()
     */
    public function clearUpdatedByUserStcusrs()
    {
        $this->collUpdatedByUserStcusrs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpdatedByUserStcusrs collection loaded partially.
     */
    public function resetPartialUpdatedByUserStcusrs($v = true)
    {
        $this->collUpdatedByUserStcusrsPartial = $v;
    }

    /**
     * Initializes the collUpdatedByUserStcusrs collection.
     *
     * By default this just sets the collUpdatedByUserStcusrs collection to an empty array (like clearcollUpdatedByUserStcusrs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpdatedByUserStcusrs($overrideExisting = true)
    {
        if (null !== $this->collUpdatedByUserStcusrs && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChatUserTableMap::getTableMap()->getCollectionClassName();

        $this->collUpdatedByUserStcusrs = new $collectionClassName;
        $this->collUpdatedByUserStcusrs->setModel('\IiMedias\StreamBundle\Model\ChatUser');
    }

    /**
     * Gets an array of ChatUser objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChatUser[] List of ChatUser objects
     * @throws PropelException
     */
    public function getUpdatedByUserStcusrs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStcusrsPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStcusrs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStcusrs) {
                // return empty collection
                $this->initUpdatedByUserStcusrs();
            } else {
                $collUpdatedByUserStcusrs = ChatUserQuery::create(null, $criteria)
                    ->filterByUpdatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpdatedByUserStcusrsPartial && count($collUpdatedByUserStcusrs)) {
                        $this->initUpdatedByUserStcusrs(false);

                        foreach ($collUpdatedByUserStcusrs as $obj) {
                            if (false == $this->collUpdatedByUserStcusrs->contains($obj)) {
                                $this->collUpdatedByUserStcusrs->append($obj);
                            }
                        }

                        $this->collUpdatedByUserStcusrsPartial = true;
                    }

                    return $collUpdatedByUserStcusrs;
                }

                if ($partial && $this->collUpdatedByUserStcusrs) {
                    foreach ($this->collUpdatedByUserStcusrs as $obj) {
                        if ($obj->isNew()) {
                            $collUpdatedByUserStcusrs[] = $obj;
                        }
                    }
                }

                $this->collUpdatedByUserStcusrs = $collUpdatedByUserStcusrs;
                $this->collUpdatedByUserStcusrsPartial = false;
            }
        }

        return $this->collUpdatedByUserStcusrs;
    }

    /**
     * Sets a collection of ChatUser objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $updatedByUserStcusrs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUpdatedByUserStcusrs(Collection $updatedByUserStcusrs, ConnectionInterface $con = null)
    {
        /** @var ChatUser[] $updatedByUserStcusrsToDelete */
        $updatedByUserStcusrsToDelete = $this->getUpdatedByUserStcusrs(new Criteria(), $con)->diff($updatedByUserStcusrs);


        $this->updatedByUserStcusrsScheduledForDeletion = $updatedByUserStcusrsToDelete;

        foreach ($updatedByUserStcusrsToDelete as $updatedByUserStcusrRemoved) {
            $updatedByUserStcusrRemoved->setUpdatedByUser(null);
        }

        $this->collUpdatedByUserStcusrs = null;
        foreach ($updatedByUserStcusrs as $updatedByUserStcusr) {
            $this->addUpdatedByUserStcusr($updatedByUserStcusr);
        }

        $this->collUpdatedByUserStcusrs = $updatedByUserStcusrs;
        $this->collUpdatedByUserStcusrsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseChatUser objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseChatUser objects.
     * @throws PropelException
     */
    public function countUpdatedByUserStcusrs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStcusrsPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStcusrs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStcusrs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpdatedByUserStcusrs());
            }

            $query = ChatUserQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUpdatedByUser($this)
                ->count($con);
        }

        return count($this->collUpdatedByUserStcusrs);
    }

    /**
     * Method called to associate a ChatUser object to this object
     * through the ChatUser foreign key attribute.
     *
     * @param  ChatUser $l ChatUser
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addUpdatedByUserStcusr(ChatUser $l)
    {
        if ($this->collUpdatedByUserStcusrs === null) {
            $this->initUpdatedByUserStcusrs();
            $this->collUpdatedByUserStcusrsPartial = true;
        }

        if (!$this->collUpdatedByUserStcusrs->contains($l)) {
            $this->doAddUpdatedByUserStcusr($l);

            if ($this->updatedByUserStcusrsScheduledForDeletion and $this->updatedByUserStcusrsScheduledForDeletion->contains($l)) {
                $this->updatedByUserStcusrsScheduledForDeletion->remove($this->updatedByUserStcusrsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChatUser $updatedByUserStcusr The ChatUser object to add.
     */
    protected function doAddUpdatedByUserStcusr(ChatUser $updatedByUserStcusr)
    {
        $this->collUpdatedByUserStcusrs[]= $updatedByUserStcusr;
        $updatedByUserStcusr->setUpdatedByUser($this);
    }

    /**
     * @param  ChatUser $updatedByUserStcusr The ChatUser object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUpdatedByUserStcusr(ChatUser $updatedByUserStcusr)
    {
        if ($this->getUpdatedByUserStcusrs()->contains($updatedByUserStcusr)) {
            $pos = $this->collUpdatedByUserStcusrs->search($updatedByUserStcusr);
            $this->collUpdatedByUserStcusrs->remove($pos);
            if (null === $this->updatedByUserStcusrsScheduledForDeletion) {
                $this->updatedByUserStcusrsScheduledForDeletion = clone $this->collUpdatedByUserStcusrs;
                $this->updatedByUserStcusrsScheduledForDeletion->clear();
            }
            $this->updatedByUserStcusrsScheduledForDeletion[]= $updatedByUserStcusr;
            $updatedByUserStcusr->setUpdatedByUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStcusrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChatUser[] List of ChatUser objects
     */
    public function getUpdatedByUserStcusrsJoinParent(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChatUserQuery::create(null, $criteria);
        $query->joinWith('Parent', $joinBehavior);

        return $this->getUpdatedByUserStcusrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStcusrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChatUser[] List of ChatUser objects
     */
    public function getUpdatedByUserStcusrsJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChatUserQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getUpdatedByUserStcusrs($query, $con);
    }

    /**
     * Clears out the collCreatedByUserStranks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCreatedByUserStranks()
     */
    public function clearCreatedByUserStranks()
    {
        $this->collCreatedByUserStranks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCreatedByUserStranks collection loaded partially.
     */
    public function resetPartialCreatedByUserStranks($v = true)
    {
        $this->collCreatedByUserStranksPartial = $v;
    }

    /**
     * Initializes the collCreatedByUserStranks collection.
     *
     * By default this just sets the collCreatedByUserStranks collection to an empty array (like clearcollCreatedByUserStranks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCreatedByUserStranks($overrideExisting = true)
    {
        if (null !== $this->collCreatedByUserStranks && !$overrideExisting) {
            return;
        }

        $collectionClassName = RankTableMap::getTableMap()->getCollectionClassName();

        $this->collCreatedByUserStranks = new $collectionClassName;
        $this->collCreatedByUserStranks->setModel('\IiMedias\StreamBundle\Model\Rank');
    }

    /**
     * Gets an array of Rank objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Rank[] List of Rank objects
     * @throws PropelException
     */
    public function getCreatedByUserStranks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStranksPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStranks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStranks) {
                // return empty collection
                $this->initCreatedByUserStranks();
            } else {
                $collCreatedByUserStranks = RankQuery::create(null, $criteria)
                    ->filterByCreatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCreatedByUserStranksPartial && count($collCreatedByUserStranks)) {
                        $this->initCreatedByUserStranks(false);

                        foreach ($collCreatedByUserStranks as $obj) {
                            if (false == $this->collCreatedByUserStranks->contains($obj)) {
                                $this->collCreatedByUserStranks->append($obj);
                            }
                        }

                        $this->collCreatedByUserStranksPartial = true;
                    }

                    return $collCreatedByUserStranks;
                }

                if ($partial && $this->collCreatedByUserStranks) {
                    foreach ($this->collCreatedByUserStranks as $obj) {
                        if ($obj->isNew()) {
                            $collCreatedByUserStranks[] = $obj;
                        }
                    }
                }

                $this->collCreatedByUserStranks = $collCreatedByUserStranks;
                $this->collCreatedByUserStranksPartial = false;
            }
        }

        return $this->collCreatedByUserStranks;
    }

    /**
     * Sets a collection of Rank objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $createdByUserStranks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setCreatedByUserStranks(Collection $createdByUserStranks, ConnectionInterface $con = null)
    {
        /** @var Rank[] $createdByUserStranksToDelete */
        $createdByUserStranksToDelete = $this->getCreatedByUserStranks(new Criteria(), $con)->diff($createdByUserStranks);


        $this->createdByUserStranksScheduledForDeletion = $createdByUserStranksToDelete;

        foreach ($createdByUserStranksToDelete as $createdByUserStrankRemoved) {
            $createdByUserStrankRemoved->setCreatedByUser(null);
        }

        $this->collCreatedByUserStranks = null;
        foreach ($createdByUserStranks as $createdByUserStrank) {
            $this->addCreatedByUserStrank($createdByUserStrank);
        }

        $this->collCreatedByUserStranks = $createdByUserStranks;
        $this->collCreatedByUserStranksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseRank objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseRank objects.
     * @throws PropelException
     */
    public function countCreatedByUserStranks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStranksPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStranks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStranks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCreatedByUserStranks());
            }

            $query = RankQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCreatedByUser($this)
                ->count($con);
        }

        return count($this->collCreatedByUserStranks);
    }

    /**
     * Method called to associate a Rank object to this object
     * through the Rank foreign key attribute.
     *
     * @param  Rank $l Rank
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addCreatedByUserStrank(Rank $l)
    {
        if ($this->collCreatedByUserStranks === null) {
            $this->initCreatedByUserStranks();
            $this->collCreatedByUserStranksPartial = true;
        }

        if (!$this->collCreatedByUserStranks->contains($l)) {
            $this->doAddCreatedByUserStrank($l);

            if ($this->createdByUserStranksScheduledForDeletion and $this->createdByUserStranksScheduledForDeletion->contains($l)) {
                $this->createdByUserStranksScheduledForDeletion->remove($this->createdByUserStranksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Rank $createdByUserStrank The Rank object to add.
     */
    protected function doAddCreatedByUserStrank(Rank $createdByUserStrank)
    {
        $this->collCreatedByUserStranks[]= $createdByUserStrank;
        $createdByUserStrank->setCreatedByUser($this);
    }

    /**
     * @param  Rank $createdByUserStrank The Rank object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeCreatedByUserStrank(Rank $createdByUserStrank)
    {
        if ($this->getCreatedByUserStranks()->contains($createdByUserStrank)) {
            $pos = $this->collCreatedByUserStranks->search($createdByUserStrank);
            $this->collCreatedByUserStranks->remove($pos);
            if (null === $this->createdByUserStranksScheduledForDeletion) {
                $this->createdByUserStranksScheduledForDeletion = clone $this->collCreatedByUserStranks;
                $this->createdByUserStranksScheduledForDeletion->clear();
            }
            $this->createdByUserStranksScheduledForDeletion[]= $createdByUserStrank;
            $createdByUserStrank->setCreatedByUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStranks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Rank[] List of Rank objects
     */
    public function getCreatedByUserStranksJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = RankQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getCreatedByUserStranks($query, $con);
    }

    /**
     * Clears out the collUpdatedByUserStranks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpdatedByUserStranks()
     */
    public function clearUpdatedByUserStranks()
    {
        $this->collUpdatedByUserStranks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpdatedByUserStranks collection loaded partially.
     */
    public function resetPartialUpdatedByUserStranks($v = true)
    {
        $this->collUpdatedByUserStranksPartial = $v;
    }

    /**
     * Initializes the collUpdatedByUserStranks collection.
     *
     * By default this just sets the collUpdatedByUserStranks collection to an empty array (like clearcollUpdatedByUserStranks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpdatedByUserStranks($overrideExisting = true)
    {
        if (null !== $this->collUpdatedByUserStranks && !$overrideExisting) {
            return;
        }

        $collectionClassName = RankTableMap::getTableMap()->getCollectionClassName();

        $this->collUpdatedByUserStranks = new $collectionClassName;
        $this->collUpdatedByUserStranks->setModel('\IiMedias\StreamBundle\Model\Rank');
    }

    /**
     * Gets an array of Rank objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Rank[] List of Rank objects
     * @throws PropelException
     */
    public function getUpdatedByUserStranks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStranksPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStranks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStranks) {
                // return empty collection
                $this->initUpdatedByUserStranks();
            } else {
                $collUpdatedByUserStranks = RankQuery::create(null, $criteria)
                    ->filterByUpdatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpdatedByUserStranksPartial && count($collUpdatedByUserStranks)) {
                        $this->initUpdatedByUserStranks(false);

                        foreach ($collUpdatedByUserStranks as $obj) {
                            if (false == $this->collUpdatedByUserStranks->contains($obj)) {
                                $this->collUpdatedByUserStranks->append($obj);
                            }
                        }

                        $this->collUpdatedByUserStranksPartial = true;
                    }

                    return $collUpdatedByUserStranks;
                }

                if ($partial && $this->collUpdatedByUserStranks) {
                    foreach ($this->collUpdatedByUserStranks as $obj) {
                        if ($obj->isNew()) {
                            $collUpdatedByUserStranks[] = $obj;
                        }
                    }
                }

                $this->collUpdatedByUserStranks = $collUpdatedByUserStranks;
                $this->collUpdatedByUserStranksPartial = false;
            }
        }

        return $this->collUpdatedByUserStranks;
    }

    /**
     * Sets a collection of Rank objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $updatedByUserStranks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUpdatedByUserStranks(Collection $updatedByUserStranks, ConnectionInterface $con = null)
    {
        /** @var Rank[] $updatedByUserStranksToDelete */
        $updatedByUserStranksToDelete = $this->getUpdatedByUserStranks(new Criteria(), $con)->diff($updatedByUserStranks);


        $this->updatedByUserStranksScheduledForDeletion = $updatedByUserStranksToDelete;

        foreach ($updatedByUserStranksToDelete as $updatedByUserStrankRemoved) {
            $updatedByUserStrankRemoved->setUpdatedByUser(null);
        }

        $this->collUpdatedByUserStranks = null;
        foreach ($updatedByUserStranks as $updatedByUserStrank) {
            $this->addUpdatedByUserStrank($updatedByUserStrank);
        }

        $this->collUpdatedByUserStranks = $updatedByUserStranks;
        $this->collUpdatedByUserStranksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseRank objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseRank objects.
     * @throws PropelException
     */
    public function countUpdatedByUserStranks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStranksPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStranks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStranks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpdatedByUserStranks());
            }

            $query = RankQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUpdatedByUser($this)
                ->count($con);
        }

        return count($this->collUpdatedByUserStranks);
    }

    /**
     * Method called to associate a Rank object to this object
     * through the Rank foreign key attribute.
     *
     * @param  Rank $l Rank
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addUpdatedByUserStrank(Rank $l)
    {
        if ($this->collUpdatedByUserStranks === null) {
            $this->initUpdatedByUserStranks();
            $this->collUpdatedByUserStranksPartial = true;
        }

        if (!$this->collUpdatedByUserStranks->contains($l)) {
            $this->doAddUpdatedByUserStrank($l);

            if ($this->updatedByUserStranksScheduledForDeletion and $this->updatedByUserStranksScheduledForDeletion->contains($l)) {
                $this->updatedByUserStranksScheduledForDeletion->remove($this->updatedByUserStranksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Rank $updatedByUserStrank The Rank object to add.
     */
    protected function doAddUpdatedByUserStrank(Rank $updatedByUserStrank)
    {
        $this->collUpdatedByUserStranks[]= $updatedByUserStrank;
        $updatedByUserStrank->setUpdatedByUser($this);
    }

    /**
     * @param  Rank $updatedByUserStrank The Rank object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUpdatedByUserStrank(Rank $updatedByUserStrank)
    {
        if ($this->getUpdatedByUserStranks()->contains($updatedByUserStrank)) {
            $pos = $this->collUpdatedByUserStranks->search($updatedByUserStrank);
            $this->collUpdatedByUserStranks->remove($pos);
            if (null === $this->updatedByUserStranksScheduledForDeletion) {
                $this->updatedByUserStranksScheduledForDeletion = clone $this->collUpdatedByUserStranks;
                $this->updatedByUserStranksScheduledForDeletion->clear();
            }
            $this->updatedByUserStranksScheduledForDeletion[]= $updatedByUserStrank;
            $updatedByUserStrank->setUpdatedByUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStranks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Rank[] List of Rank objects
     */
    public function getUpdatedByUserStranksJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = RankQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getUpdatedByUserStranks($query, $con);
    }

    /**
     * Clears out the collCreatedByUserStavtrs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCreatedByUserStavtrs()
     */
    public function clearCreatedByUserStavtrs()
    {
        $this->collCreatedByUserStavtrs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCreatedByUserStavtrs collection loaded partially.
     */
    public function resetPartialCreatedByUserStavtrs($v = true)
    {
        $this->collCreatedByUserStavtrsPartial = $v;
    }

    /**
     * Initializes the collCreatedByUserStavtrs collection.
     *
     * By default this just sets the collCreatedByUserStavtrs collection to an empty array (like clearcollCreatedByUserStavtrs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCreatedByUserStavtrs($overrideExisting = true)
    {
        if (null !== $this->collCreatedByUserStavtrs && !$overrideExisting) {
            return;
        }

        $collectionClassName = AvatarTableMap::getTableMap()->getCollectionClassName();

        $this->collCreatedByUserStavtrs = new $collectionClassName;
        $this->collCreatedByUserStavtrs->setModel('\IiMedias\StreamBundle\Model\Avatar');
    }

    /**
     * Gets an array of Avatar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Avatar[] List of Avatar objects
     * @throws PropelException
     */
    public function getCreatedByUserStavtrs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStavtrsPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStavtrs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStavtrs) {
                // return empty collection
                $this->initCreatedByUserStavtrs();
            } else {
                $collCreatedByUserStavtrs = AvatarQuery::create(null, $criteria)
                    ->filterByCreatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCreatedByUserStavtrsPartial && count($collCreatedByUserStavtrs)) {
                        $this->initCreatedByUserStavtrs(false);

                        foreach ($collCreatedByUserStavtrs as $obj) {
                            if (false == $this->collCreatedByUserStavtrs->contains($obj)) {
                                $this->collCreatedByUserStavtrs->append($obj);
                            }
                        }

                        $this->collCreatedByUserStavtrsPartial = true;
                    }

                    return $collCreatedByUserStavtrs;
                }

                if ($partial && $this->collCreatedByUserStavtrs) {
                    foreach ($this->collCreatedByUserStavtrs as $obj) {
                        if ($obj->isNew()) {
                            $collCreatedByUserStavtrs[] = $obj;
                        }
                    }
                }

                $this->collCreatedByUserStavtrs = $collCreatedByUserStavtrs;
                $this->collCreatedByUserStavtrsPartial = false;
            }
        }

        return $this->collCreatedByUserStavtrs;
    }

    /**
     * Sets a collection of Avatar objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $createdByUserStavtrs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setCreatedByUserStavtrs(Collection $createdByUserStavtrs, ConnectionInterface $con = null)
    {
        /** @var Avatar[] $createdByUserStavtrsToDelete */
        $createdByUserStavtrsToDelete = $this->getCreatedByUserStavtrs(new Criteria(), $con)->diff($createdByUserStavtrs);


        $this->createdByUserStavtrsScheduledForDeletion = $createdByUserStavtrsToDelete;

        foreach ($createdByUserStavtrsToDelete as $createdByUserStavtrRemoved) {
            $createdByUserStavtrRemoved->setCreatedByUser(null);
        }

        $this->collCreatedByUserStavtrs = null;
        foreach ($createdByUserStavtrs as $createdByUserStavtr) {
            $this->addCreatedByUserStavtr($createdByUserStavtr);
        }

        $this->collCreatedByUserStavtrs = $createdByUserStavtrs;
        $this->collCreatedByUserStavtrsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseAvatar objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseAvatar objects.
     * @throws PropelException
     */
    public function countCreatedByUserStavtrs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserStavtrsPartial && !$this->isNew();
        if (null === $this->collCreatedByUserStavtrs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserStavtrs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCreatedByUserStavtrs());
            }

            $query = AvatarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCreatedByUser($this)
                ->count($con);
        }

        return count($this->collCreatedByUserStavtrs);
    }

    /**
     * Method called to associate a Avatar object to this object
     * through the Avatar foreign key attribute.
     *
     * @param  Avatar $l Avatar
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addCreatedByUserStavtr(Avatar $l)
    {
        if ($this->collCreatedByUserStavtrs === null) {
            $this->initCreatedByUserStavtrs();
            $this->collCreatedByUserStavtrsPartial = true;
        }

        if (!$this->collCreatedByUserStavtrs->contains($l)) {
            $this->doAddCreatedByUserStavtr($l);

            if ($this->createdByUserStavtrsScheduledForDeletion and $this->createdByUserStavtrsScheduledForDeletion->contains($l)) {
                $this->createdByUserStavtrsScheduledForDeletion->remove($this->createdByUserStavtrsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Avatar $createdByUserStavtr The Avatar object to add.
     */
    protected function doAddCreatedByUserStavtr(Avatar $createdByUserStavtr)
    {
        $this->collCreatedByUserStavtrs[]= $createdByUserStavtr;
        $createdByUserStavtr->setCreatedByUser($this);
    }

    /**
     * @param  Avatar $createdByUserStavtr The Avatar object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeCreatedByUserStavtr(Avatar $createdByUserStavtr)
    {
        if ($this->getCreatedByUserStavtrs()->contains($createdByUserStavtr)) {
            $pos = $this->collCreatedByUserStavtrs->search($createdByUserStavtr);
            $this->collCreatedByUserStavtrs->remove($pos);
            if (null === $this->createdByUserStavtrsScheduledForDeletion) {
                $this->createdByUserStavtrsScheduledForDeletion = clone $this->collCreatedByUserStavtrs;
                $this->createdByUserStavtrsScheduledForDeletion->clear();
            }
            $this->createdByUserStavtrsScheduledForDeletion[]= $createdByUserStavtr;
            $createdByUserStavtr->setCreatedByUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStavtrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Avatar[] List of Avatar objects
     */
    public function getCreatedByUserStavtrsJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = AvatarQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getCreatedByUserStavtrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStavtrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Avatar[] List of Avatar objects
     */
    public function getCreatedByUserStavtrsJoinMinRank(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = AvatarQuery::create(null, $criteria);
        $query->joinWith('MinRank', $joinBehavior);

        return $this->getCreatedByUserStavtrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CreatedByUserStavtrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Avatar[] List of Avatar objects
     */
    public function getCreatedByUserStavtrsJoinAssociatedRank(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = AvatarQuery::create(null, $criteria);
        $query->joinWith('AssociatedRank', $joinBehavior);

        return $this->getCreatedByUserStavtrs($query, $con);
    }

    /**
     * Clears out the collUpdatedByUserStavtrs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpdatedByUserStavtrs()
     */
    public function clearUpdatedByUserStavtrs()
    {
        $this->collUpdatedByUserStavtrs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpdatedByUserStavtrs collection loaded partially.
     */
    public function resetPartialUpdatedByUserStavtrs($v = true)
    {
        $this->collUpdatedByUserStavtrsPartial = $v;
    }

    /**
     * Initializes the collUpdatedByUserStavtrs collection.
     *
     * By default this just sets the collUpdatedByUserStavtrs collection to an empty array (like clearcollUpdatedByUserStavtrs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpdatedByUserStavtrs($overrideExisting = true)
    {
        if (null !== $this->collUpdatedByUserStavtrs && !$overrideExisting) {
            return;
        }

        $collectionClassName = AvatarTableMap::getTableMap()->getCollectionClassName();

        $this->collUpdatedByUserStavtrs = new $collectionClassName;
        $this->collUpdatedByUserStavtrs->setModel('\IiMedias\StreamBundle\Model\Avatar');
    }

    /**
     * Gets an array of Avatar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Avatar[] List of Avatar objects
     * @throws PropelException
     */
    public function getUpdatedByUserStavtrs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStavtrsPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStavtrs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStavtrs) {
                // return empty collection
                $this->initUpdatedByUserStavtrs();
            } else {
                $collUpdatedByUserStavtrs = AvatarQuery::create(null, $criteria)
                    ->filterByUpdatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpdatedByUserStavtrsPartial && count($collUpdatedByUserStavtrs)) {
                        $this->initUpdatedByUserStavtrs(false);

                        foreach ($collUpdatedByUserStavtrs as $obj) {
                            if (false == $this->collUpdatedByUserStavtrs->contains($obj)) {
                                $this->collUpdatedByUserStavtrs->append($obj);
                            }
                        }

                        $this->collUpdatedByUserStavtrsPartial = true;
                    }

                    return $collUpdatedByUserStavtrs;
                }

                if ($partial && $this->collUpdatedByUserStavtrs) {
                    foreach ($this->collUpdatedByUserStavtrs as $obj) {
                        if ($obj->isNew()) {
                            $collUpdatedByUserStavtrs[] = $obj;
                        }
                    }
                }

                $this->collUpdatedByUserStavtrs = $collUpdatedByUserStavtrs;
                $this->collUpdatedByUserStavtrsPartial = false;
            }
        }

        return $this->collUpdatedByUserStavtrs;
    }

    /**
     * Sets a collection of Avatar objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $updatedByUserStavtrs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUpdatedByUserStavtrs(Collection $updatedByUserStavtrs, ConnectionInterface $con = null)
    {
        /** @var Avatar[] $updatedByUserStavtrsToDelete */
        $updatedByUserStavtrsToDelete = $this->getUpdatedByUserStavtrs(new Criteria(), $con)->diff($updatedByUserStavtrs);


        $this->updatedByUserStavtrsScheduledForDeletion = $updatedByUserStavtrsToDelete;

        foreach ($updatedByUserStavtrsToDelete as $updatedByUserStavtrRemoved) {
            $updatedByUserStavtrRemoved->setUpdatedByUser(null);
        }

        $this->collUpdatedByUserStavtrs = null;
        foreach ($updatedByUserStavtrs as $updatedByUserStavtr) {
            $this->addUpdatedByUserStavtr($updatedByUserStavtr);
        }

        $this->collUpdatedByUserStavtrs = $updatedByUserStavtrs;
        $this->collUpdatedByUserStavtrsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseAvatar objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseAvatar objects.
     * @throws PropelException
     */
    public function countUpdatedByUserStavtrs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserStavtrsPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserStavtrs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserStavtrs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpdatedByUserStavtrs());
            }

            $query = AvatarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUpdatedByUser($this)
                ->count($con);
        }

        return count($this->collUpdatedByUserStavtrs);
    }

    /**
     * Method called to associate a Avatar object to this object
     * through the Avatar foreign key attribute.
     *
     * @param  Avatar $l Avatar
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addUpdatedByUserStavtr(Avatar $l)
    {
        if ($this->collUpdatedByUserStavtrs === null) {
            $this->initUpdatedByUserStavtrs();
            $this->collUpdatedByUserStavtrsPartial = true;
        }

        if (!$this->collUpdatedByUserStavtrs->contains($l)) {
            $this->doAddUpdatedByUserStavtr($l);

            if ($this->updatedByUserStavtrsScheduledForDeletion and $this->updatedByUserStavtrsScheduledForDeletion->contains($l)) {
                $this->updatedByUserStavtrsScheduledForDeletion->remove($this->updatedByUserStavtrsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Avatar $updatedByUserStavtr The Avatar object to add.
     */
    protected function doAddUpdatedByUserStavtr(Avatar $updatedByUserStavtr)
    {
        $this->collUpdatedByUserStavtrs[]= $updatedByUserStavtr;
        $updatedByUserStavtr->setUpdatedByUser($this);
    }

    /**
     * @param  Avatar $updatedByUserStavtr The Avatar object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUpdatedByUserStavtr(Avatar $updatedByUserStavtr)
    {
        if ($this->getUpdatedByUserStavtrs()->contains($updatedByUserStavtr)) {
            $pos = $this->collUpdatedByUserStavtrs->search($updatedByUserStavtr);
            $this->collUpdatedByUserStavtrs->remove($pos);
            if (null === $this->updatedByUserStavtrsScheduledForDeletion) {
                $this->updatedByUserStavtrsScheduledForDeletion = clone $this->collUpdatedByUserStavtrs;
                $this->updatedByUserStavtrsScheduledForDeletion->clear();
            }
            $this->updatedByUserStavtrsScheduledForDeletion[]= $updatedByUserStavtr;
            $updatedByUserStavtr->setUpdatedByUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStavtrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Avatar[] List of Avatar objects
     */
    public function getUpdatedByUserStavtrsJoinStream(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = AvatarQuery::create(null, $criteria);
        $query->joinWith('Stream', $joinBehavior);

        return $this->getUpdatedByUserStavtrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStavtrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Avatar[] List of Avatar objects
     */
    public function getUpdatedByUserStavtrsJoinMinRank(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = AvatarQuery::create(null, $criteria);
        $query->joinWith('MinRank', $joinBehavior);

        return $this->getUpdatedByUserStavtrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UpdatedByUserStavtrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Avatar[] List of Avatar objects
     */
    public function getUpdatedByUserStavtrsJoinAssociatedRank(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = AvatarQuery::create(null, $criteria);
        $query->joinWith('AssociatedRank', $joinBehavior);

        return $this->getUpdatedByUserStavtrs($query, $con);
    }

    /**
     * Clears out the collCreatedByUserVideoGamesApiCounts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCreatedByUserVideoGamesApiCounts()
     */
    public function clearCreatedByUserVideoGamesApiCounts()
    {
        $this->collCreatedByUserVideoGamesApiCounts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCreatedByUserVideoGamesApiCounts collection loaded partially.
     */
    public function resetPartialCreatedByUserVideoGamesApiCounts($v = true)
    {
        $this->collCreatedByUserVideoGamesApiCountsPartial = $v;
    }

    /**
     * Initializes the collCreatedByUserVideoGamesApiCounts collection.
     *
     * By default this just sets the collCreatedByUserVideoGamesApiCounts collection to an empty array (like clearcollCreatedByUserVideoGamesApiCounts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCreatedByUserVideoGamesApiCounts($overrideExisting = true)
    {
        if (null !== $this->collCreatedByUserVideoGamesApiCounts && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiCountTableMap::getTableMap()->getCollectionClassName();

        $this->collCreatedByUserVideoGamesApiCounts = new $collectionClassName;
        $this->collCreatedByUserVideoGamesApiCounts->setModel('\IiMedias\VideoGamesBundle\Model\ApiCount');
    }

    /**
     * Gets an array of ApiCount objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ApiCount[] List of ApiCount objects
     * @throws PropelException
     */
    public function getCreatedByUserVideoGamesApiCounts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserVideoGamesApiCountsPartial && !$this->isNew();
        if (null === $this->collCreatedByUserVideoGamesApiCounts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserVideoGamesApiCounts) {
                // return empty collection
                $this->initCreatedByUserVideoGamesApiCounts();
            } else {
                $collCreatedByUserVideoGamesApiCounts = ApiCountQuery::create(null, $criteria)
                    ->filterByCreatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCreatedByUserVideoGamesApiCountsPartial && count($collCreatedByUserVideoGamesApiCounts)) {
                        $this->initCreatedByUserVideoGamesApiCounts(false);

                        foreach ($collCreatedByUserVideoGamesApiCounts as $obj) {
                            if (false == $this->collCreatedByUserVideoGamesApiCounts->contains($obj)) {
                                $this->collCreatedByUserVideoGamesApiCounts->append($obj);
                            }
                        }

                        $this->collCreatedByUserVideoGamesApiCountsPartial = true;
                    }

                    return $collCreatedByUserVideoGamesApiCounts;
                }

                if ($partial && $this->collCreatedByUserVideoGamesApiCounts) {
                    foreach ($this->collCreatedByUserVideoGamesApiCounts as $obj) {
                        if ($obj->isNew()) {
                            $collCreatedByUserVideoGamesApiCounts[] = $obj;
                        }
                    }
                }

                $this->collCreatedByUserVideoGamesApiCounts = $collCreatedByUserVideoGamesApiCounts;
                $this->collCreatedByUserVideoGamesApiCountsPartial = false;
            }
        }

        return $this->collCreatedByUserVideoGamesApiCounts;
    }

    /**
     * Sets a collection of ApiCount objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $createdByUserVideoGamesApiCounts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setCreatedByUserVideoGamesApiCounts(Collection $createdByUserVideoGamesApiCounts, ConnectionInterface $con = null)
    {
        /** @var ApiCount[] $createdByUserVideoGamesApiCountsToDelete */
        $createdByUserVideoGamesApiCountsToDelete = $this->getCreatedByUserVideoGamesApiCounts(new Criteria(), $con)->diff($createdByUserVideoGamesApiCounts);


        $this->createdByUserVideoGamesApiCountsScheduledForDeletion = $createdByUserVideoGamesApiCountsToDelete;

        foreach ($createdByUserVideoGamesApiCountsToDelete as $createdByUserVideoGamesApiCountRemoved) {
            $createdByUserVideoGamesApiCountRemoved->setCreatedByUser(null);
        }

        $this->collCreatedByUserVideoGamesApiCounts = null;
        foreach ($createdByUserVideoGamesApiCounts as $createdByUserVideoGamesApiCount) {
            $this->addCreatedByUserVideoGamesApiCount($createdByUserVideoGamesApiCount);
        }

        $this->collCreatedByUserVideoGamesApiCounts = $createdByUserVideoGamesApiCounts;
        $this->collCreatedByUserVideoGamesApiCountsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseApiCount objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseApiCount objects.
     * @throws PropelException
     */
    public function countCreatedByUserVideoGamesApiCounts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCreatedByUserVideoGamesApiCountsPartial && !$this->isNew();
        if (null === $this->collCreatedByUserVideoGamesApiCounts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCreatedByUserVideoGamesApiCounts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCreatedByUserVideoGamesApiCounts());
            }

            $query = ApiCountQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCreatedByUser($this)
                ->count($con);
        }

        return count($this->collCreatedByUserVideoGamesApiCounts);
    }

    /**
     * Method called to associate a ApiCount object to this object
     * through the ApiCount foreign key attribute.
     *
     * @param  ApiCount $l ApiCount
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addCreatedByUserVideoGamesApiCount(ApiCount $l)
    {
        if ($this->collCreatedByUserVideoGamesApiCounts === null) {
            $this->initCreatedByUserVideoGamesApiCounts();
            $this->collCreatedByUserVideoGamesApiCountsPartial = true;
        }

        if (!$this->collCreatedByUserVideoGamesApiCounts->contains($l)) {
            $this->doAddCreatedByUserVideoGamesApiCount($l);

            if ($this->createdByUserVideoGamesApiCountsScheduledForDeletion and $this->createdByUserVideoGamesApiCountsScheduledForDeletion->contains($l)) {
                $this->createdByUserVideoGamesApiCountsScheduledForDeletion->remove($this->createdByUserVideoGamesApiCountsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ApiCount $createdByUserVideoGamesApiCount The ApiCount object to add.
     */
    protected function doAddCreatedByUserVideoGamesApiCount(ApiCount $createdByUserVideoGamesApiCount)
    {
        $this->collCreatedByUserVideoGamesApiCounts[]= $createdByUserVideoGamesApiCount;
        $createdByUserVideoGamesApiCount->setCreatedByUser($this);
    }

    /**
     * @param  ApiCount $createdByUserVideoGamesApiCount The ApiCount object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeCreatedByUserVideoGamesApiCount(ApiCount $createdByUserVideoGamesApiCount)
    {
        if ($this->getCreatedByUserVideoGamesApiCounts()->contains($createdByUserVideoGamesApiCount)) {
            $pos = $this->collCreatedByUserVideoGamesApiCounts->search($createdByUserVideoGamesApiCount);
            $this->collCreatedByUserVideoGamesApiCounts->remove($pos);
            if (null === $this->createdByUserVideoGamesApiCountsScheduledForDeletion) {
                $this->createdByUserVideoGamesApiCountsScheduledForDeletion = clone $this->collCreatedByUserVideoGamesApiCounts;
                $this->createdByUserVideoGamesApiCountsScheduledForDeletion->clear();
            }
            $this->createdByUserVideoGamesApiCountsScheduledForDeletion[]= $createdByUserVideoGamesApiCount;
            $createdByUserVideoGamesApiCount->setCreatedByUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collUpdatedByUserVideoGamesApiCounts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpdatedByUserVideoGamesApiCounts()
     */
    public function clearUpdatedByUserVideoGamesApiCounts()
    {
        $this->collUpdatedByUserVideoGamesApiCounts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpdatedByUserVideoGamesApiCounts collection loaded partially.
     */
    public function resetPartialUpdatedByUserVideoGamesApiCounts($v = true)
    {
        $this->collUpdatedByUserVideoGamesApiCountsPartial = $v;
    }

    /**
     * Initializes the collUpdatedByUserVideoGamesApiCounts collection.
     *
     * By default this just sets the collUpdatedByUserVideoGamesApiCounts collection to an empty array (like clearcollUpdatedByUserVideoGamesApiCounts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpdatedByUserVideoGamesApiCounts($overrideExisting = true)
    {
        if (null !== $this->collUpdatedByUserVideoGamesApiCounts && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiCountTableMap::getTableMap()->getCollectionClassName();

        $this->collUpdatedByUserVideoGamesApiCounts = new $collectionClassName;
        $this->collUpdatedByUserVideoGamesApiCounts->setModel('\IiMedias\VideoGamesBundle\Model\ApiCount');
    }

    /**
     * Gets an array of ApiCount objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ApiCount[] List of ApiCount objects
     * @throws PropelException
     */
    public function getUpdatedByUserVideoGamesApiCounts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserVideoGamesApiCountsPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserVideoGamesApiCounts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserVideoGamesApiCounts) {
                // return empty collection
                $this->initUpdatedByUserVideoGamesApiCounts();
            } else {
                $collUpdatedByUserVideoGamesApiCounts = ApiCountQuery::create(null, $criteria)
                    ->filterByUpdatedByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpdatedByUserVideoGamesApiCountsPartial && count($collUpdatedByUserVideoGamesApiCounts)) {
                        $this->initUpdatedByUserVideoGamesApiCounts(false);

                        foreach ($collUpdatedByUserVideoGamesApiCounts as $obj) {
                            if (false == $this->collUpdatedByUserVideoGamesApiCounts->contains($obj)) {
                                $this->collUpdatedByUserVideoGamesApiCounts->append($obj);
                            }
                        }

                        $this->collUpdatedByUserVideoGamesApiCountsPartial = true;
                    }

                    return $collUpdatedByUserVideoGamesApiCounts;
                }

                if ($partial && $this->collUpdatedByUserVideoGamesApiCounts) {
                    foreach ($this->collUpdatedByUserVideoGamesApiCounts as $obj) {
                        if ($obj->isNew()) {
                            $collUpdatedByUserVideoGamesApiCounts[] = $obj;
                        }
                    }
                }

                $this->collUpdatedByUserVideoGamesApiCounts = $collUpdatedByUserVideoGamesApiCounts;
                $this->collUpdatedByUserVideoGamesApiCountsPartial = false;
            }
        }

        return $this->collUpdatedByUserVideoGamesApiCounts;
    }

    /**
     * Sets a collection of ApiCount objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $updatedByUserVideoGamesApiCounts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUpdatedByUserVideoGamesApiCounts(Collection $updatedByUserVideoGamesApiCounts, ConnectionInterface $con = null)
    {
        /** @var ApiCount[] $updatedByUserVideoGamesApiCountsToDelete */
        $updatedByUserVideoGamesApiCountsToDelete = $this->getUpdatedByUserVideoGamesApiCounts(new Criteria(), $con)->diff($updatedByUserVideoGamesApiCounts);


        $this->updatedByUserVideoGamesApiCountsScheduledForDeletion = $updatedByUserVideoGamesApiCountsToDelete;

        foreach ($updatedByUserVideoGamesApiCountsToDelete as $updatedByUserVideoGamesApiCountRemoved) {
            $updatedByUserVideoGamesApiCountRemoved->setUpdatedByUser(null);
        }

        $this->collUpdatedByUserVideoGamesApiCounts = null;
        foreach ($updatedByUserVideoGamesApiCounts as $updatedByUserVideoGamesApiCount) {
            $this->addUpdatedByUserVideoGamesApiCount($updatedByUserVideoGamesApiCount);
        }

        $this->collUpdatedByUserVideoGamesApiCounts = $updatedByUserVideoGamesApiCounts;
        $this->collUpdatedByUserVideoGamesApiCountsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseApiCount objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseApiCount objects.
     * @throws PropelException
     */
    public function countUpdatedByUserVideoGamesApiCounts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpdatedByUserVideoGamesApiCountsPartial && !$this->isNew();
        if (null === $this->collUpdatedByUserVideoGamesApiCounts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpdatedByUserVideoGamesApiCounts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpdatedByUserVideoGamesApiCounts());
            }

            $query = ApiCountQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUpdatedByUser($this)
                ->count($con);
        }

        return count($this->collUpdatedByUserVideoGamesApiCounts);
    }

    /**
     * Method called to associate a ApiCount object to this object
     * through the ApiCount foreign key attribute.
     *
     * @param  ApiCount $l ApiCount
     * @return $this|\IiMedias\AdminBundle\Model\User The current object (for fluent API support)
     */
    public function addUpdatedByUserVideoGamesApiCount(ApiCount $l)
    {
        if ($this->collUpdatedByUserVideoGamesApiCounts === null) {
            $this->initUpdatedByUserVideoGamesApiCounts();
            $this->collUpdatedByUserVideoGamesApiCountsPartial = true;
        }

        if (!$this->collUpdatedByUserVideoGamesApiCounts->contains($l)) {
            $this->doAddUpdatedByUserVideoGamesApiCount($l);

            if ($this->updatedByUserVideoGamesApiCountsScheduledForDeletion and $this->updatedByUserVideoGamesApiCountsScheduledForDeletion->contains($l)) {
                $this->updatedByUserVideoGamesApiCountsScheduledForDeletion->remove($this->updatedByUserVideoGamesApiCountsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ApiCount $updatedByUserVideoGamesApiCount The ApiCount object to add.
     */
    protected function doAddUpdatedByUserVideoGamesApiCount(ApiCount $updatedByUserVideoGamesApiCount)
    {
        $this->collUpdatedByUserVideoGamesApiCounts[]= $updatedByUserVideoGamesApiCount;
        $updatedByUserVideoGamesApiCount->setUpdatedByUser($this);
    }

    /**
     * @param  ApiCount $updatedByUserVideoGamesApiCount The ApiCount object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUpdatedByUserVideoGamesApiCount(ApiCount $updatedByUserVideoGamesApiCount)
    {
        if ($this->getUpdatedByUserVideoGamesApiCounts()->contains($updatedByUserVideoGamesApiCount)) {
            $pos = $this->collUpdatedByUserVideoGamesApiCounts->search($updatedByUserVideoGamesApiCount);
            $this->collUpdatedByUserVideoGamesApiCounts->remove($pos);
            if (null === $this->updatedByUserVideoGamesApiCountsScheduledForDeletion) {
                $this->updatedByUserVideoGamesApiCountsScheduledForDeletion = clone $this->collUpdatedByUserVideoGamesApiCounts;
                $this->updatedByUserVideoGamesApiCountsScheduledForDeletion->clear();
            }
            $this->updatedByUserVideoGamesApiCountsScheduledForDeletion[]= $updatedByUserVideoGamesApiCount;
            $updatedByUserVideoGamesApiCount->setUpdatedByUser(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->usrusr_id = null;
        $this->usrusr_username = null;
        $this->usrusr_email = null;
        $this->usrusr_facebook_id = null;
        $this->usrusr_google_id = null;
        $this->usrusr_twitter_id = null;
        $this->usrusr_password = null;
        $this->usrusr_salt = null;
        $this->usrusr_roles = null;
        $this->usrusr_roles_unserialized = null;
        $this->usrusr_is_active = null;
        $this->usrusr_created_at = null;
        $this->usrusr_updated_at = null;
        $this->usrusr_slug = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCreatedByUserStsites) {
                foreach ($this->collCreatedByUserStsites as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpdatedByUserStsites) {
                foreach ($this->collUpdatedByUserStsites as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCreatedByUserStstrms) {
                foreach ($this->collCreatedByUserStstrms as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpdatedByUserStstrms) {
                foreach ($this->collUpdatedByUserStstrms as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCreatedByUserStchans) {
                foreach ($this->collCreatedByUserStchans as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpdatedByUserStchans) {
                foreach ($this->collUpdatedByUserStchans as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCreatedByUserStcusrs) {
                foreach ($this->collCreatedByUserStcusrs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpdatedByUserStcusrs) {
                foreach ($this->collUpdatedByUserStcusrs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCreatedByUserStranks) {
                foreach ($this->collCreatedByUserStranks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpdatedByUserStranks) {
                foreach ($this->collUpdatedByUserStranks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCreatedByUserStavtrs) {
                foreach ($this->collCreatedByUserStavtrs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpdatedByUserStavtrs) {
                foreach ($this->collUpdatedByUserStavtrs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCreatedByUserVideoGamesApiCounts) {
                foreach ($this->collCreatedByUserVideoGamesApiCounts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpdatedByUserVideoGamesApiCounts) {
                foreach ($this->collUpdatedByUserVideoGamesApiCounts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCreatedByUserStsites = null;
        $this->collUpdatedByUserStsites = null;
        $this->collCreatedByUserStstrms = null;
        $this->collUpdatedByUserStstrms = null;
        $this->collCreatedByUserStchans = null;
        $this->collUpdatedByUserStchans = null;
        $this->collCreatedByUserStcusrs = null;
        $this->collUpdatedByUserStcusrs = null;
        $this->collCreatedByUserStranks = null;
        $this->collUpdatedByUserStranks = null;
        $this->collCreatedByUserStavtrs = null;
        $this->collUpdatedByUserStavtrs = null;
        $this->collCreatedByUserVideoGamesApiCounts = null;
        $this->collUpdatedByUserVideoGamesApiCounts = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildUser The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[UserTableMap::COL_USRUSR_UPDATED_AT] = true;

        return $this;
    }

    // sluggable behavior

    /**
     * Wrap the setter for slug value
     *
     * @param   string
     * @return  $this|User
     */
    public function setSlug($v)
    {
        return $this->setUsrusrSlug($v);
    }

    /**
     * Wrap the getter for slug value
     *
     * @return  string
     */
    public function getSlug()
    {
        return $this->getUsrusrSlug();
    }

    /**
     * Create a unique slug based on the object
     *
     * @return string The object slug
     */
    protected function createSlug()
    {
        $slug = $this->createRawSlug();
        $slug = $this->limitSlugSize($slug);
        $slug = $this->makeSlugUnique($slug);

        return $slug;
    }

    /**
     * Create the slug from the appropriate columns
     *
     * @return string
     */
    protected function createRawSlug()
    {
        return '' . $this->cleanupSlugPart($this->getUsername()) . '';
    }

    /**
     * Cleanup a string to make a slug of it
     * Removes special characters, replaces blanks with a separator, and trim it
     *
     * @param     string $slug        the text to slugify
     * @param     string $replacement the separator used by slug
     * @return    string               the slugified text
     */
    protected static function cleanupSlugPart($slug, $replacement = '-')
    {
        // transliterate
        if (function_exists('iconv')) {
            $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
        }

        // lowercase
        if (function_exists('mb_strtolower')) {
            $slug = mb_strtolower($slug);
        } else {
            $slug = strtolower($slug);
        }

        // remove accents resulting from OSX's iconv
        $slug = str_replace(array('\'', '`', '^'), '', $slug);

        // replace non letter or digits with separator
        $slug = preg_replace('/[^\w\/]+/u', $replacement, $slug);

        // trim
        $slug = trim($slug, $replacement);

        if (empty($slug)) {
            return 'n-a';
        }

        return $slug;
    }


    /**
     * Make sure the slug is short enough to accommodate the column size
     *
     * @param    string $slug            the slug to check
     *
     * @return string                        the truncated slug
     */
    protected static function limitSlugSize($slug, $incrementReservedSpace = 3)
    {
        // check length, as suffix could put it over maximum
        if (strlen($slug) > (255 - $incrementReservedSpace)) {
            $slug = substr($slug, 0, 255 - $incrementReservedSpace);
        }

        return $slug;
    }


    /**
     * Get the slug, ensuring its uniqueness
     *
     * @param    string $slug            the slug to check
     * @param    string $separator       the separator used by slug
     * @param    int    $alreadyExists   false for the first try, true for the second, and take the high count + 1
     * @return   string                   the unique slug
     */
    protected function makeSlugUnique($slug, $separator = '-', $alreadyExists = false)
    {
        if (!$alreadyExists) {
            $slug2 = $slug;
        } else {
            $slug2 = $slug . $separator;
        }

        $adapter = \Propel\Runtime\Propel::getServiceContainer()->getAdapter('default');
        $col = 'q.UsrusrSlug';
        $compare = $alreadyExists ? $adapter->compareRegex($col, '?') : sprintf('%s = ?', $col);

        $query = \IiMedias\AdminBundle\Model\UserQuery::create('q')
            ->where($compare, $alreadyExists ? '^' . $slug2 . '[0-9]+$' : $slug2)
            ->prune($this)
        ;

        if (!$alreadyExists) {
            $count = $query->count();
            if ($count > 0) {
                return $this->makeSlugUnique($slug, $separator, true);
            }

            return $slug2;
        }

        $adapter = \Propel\Runtime\Propel::getServiceContainer()->getAdapter('default');
        // Already exists
        $object = $query
            ->addDescendingOrderByColumn($adapter->strLength('usrusr_slug'))
            ->addDescendingOrderByColumn('usrusr_slug')
        ->findOne();

        // First duplicate slug
        if (null == $object) {
            return $slug2 . '1';
        }

        $slugNum = substr($object->getUsrusrSlug(), strlen($slug) + 1);
        if (0 == $slugNum[0]) {
            $slugNum[0] = 1;
        }

        return $slug2 . ($slugNum + 1);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
