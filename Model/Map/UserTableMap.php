<?php

namespace IiMedias\AdminBundle\Model\Map;

use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;
use IiMedias\StreamBundle\Model\Map\AvatarTableMap;
use IiMedias\StreamBundle\Model\Map\ChannelTableMap;
use IiMedias\StreamBundle\Model\Map\ChatUserTableMap;
use IiMedias\StreamBundle\Model\Map\RankTableMap;
use IiMedias\StreamBundle\Model\Map\SiteTableMap;
use IiMedias\StreamBundle\Model\Map\StreamTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiCountTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'user_user_usrusr' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.AdminBundle.Model.Map.UserTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'user_user_usrusr';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\AdminBundle\\Model\\User';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.AdminBundle.Model.User';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the usrusr_id field
     */
    const COL_USRUSR_ID = 'user_user_usrusr.usrusr_id';

    /**
     * the column name for the usrusr_username field
     */
    const COL_USRUSR_USERNAME = 'user_user_usrusr.usrusr_username';

    /**
     * the column name for the usrusr_email field
     */
    const COL_USRUSR_EMAIL = 'user_user_usrusr.usrusr_email';

    /**
     * the column name for the usrusr_facebook_id field
     */
    const COL_USRUSR_FACEBOOK_ID = 'user_user_usrusr.usrusr_facebook_id';

    /**
     * the column name for the usrusr_google_id field
     */
    const COL_USRUSR_GOOGLE_ID = 'user_user_usrusr.usrusr_google_id';

    /**
     * the column name for the usrusr_twitter_id field
     */
    const COL_USRUSR_TWITTER_ID = 'user_user_usrusr.usrusr_twitter_id';

    /**
     * the column name for the usrusr_password field
     */
    const COL_USRUSR_PASSWORD = 'user_user_usrusr.usrusr_password';

    /**
     * the column name for the usrusr_salt field
     */
    const COL_USRUSR_SALT = 'user_user_usrusr.usrusr_salt';

    /**
     * the column name for the usrusr_roles field
     */
    const COL_USRUSR_ROLES = 'user_user_usrusr.usrusr_roles';

    /**
     * the column name for the usrusr_is_active field
     */
    const COL_USRUSR_IS_ACTIVE = 'user_user_usrusr.usrusr_is_active';

    /**
     * the column name for the usrusr_created_at field
     */
    const COL_USRUSR_CREATED_AT = 'user_user_usrusr.usrusr_created_at';

    /**
     * the column name for the usrusr_updated_at field
     */
    const COL_USRUSR_UPDATED_AT = 'user_user_usrusr.usrusr_updated_at';

    /**
     * the column name for the usrusr_slug field
     */
    const COL_USRUSR_SLUG = 'user_user_usrusr.usrusr_slug';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Username', 'Email', 'FacebookId', 'GoogleId', 'TwitterId', 'Password', 'Salt', 'Roles', 'IsActive', 'CreatedAt', 'UpdatedAt', 'UsrusrSlug', ),
        self::TYPE_CAMELNAME     => array('id', 'username', 'email', 'facebookId', 'googleId', 'twitterId', 'password', 'salt', 'roles', 'isActive', 'createdAt', 'updatedAt', 'usrusrSlug', ),
        self::TYPE_COLNAME       => array(UserTableMap::COL_USRUSR_ID, UserTableMap::COL_USRUSR_USERNAME, UserTableMap::COL_USRUSR_EMAIL, UserTableMap::COL_USRUSR_FACEBOOK_ID, UserTableMap::COL_USRUSR_GOOGLE_ID, UserTableMap::COL_USRUSR_TWITTER_ID, UserTableMap::COL_USRUSR_PASSWORD, UserTableMap::COL_USRUSR_SALT, UserTableMap::COL_USRUSR_ROLES, UserTableMap::COL_USRUSR_IS_ACTIVE, UserTableMap::COL_USRUSR_CREATED_AT, UserTableMap::COL_USRUSR_UPDATED_AT, UserTableMap::COL_USRUSR_SLUG, ),
        self::TYPE_FIELDNAME     => array('usrusr_id', 'usrusr_username', 'usrusr_email', 'usrusr_facebook_id', 'usrusr_google_id', 'usrusr_twitter_id', 'usrusr_password', 'usrusr_salt', 'usrusr_roles', 'usrusr_is_active', 'usrusr_created_at', 'usrusr_updated_at', 'usrusr_slug', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Username' => 1, 'Email' => 2, 'FacebookId' => 3, 'GoogleId' => 4, 'TwitterId' => 5, 'Password' => 6, 'Salt' => 7, 'Roles' => 8, 'IsActive' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, 'UsrusrSlug' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'username' => 1, 'email' => 2, 'facebookId' => 3, 'googleId' => 4, 'twitterId' => 5, 'password' => 6, 'salt' => 7, 'roles' => 8, 'isActive' => 9, 'createdAt' => 10, 'updatedAt' => 11, 'usrusrSlug' => 12, ),
        self::TYPE_COLNAME       => array(UserTableMap::COL_USRUSR_ID => 0, UserTableMap::COL_USRUSR_USERNAME => 1, UserTableMap::COL_USRUSR_EMAIL => 2, UserTableMap::COL_USRUSR_FACEBOOK_ID => 3, UserTableMap::COL_USRUSR_GOOGLE_ID => 4, UserTableMap::COL_USRUSR_TWITTER_ID => 5, UserTableMap::COL_USRUSR_PASSWORD => 6, UserTableMap::COL_USRUSR_SALT => 7, UserTableMap::COL_USRUSR_ROLES => 8, UserTableMap::COL_USRUSR_IS_ACTIVE => 9, UserTableMap::COL_USRUSR_CREATED_AT => 10, UserTableMap::COL_USRUSR_UPDATED_AT => 11, UserTableMap::COL_USRUSR_SLUG => 12, ),
        self::TYPE_FIELDNAME     => array('usrusr_id' => 0, 'usrusr_username' => 1, 'usrusr_email' => 2, 'usrusr_facebook_id' => 3, 'usrusr_google_id' => 4, 'usrusr_twitter_id' => 5, 'usrusr_password' => 6, 'usrusr_salt' => 7, 'usrusr_roles' => 8, 'usrusr_is_active' => 9, 'usrusr_created_at' => 10, 'usrusr_updated_at' => 11, 'usrusr_slug' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_user_usrusr');
        $this->setPhpName('User');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\AdminBundle\\Model\\User');
        $this->setPackage('src.IiMedias.AdminBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('usrusr_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('usrusr_username', 'Username', 'VARCHAR', true, 255, null);
        $this->addColumn('usrusr_email', 'Email', 'VARCHAR', true, 255, null);
        $this->addColumn('usrusr_facebook_id', 'FacebookId', 'VARCHAR', false, 255, null);
        $this->addColumn('usrusr_google_id', 'GoogleId', 'VARCHAR', false, 255, null);
        $this->addColumn('usrusr_twitter_id', 'TwitterId', 'VARCHAR', false, 255, null);
        $this->addColumn('usrusr_password', 'Password', 'VARCHAR', true, 255, null);
        $this->addColumn('usrusr_salt', 'Salt', 'VARCHAR', true, 255, null);
        $this->addColumn('usrusr_roles', 'Roles', 'ARRAY', false, null, null);
        $this->addColumn('usrusr_is_active', 'IsActive', 'BOOLEAN', true, 1, false);
        $this->addColumn('usrusr_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('usrusr_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('usrusr_slug', 'UsrusrSlug', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CreatedByUserStsite', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stsite_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'CreatedByUserStsites', false);
        $this->addRelation('UpdatedByUserStsite', '\\IiMedias\\StreamBundle\\Model\\Site', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stsite_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'UpdatedByUserStsites', false);
        $this->addRelation('CreatedByUserStstrm', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':ststrm_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'CreatedByUserStstrms', false);
        $this->addRelation('UpdatedByUserStstrm', '\\IiMedias\\StreamBundle\\Model\\Stream', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':ststrm_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'UpdatedByUserStstrms', false);
        $this->addRelation('CreatedByUserStchan', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stchan_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'CreatedByUserStchans', false);
        $this->addRelation('UpdatedByUserStchan', '\\IiMedias\\StreamBundle\\Model\\Channel', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stchan_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'UpdatedByUserStchans', false);
        $this->addRelation('CreatedByUserStcusr', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcusr_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'CreatedByUserStcusrs', false);
        $this->addRelation('UpdatedByUserStcusr', '\\IiMedias\\StreamBundle\\Model\\ChatUser', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stcusr_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'UpdatedByUserStcusrs', false);
        $this->addRelation('CreatedByUserStrank', '\\IiMedias\\StreamBundle\\Model\\Rank', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':strank_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'CreatedByUserStranks', false);
        $this->addRelation('UpdatedByUserStrank', '\\IiMedias\\StreamBundle\\Model\\Rank', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':strank_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'UpdatedByUserStranks', false);
        $this->addRelation('CreatedByUserStavtr', '\\IiMedias\\StreamBundle\\Model\\Avatar', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stavtr_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'CreatedByUserStavtrs', false);
        $this->addRelation('UpdatedByUserStavtr', '\\IiMedias\\StreamBundle\\Model\\Avatar', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':stavtr_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'UpdatedByUserStavtrs', false);
        $this->addRelation('CreatedByUserVideoGamesApiCount', '\\IiMedias\\VideoGamesBundle\\Model\\ApiCount', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgapic_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'CreatedByUserVideoGamesApiCounts', false);
        $this->addRelation('UpdatedByUserVideoGamesApiCount', '\\IiMedias\\VideoGamesBundle\\Model\\ApiCount', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgapic_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', 'UpdatedByUserVideoGamesApiCounts', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'usrusr_created_at', 'update_column' => 'usrusr_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'sluggable' => array('slug_column' => 'usrusr_slug', 'slug_pattern' => '{Username}', 'replace_pattern' => '/[^\w\/]+/u', 'replacement' => '-', 'separator' => '-', 'permanent' => 'true', 'scope_column' => '', 'unique_constraint' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to user_user_usrusr     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SiteTableMap::clearInstancePool();
        StreamTableMap::clearInstancePool();
        ChannelTableMap::clearInstancePool();
        ChatUserTableMap::clearInstancePool();
        RankTableMap::clearInstancePool();
        AvatarTableMap::clearInstancePool();
        ApiCountTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserTableMap::CLASS_DEFAULT : UserTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (User object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserTableMap::OM_CLASS;
            /** @var User $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var User $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_ID);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_USERNAME);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_EMAIL);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_FACEBOOK_ID);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_GOOGLE_ID);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_TWITTER_ID);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_PASSWORD);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_SALT);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_ROLES);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_IS_ACTIVE);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_CREATED_AT);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_UPDATED_AT);
            $criteria->addSelectColumn(UserTableMap::COL_USRUSR_SLUG);
        } else {
            $criteria->addSelectColumn($alias . '.usrusr_id');
            $criteria->addSelectColumn($alias . '.usrusr_username');
            $criteria->addSelectColumn($alias . '.usrusr_email');
            $criteria->addSelectColumn($alias . '.usrusr_facebook_id');
            $criteria->addSelectColumn($alias . '.usrusr_google_id');
            $criteria->addSelectColumn($alias . '.usrusr_twitter_id');
            $criteria->addSelectColumn($alias . '.usrusr_password');
            $criteria->addSelectColumn($alias . '.usrusr_salt');
            $criteria->addSelectColumn($alias . '.usrusr_roles');
            $criteria->addSelectColumn($alias . '.usrusr_is_active');
            $criteria->addSelectColumn($alias . '.usrusr_created_at');
            $criteria->addSelectColumn($alias . '.usrusr_updated_at');
            $criteria->addSelectColumn($alias . '.usrusr_slug');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserTableMap::DATABASE_NAME)->getTable(UserTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a User or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or User object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\AdminBundle\Model\User) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserTableMap::DATABASE_NAME);
            $criteria->add(UserTableMap::COL_USRUSR_ID, (array) $values, Criteria::IN);
        }

        $query = UserQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the user_user_usrusr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a User or Criteria object.
     *
     * @param mixed               $criteria Criteria or User object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from User object
        }

        if ($criteria->containsKey(UserTableMap::COL_USRUSR_ID) && $criteria->keyContainsValue(UserTableMap::COL_USRUSR_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserTableMap::COL_USRUSR_ID.')');
        }


        // Set the correct dbName
        $query = UserQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserTableMap::buildTableMap();
