<?php

namespace IiMedias\AdminBundle\Model\Map;

use IiMedias\AdminBundle\Model\MenuElement;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'menu_element_menelm' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class MenuElementTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.AdminBundle.Model.Map.MenuElementTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'menu_element_menelm';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\AdminBundle\\Model\\MenuElement';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.AdminBundle.Model.MenuElement';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the menelm_id field
     */
    const COL_MENELM_ID = 'menu_element_menelm.menelm_id';

    /**
     * the column name for the menelm_mengrp_id field
     */
    const COL_MENELM_MENGRP_ID = 'menu_element_menelm.menelm_mengrp_id';

    /**
     * the column name for the menelm_code field
     */
    const COL_MENELM_CODE = 'menu_element_menelm.menelm_code';

    /**
     * the column name for the menelm_position field
     */
    const COL_MENELM_POSITION = 'menu_element_menelm.menelm_position';

    /**
     * the column name for the menelm_message field
     */
    const COL_MENELM_MESSAGE = 'menu_element_menelm.menelm_message';

    /**
     * the column name for the menelm_icon_fa field
     */
    const COL_MENELM_ICON_FA = 'menu_element_menelm.menelm_icon_fa';

    /**
     * the column name for the menelm_route_name field
     */
    const COL_MENELM_ROUTE_NAME = 'menu_element_menelm.menelm_route_name';

    /**
     * the column name for the menelm_route_params field
     */
    const COL_MENELM_ROUTE_PARAMS = 'menu_element_menelm.menelm_route_params';

    /**
     * the column name for the menelm_role field
     */
    const COL_MENELM_ROLE = 'menu_element_menelm.menelm_role';

    /**
     * the column name for the menelm_created_at field
     */
    const COL_MENELM_CREATED_AT = 'menu_element_menelm.menelm_created_at';

    /**
     * the column name for the menelm_updated_at field
     */
    const COL_MENELM_UPDATED_AT = 'menu_element_menelm.menelm_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'MenuGroupId', 'Code', 'Position', 'Message', 'IconFontAwesome', 'RouteName', 'RouteParams', 'Role', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'menuGroupId', 'code', 'position', 'message', 'iconFontAwesome', 'routeName', 'routeParams', 'role', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(MenuElementTableMap::COL_MENELM_ID, MenuElementTableMap::COL_MENELM_MENGRP_ID, MenuElementTableMap::COL_MENELM_CODE, MenuElementTableMap::COL_MENELM_POSITION, MenuElementTableMap::COL_MENELM_MESSAGE, MenuElementTableMap::COL_MENELM_ICON_FA, MenuElementTableMap::COL_MENELM_ROUTE_NAME, MenuElementTableMap::COL_MENELM_ROUTE_PARAMS, MenuElementTableMap::COL_MENELM_ROLE, MenuElementTableMap::COL_MENELM_CREATED_AT, MenuElementTableMap::COL_MENELM_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('menelm_id', 'menelm_mengrp_id', 'menelm_code', 'menelm_position', 'menelm_message', 'menelm_icon_fa', 'menelm_route_name', 'menelm_route_params', 'menelm_role', 'menelm_created_at', 'menelm_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'MenuGroupId' => 1, 'Code' => 2, 'Position' => 3, 'Message' => 4, 'IconFontAwesome' => 5, 'RouteName' => 6, 'RouteParams' => 7, 'Role' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'menuGroupId' => 1, 'code' => 2, 'position' => 3, 'message' => 4, 'iconFontAwesome' => 5, 'routeName' => 6, 'routeParams' => 7, 'role' => 8, 'createdAt' => 9, 'updatedAt' => 10, ),
        self::TYPE_COLNAME       => array(MenuElementTableMap::COL_MENELM_ID => 0, MenuElementTableMap::COL_MENELM_MENGRP_ID => 1, MenuElementTableMap::COL_MENELM_CODE => 2, MenuElementTableMap::COL_MENELM_POSITION => 3, MenuElementTableMap::COL_MENELM_MESSAGE => 4, MenuElementTableMap::COL_MENELM_ICON_FA => 5, MenuElementTableMap::COL_MENELM_ROUTE_NAME => 6, MenuElementTableMap::COL_MENELM_ROUTE_PARAMS => 7, MenuElementTableMap::COL_MENELM_ROLE => 8, MenuElementTableMap::COL_MENELM_CREATED_AT => 9, MenuElementTableMap::COL_MENELM_UPDATED_AT => 10, ),
        self::TYPE_FIELDNAME     => array('menelm_id' => 0, 'menelm_mengrp_id' => 1, 'menelm_code' => 2, 'menelm_position' => 3, 'menelm_message' => 4, 'menelm_icon_fa' => 5, 'menelm_route_name' => 6, 'menelm_route_params' => 7, 'menelm_role' => 8, 'menelm_created_at' => 9, 'menelm_updated_at' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('menu_element_menelm');
        $this->setPhpName('MenuElement');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\AdminBundle\\Model\\MenuElement');
        $this->setPackage('src.IiMedias.AdminBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('menelm_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('menelm_mengrp_id', 'MenuGroupId', 'INTEGER', 'menu_group_mengrp', 'mengrp_id', true, null, null);
        $this->addColumn('menelm_code', 'Code', 'VARCHAR', true, 255, null);
        $this->addColumn('menelm_position', 'Position', 'INTEGER', true, null, null);
        $this->addColumn('menelm_message', 'Message', 'VARCHAR', true, 255, null);
        $this->addColumn('menelm_icon_fa', 'IconFontAwesome', 'VARCHAR', true, 255, null);
        $this->addColumn('menelm_route_name', 'RouteName', 'VARCHAR', true, 255, null);
        $this->addColumn('menelm_route_params', 'RouteParams', 'VARCHAR', true, 255, null);
        $this->addColumn('menelm_role', 'Role', 'VARCHAR', true, 255, null);
        $this->addColumn('menelm_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('menelm_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('MenuGroup', '\\IiMedias\\AdminBundle\\Model\\MenuGroup', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':menelm_mengrp_id',
    1 => ':mengrp_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'menelm_created_at', 'update_column' => 'menelm_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? MenuElementTableMap::CLASS_DEFAULT : MenuElementTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (MenuElement object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = MenuElementTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MenuElementTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MenuElementTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MenuElementTableMap::OM_CLASS;
            /** @var MenuElement $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MenuElementTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MenuElementTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MenuElementTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var MenuElement $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MenuElementTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_ID);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_MENGRP_ID);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_CODE);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_POSITION);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_MESSAGE);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_ICON_FA);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_ROUTE_NAME);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_ROUTE_PARAMS);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_ROLE);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_CREATED_AT);
            $criteria->addSelectColumn(MenuElementTableMap::COL_MENELM_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.menelm_id');
            $criteria->addSelectColumn($alias . '.menelm_mengrp_id');
            $criteria->addSelectColumn($alias . '.menelm_code');
            $criteria->addSelectColumn($alias . '.menelm_position');
            $criteria->addSelectColumn($alias . '.menelm_message');
            $criteria->addSelectColumn($alias . '.menelm_icon_fa');
            $criteria->addSelectColumn($alias . '.menelm_route_name');
            $criteria->addSelectColumn($alias . '.menelm_route_params');
            $criteria->addSelectColumn($alias . '.menelm_role');
            $criteria->addSelectColumn($alias . '.menelm_created_at');
            $criteria->addSelectColumn($alias . '.menelm_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(MenuElementTableMap::DATABASE_NAME)->getTable(MenuElementTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(MenuElementTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(MenuElementTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new MenuElementTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a MenuElement or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or MenuElement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MenuElementTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\AdminBundle\Model\MenuElement) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MenuElementTableMap::DATABASE_NAME);
            $criteria->add(MenuElementTableMap::COL_MENELM_ID, (array) $values, Criteria::IN);
        }

        $query = MenuElementQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MenuElementTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MenuElementTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the menu_element_menelm table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return MenuElementQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a MenuElement or Criteria object.
     *
     * @param mixed               $criteria Criteria or MenuElement object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MenuElementTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from MenuElement object
        }

        if ($criteria->containsKey(MenuElementTableMap::COL_MENELM_ID) && $criteria->keyContainsValue(MenuElementTableMap::COL_MENELM_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.MenuElementTableMap::COL_MENELM_ID.')');
        }


        // Set the correct dbName
        $query = MenuElementQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // MenuElementTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
MenuElementTableMap::buildTableMap();
