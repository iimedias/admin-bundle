<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\LocaleQuery as BaseLocaleQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'lang_locale_lanloc' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LocaleQuery extends BaseLocaleQuery
{
    public static function getAll()
    {
        $locales = self::create()
//            ->setQueryKey('LocaleGetAll')
            ->orderByCode()
            ->find()
        ;

        return $locales;
    }

    public function addLocale($code, $name, $flag)
    {
        $locale = self::create()
            ->filterByCode($code)
            ->findOne()
        ;

        if (is_null($locale)) {
            $locale = new Locale();
            $locale
                ->setCode($code)
                ->setName($name)
                ->setFlag($flag)
                ->save()
            ;
        }

        return $locale;
    }

    public function addTranslation($code, $translationArray)
    {
        $locales = self::getAll();
        $message = MessageQuery::getOneByCodeOrCreate($code);

        foreach ($locales as $locale) {
            $translation = isset($translationArray[$locale->getCode()]) ? $translationArray[$locale->getCode()] : '';
            MessageLocaleQuery::addTranslation($locale, $message, $translation);
        }
    }

    public function addTranslations($translationsArray)
    {
        $locales = self::getAll();

        foreach ($translationsArray as $code => $translations) {
            $message = MessageQuery::getOneByCodeOrCreate($code);

            foreach ($locales as $locale) {
                $translation = isset($translations[$locale->getCode()]) ? $translations[$locale->getCode()] : '';
                MessageLocaleQuery::addTranslation($locale, $message, $translation);
            }
        }
    }
}
