<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\Locale as BaseLocale;

/**
 * Skeleton subclass for representing a row from the 'lang_locale_lanloc' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Locale extends BaseLocale
{

}
