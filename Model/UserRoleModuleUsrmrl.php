<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\UserRoleModuleUsrmrl as BaseUserRoleModuleUsrmrl;

/**
 * Skeleton subclass for representing a row from the 'user_role_module_usrmrl' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserRoleModuleUsrmrl extends BaseUserRoleModuleUsrmrl
{

}
