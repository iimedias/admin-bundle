<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\MessageLocaleQuery as BaseMessageLocaleQuery;
use IiMedias\AdminBundle\Model\MessageLocale;

/**
 * Skeleton subclass for performing query and update operations on the 'lang_message_label_lamsgl' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MessageLocaleQuery extends BaseMessageLocaleQuery
{
    public function getAll()
    {
        $messageLocales = self::create()
            ->find();

        return $messageLocales;
    }

    public function getOneById($id)
    {
        $messageLocale = self::create()
            ->filterById($id)
            ->findOne()
        ;

        return $messageLocale;
    }

    public function findOneByLocaleAndMessage($locale, $message)
    {
        $messageLocale = self::create()
            ->filterByLocale($locale)
            ->filterByMessage($message)
            ->findOne()
        ;

        return $messageLocale;
    }

    public function addTranslation($locale, $message, $translation)
    {
        $messageLocale = self::findOneByLocaleAndMessage($locale, $message);

        if (is_null($messageLocale)) {
            $messageLocale = new MessageLocale();
            $messageLocale
                ->setLocale($locale)
                ->setMessage($message)
                ->setLabel($translation)
                ->save()
            ;
        }

        return $messageLocale;
    }
}
