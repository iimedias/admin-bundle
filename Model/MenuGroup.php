<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\MenuGroup as BaseMenuGroup;
use IiMedias\AdminBundle\Model\MenuGroupQuery;

/**
 * Skeleton subclass for representing a row from the 'menu_group_mengrp' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MenuGroup extends BaseMenuGroup
{
    protected $config = null;
    
    public function getConfig()
    {
        $menuElements = null;
        if (is_null($this->config)) {
            $menuElements = MenuGroupQuery::getElementsByMenuGroupName($this->getName());
        }
        

        return $menuElements;
    }
}
