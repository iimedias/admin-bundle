<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\MenuElement as BaseMenuElement;

/**
 * Skeleton subclass for representing a row from the 'menu_element_menelm' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MenuElement extends BaseMenuElement
{
    public function getRouteParams()
    {
        return json_decode(parent::getRouteParams());
    }

    public function setRouteParams($v)
    {
        return parent::setRouteParams(json_encode($v));
    }

    public function getRouteParamsJson()
    {
        return parent::getRouteParams();
    }

    public function setRouteParamsJson($routeParamsJson)
    {
        parent::setRouteParams(json_encode(json_decode($routeParamsJson)));
        return $this;
    }

    public function positionUp()
    {
        $this
            ->setPosition($this->getPosition() - 1)
            ->save()
        ;

        return $this;
    }

    public function positionDown()
    {
        $this
            ->setPosition($this->getPosition() + 1)
            ->save()
        ;

        return $this;
    }

    public function canAccess(User $user)
    {
        if ($this->getRole() == '' || is_null($this->getRole())) {
            return true;
        }
        elseif (in_array('ROLE_ROOT', $user->getRoles())) {
            return true;
        }
        elseif (in_array($this->getRole(), $user->getRoles())) {
            return true;
        }
        return false;
    }
}
