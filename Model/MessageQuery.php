<?php

namespace IiMedias\AdminBundle\Model;

use IiMedias\AdminBundle\Model\Base\MessageQuery as BaseMessageQuery;
use IiMedias\AdminBundle\Model\Message;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'lang_message_lanmsg' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MessageQuery extends BaseMessageQuery
{
    public function getAll()
    {
        $messages = self::create()
            ->orderByCode(Criteria::ASC)
            ->find()
        ;

        return $messages;
    }

    public function getOneByCode($code)
    {
        $message = self::create()
            ->filterByCode($code)
            ->findOne();

        return $message;
    }

    public function getOneByCodeOrCreate($code)
    {
        $message = self::getOneByCode($code);

        if (is_null($message)) {
            $message = new Message();
            $message
                ->setCode($code)
                ->save()
            ;
        }

        return $message;
    }
}
