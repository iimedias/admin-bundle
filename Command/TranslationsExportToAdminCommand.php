<?php

namespace IiMedias\AdminBundle\Command;

use IiMedias\AdminBundle\Model\MessageLocaleQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use IiMedias\AdminBundle\Model\LocaleQuery;

class TranslationsExportToAdminCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('translations:exporttoadmin')
            ->setDescription('Récupère la configuration pour le serveur nodejs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fs              = new Filesystem();
        $resourcesPath   = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources');
        $translationPath = $resourcesPath . DIRECTORY_SEPARATOR . 'translations';
        if (!$fs->exists($translationPath)) {
            $fs->mkdir($translationPath);
        }

        $translations = MessageLocaleQuery::getAll();

        $export = array();
        foreach ($translations as $translation) {
            $localeCode = $translation->getLocale()->getCode();
            if (!isset($export[$localeCode])) {
                $export[$localeCode] = array();
            }
            $export[$localeCode] = $this->addArrayChild($export[$localeCode], $translation->getMessage()->getCode(), $translation->getLabel());
        }

        $translationPath = realpath($translationPath);
        foreach ($export as $locale => $translationsArray) {
            $translationsYml = Yaml::dump($translationsArray, 1024, 4);
            file_put_contents($translationPath . DIRECTORY_SEPARATOR . 'messages.' . $locale . '.yml', $translationsYml);
        }
    }

    private function addArrayChild($array, $key, $value)
    {
        $keyExplode = explode('.', $key);
        if (count($keyExplode) == 1) {
            $array[$key] = $value;
        }
        elseif (count($keyExplode) > 1) {
            $firstKey         = $keyExplode[0];
            for ($i = 1, $newKeyExplode = array(); isset($keyExplode[$i]); $i++) {
                $newKeyExplode[] = $keyExplode[$i];
            }
            if (!isset($array[$firstKey])) {
                $array[$firstKey] = array();
            }
            $array[$firstKey] = $this->addArrayChild($array[$firstKey], implode('.', $newKeyExplode), $value);
        }
        ksort($array);
        return $array;
    }
}
