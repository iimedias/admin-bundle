<?php

namespace IiMedias\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class NodeJsConstructCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nodejs:construct')
            ->setDescription('Construction de l\'application NodeJS')
            ->addOption('simulate', null, InputOption::VALUE_NONE, 'Simuler la sortie ?')
        ;
    }
    
    protected function compress($buffer) {
        /* Suppression des commentaires */
        $buffer = preg_replace("/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/", '', $buffer);
        /* Suprression des tabs, espaces, nouvelles lignes, ... */
        $buffer = str_replace(["\r\n", "\r", "\t", "\n", '  ', '    ', '     '], '', $buffer);
        /* Suppression des espaces avant après les parenthèses */
        $buffer = preg_replace(['(( )+\))','(\)( )+)'], ')', $buffer);
        return $buffer;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_output              = $output;
        $this->_nodejsConfig        = $this->getContainer()->getParameter('iimedias_config_nodejs');
        $this->_phpCliPath          = $this->getContainer()->getParameter('php_cli_path');
        $this->_twig                = $this->getContainer()->get('templating');
        $this->_functions           = [];
        $this->_nodelibs            = [];
        $this->_singleExecs         = [];
        $this->_repeatExecs         = [];
        $this->_ioOns               = [];
        $this->_ioEmits             = [];
        $this->_dependencyFunctions = [];
        
        /* Gestion des fonctions enregistrées */
        foreach ($this->_nodejsConfig['functions'] as $functionParams) {
            
            dump($functionParams);
            
            /* Est-ce qu'une fonction du même nom est déjà définie ? */
            if (($functionParams['io_emit'] === true && $functionParams['io_on'] !== true && !isset($this->_ioEmits[$functionParams['name']]))
                || ($functionParams['io_on'] === true && $functionParams['io_emit'] !== true && !isset($this->_ioOns[$functionParams['name']]))
                || ($functionParams['io_emit'] !== true && $functionParams['io_on'] !== true && !isset($this->_functions[$functionParams['name']]))) {
                $renderFunction = $this->_twig->render($functionParams['twig_template'], ['phpCliPath' => $this->_phpCliPath, 'ioEmits' => $this->_ioEmits, 'ioOns' => $this->_ioOns]);
                if ($functionParams['io_emit'] === true) {
                    $this->_ioEmits[$functionParams['name']] = $renderFunction;
                } elseif ($functionParams['io_on'] === true) {
                    $this->_ioOns[$functionParams['name']] = $renderFunction;
                } else {
                    $this->_functions[$functionParams['name']] = $renderFunction;
                }
                $this->_output->writeln($functionParams['name'] . ' -> Ajout de la fonction');

                /* Ajout des librairies nodejs nécessaires pour la fonction */
                if (isset($functionParams['node_libs']) && is_array($functionParams['node_libs'])) {
                    foreach ($functionParams['node_libs'] as $nodeLibParams) {
                        if (!in_array($nodeLibParams['name'], $this->_nodelibs)) {
                            $this->_nodelibs[$nodeLibParams['name']] = $nodeLibParams;
                            $this->_output->writeln($functionParams['name'] . ' -> Ajout de la lib nodejs ' . $nodeLibParams['name']);
                        }
                    }
                }
                
                /* Ajout des fonctions dépendentes */
                if (isset($functionParams['dependency_functions']) && is_array($functionParams['dependency_functions'])) {
                    foreach ($functionParams['dependency_functions'] as $dependencyFunction) {
                        if (!in_array($dependencyFunction, $this->_dependencyFunctions)) {
                            $this->_dependencyFunctions[] = $dependencyFunction;
                            $this->_output->writeln($functionParams['name'] . ' -> Dépendant de la fonction ' . $dependencyFunction);
                        }
                    }
                }
                
                /* Est-ce une fonction à éxécuter dès le début ? */
                if (isset($functionParams['start_exec']) && is_bool($functionParams['start_exec']) && $functionParams['start_exec'] === true) {
                    $this->_singleExecs[] = $functionParams['name'];
                    $this->_output->writeln($functionParams['name'] . ' -> Exécution au démarrage');
                }
                
                /* Est-ce une fonction à répéter ? */
//                if (isset($functionParams['repeatDelay']) && is_int($functionParams['repeatDelay']) && $functionParams['repeatDelay'] > 0) {
//                    $this->_repeatExecs[] = ['name' => $functionParams['name'], 'delay' => $functionParams['repeatDelay']];
//                }
            }
            else {
                $this->_output->writeln($functionParams['name'] . ' -> Fonction déjà existante');
            }
            $this->_output->writeln(PHP_EOL . '********** ********** ********** ********** **********' . PHP_EOL);
        }

        ksort($this->_functions);
        ksort($this->_nodelibs);
        sort($this->_singleExecs);
        

        /* Construction du contenu du fichier nodejs */
        $js = '';

        foreach ($this->_nodelibs as $nodejsLibParams) {
            $js .= 'var ' . $nodejsLibParams['name'] . ' = require("' . $nodejsLibParams['node_lib'] . '")' . (!is_null($nodejsLibParams['node_sub_lib']) ? '.' . $nodejsLibParams['node_sub_lib'] : '') . ';' . PHP_EOL;
        }

        $js .= PHP_EOL. 'var settings = null;' . PHP_EOL . PHP_EOL;

        foreach ($this->_functions as $functionContent) {
            $js .= $functionContent;
        }
        $js .= PHP_EOL;
        
        $js .= 'console.log("Serveur NodeJS démarré");' . PHP_EOL;

        foreach ($this->_singleExecs as $singleExec) {
            $js .= $singleExec . '();' . PHP_EOL;
        }
        $js .= PHP_EOL;

        /* Impression du fichier nodejs */
        if ($input->getOption('simulate')) {
            $this->_output->writeln($js);
        } else {
//            $js = $this->compress($js);
            $oldContent = file_get_contents('node.js');
            if ($oldContent != $js) {
                file_put_contents('node.js', $js);
            }
        }
    }
}
