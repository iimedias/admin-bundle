<?php

namespace IiMedias\AdminBundle\Command;

use \Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use IiMedias\AdminBundle\Model\MenuElementQuery;
use IiMedias\AdminBundle\Model\LocaleQuery;

class BundlesConfigInstallCommand extends ContainerAwareCommand
{
    protected $subConfigCmdList;
    protected $subConfigBundleList;

    protected function configure()
    {
        $this
            ->setName('bundles:config')
            ->setDescription('Récupère la configuration pour le serveur nodejs')
        ;
    }

    protected function getAllSubConfigCmd()
    {
        $commandList  = $this->getApplication()->find('list');
        $inputList    = new ArrayInput(array());
        $outputList   = new BufferedOutput();
        $commandList->run($inputList, $outputList);
        preg_match_all('# bundles:([a-zA-Z0-9]*):config #', $outputList->fetch(), $matchesList);
        $this->subConfigCmdList    = $matchesList[0];
        $this->subConfigBundleList = $matchesList[1];
    }

    protected function setLocales()
    {
        LocaleQuery::addLocale('fr', 'Français', 'flag_france');
        LocaleQuery::addLocale('en', 'English', 'flag_great_britain');
        LocaleQuery::addLocale('es', 'Español', 'flag_spain');
    }

    protected function setMessages()
    {
        $translations = array();

        /* Commun */
        $translations['admin.title']                     = array('fr' => 'Admin / %sitename%', 'en' => 'Admin %sitename%');
        $translations['admin.head.hello']                = array('fr' => 'Bonjour %username%', 'en' => 'Hello %username%');
        $translations['admin.head.menu']                 = array('fr' => 'Menu', 'en' => 'Menu');
        $translations['admin.head.site']                 = array('fr' => 'Site', 'en' => 'Site');
        $translations['admin.head.notification.title']   = array('fr' => 'Notifications', 'en' => 'Notifications');
        $translations['admin.head.create.title']         = array('fr' => 'Créer', 'en' => 'Create');
        $translations['admin.menu.dashboard']            = array('fr' => 'Tableau de bord', 'en' => 'Dashboard');
        $translations['admin.menu.menus']                = array('fr' => 'Menus', 'en' => 'Menus');
        $translations['admin.menu.locales']              = array('fr' => 'Langues & Traductions', 'en' => 'Languages & Translations');
        $translations['admin.menu.users']                = array('fr' => 'Utilisateurs', 'en' => 'Users');
        $translations['admin.menu.options']              = array('fr' => 'Réglages', 'en' => 'Options');
        $translations['admin.breadcrumb.site']           = array('fr' => 'Site', 'en' => 'Site');
        $translations['admin.breadcrumb.administration'] = array('fr' => 'Administration', 'en' => 'Administration');
        $translations['admin.breadcrumb.dashboard']      = array('fr' => 'Tableau de bord', 'en' => 'Dashboard');

        /* Pays */
        $translations['admin.flags.choice.br'] = array('fr' => 'Brésil', 'en' => 'Brazil');
        $translations['admin.flags.choice.cl'] = array('fr' => 'Chili', 'en' => 'Chile');
        $translations['admin.flags.choice.co'] = array('fr' => 'Colombie', 'en' => 'Colombia');
        $translations['admin.flags.choice.cu'] = array('fr' => 'Cuba', 'en' => 'Cuba');
        $translations['admin.flags.choice.do'] = array('fr' => 'République Dominicaine', 'en' => 'Dominican Republic');
        $translations['admin.flags.choice.ec'] = array('fr' => 'Equateur', 'en' => 'Ecuador');
        $translations['admin.flags.choice.es'] = array('fr' => 'Espagne', 'en' => 'Spain');
        $translations['admin.flags.choice.fr'] = array('fr' => 'France', 'en' => 'France');
        $translations['admin.flags.choice.gb'] = array('fr' => 'Royaume-Uni', 'en' => 'United Kingdom');
        $translations['admin.flags.choice.it'] = array('fr' => 'Italie', 'en' => 'Italy');
        $translations['admin.flags.choice.lv'] = array('fr' => 'Lettonie', 'en' => 'Latvia');
        $translations['admin.flags.choice.mx'] = array('fr' => 'Mexique', 'en' => 'Mexico');
        $translations['admin.flags.choice.pt'] = array('fr' => 'Portugal', 'en' => 'Portugal');
        $translations['admin.flags.choice.ro'] = array('fr' => 'Roumanie', 'en' => 'Romania');
        $translations['admin.flags.choice.us'] = array('fr' => 'Etats-Unis', 'en' => 'United States');

        /* Menus > index */
        $translations['admin.menus.title']                         = array('fr' => 'Menus', 'en' => 'Menus');
        $translations['admin.menus.breadcrumb']                    = array('fr' => 'Menus', 'en' => 'Menus');
        $translations['admin.menus.index.actions.add']             = array('fr' => 'Ajouter', 'en' => 'Add');
        $translations['admin.menus.index.table.head.id']           = array('fr' => '#', 'en' => '#');
        $translations['admin.menus.index.table.head.name']         = array('fr' => 'Nom du groupe', 'en' => 'Group Name');
        $translations['admin.menus.index.table.head.actions']      = array('fr' => '¤', 'en' => '¤');
        $translations['admin.menus.index.table.body.actions.view'] = array('fr' => 'Voir', 'en' => 'See');
        $translations['admin.menus.index.table.body.actions.edit'] = array('fr' => 'Editer', 'en' => 'Edit');

        /* Menus > formulaire */
        $translations['admin.menus.formmenu.title.add']       = array('fr' => 'Ajout d\'un groupe de menu', 'en' => 'Adding a menu group');
        $translations['admin.menus.formmenu.title.edit']      = array('fr' => 'Edition du groupe de menu #%groupid% "%groupname%"', 'en' => 'Edition of menu group #%groupid% "%groupname%"');
        $translations['admin.menus.formmenu.breadcrumb.add']  = array('fr' => 'Ajouter', 'en' => 'Add');
        $translations['admin.menus.formmenu.breadcrumb.edit'] = array('fr' => 'Editer', 'en' => 'Edit');
        $translations['admin.menus.formmenu.form.name']       = array('fr' => 'Nom', 'en' => 'Name');
        $translations['admin.menus.formmenu.form.cancel']     = array('fr' => 'Annuler', 'en' => 'Cancel');
        $translations['admin.menus.formmenu.form.submit']     = array('fr' => 'Enregistrer', 'en' => 'Save');

        /* Menus > elements */
        $translations['admin.menus.group.title']                   = array('fr' => 'Menu %groupid% "%groupname%"', 'en' => 'Menu %groupid% "%groupname%"');
        $translations['admin.menus.group.actions.group.title']     = array('fr' => 'Groupe', 'en' => 'Groupe');
        $translations['admin.menus.group.actions.group.edit']      = array('fr' => 'Editer', 'en' => 'Edit');
        $translations['admin.menus.group.actions.group.organize']  = array('fr' => 'Organiser', 'en' => 'Organize');
        $translations['admin.menus.group.actions.element.title']   = array('fr' => 'Élément', 'en' => 'Elements');
        $translations['admin.menus.group.actions.element.add']     = array('fr' => 'Ajouter', 'en' => 'Add');
        $translations['admin.menus.group.table.head.id']           = array('fr' => '#', 'en' => '#');
        $translations['admin.menus.group.table.head.code']         = array('fr' => 'Code', 'en' => 'Code');
        $translations['admin.menus.group.table.head.message']      = array('fr' => 'Message', 'en' => 'Message');
        $translations['admin.menus.group.table.head.icon']         = array('fr' => 'Icône', 'en' => 'Icon');
        $translations['admin.menus.group.table.head.route']        = array('fr' => 'Route', 'en' => 'Route');
        $translations['admin.menus.group.table.head.position']     = array('fr' => 'Position', 'en' => 'Position');
        $translations['admin.menus.group.table.head.actions']      = array('fr' => '¤', 'en' => '¤');
        $translations['admin.menus.group.table.body.actions.view'] = array('fr' => 'Voir', 'en' => 'See');
        $translations['admin.menus.group.table.body.actions.edit'] = array('fr' => 'Editer', 'en' => 'Edit');
        $translations['admin.menus.group.table.body.actions.up']   = array('fr' => 'Monter', 'en' => 'Up');
        $translations['admin.menus.group.table.body.actions.down'] = array('fr' => 'Descendre', 'en' => 'Down');

        LocaleQuery::addTranslations($translations);
    }

    protected function generateAdminLessFile()
    {
        /* Chemin du fichier */
        $adminLessFile = realpath($this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'admin.less';

        /* Contenu du fichier */
        $adminLessContent = '@import "' . realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'less') . DIRECTORY_SEPARATOR . 'admin.less' . '";' . PHP_EOL;

        /* Enregistrement du fichier */
        file_put_contents($adminLessFile, $adminLessContent);
    }

    protected function generatePackageJsonFile()
    {
        /*
{
  "name": "www.iimedias.com",
  "version": "0.0.0",
  "description": "Développement de tous les modules iiMedias",
  "main": "index.js",
  "directories": {
    "test": "tests"
  },
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git@gitlab.iimedias.com:iimedias/all.git"
  },
  "author": "",
  "license": "BSD-2-Clause",
  "dependencies": {
    "colors": "^1.1.2",
    "express": "^4.14.0",
    "grunt": "^0.4.5",
    "grunt-concat-css": "~0.3.1",
    "grunt-contrib-concat": "~1.0.0",
    "grunt-contrib-copy": "~1.0.0",
    "grunt-contrib-cssmin": "~1.0.1",
    "grunt-contrib-jshint": "~1.0.0",
    "grunt-contrib-less": "~1.2.0",
    "grunt-contrib-uglify": "~1.0.1",
    "grunt-contrib-watch": "~1.0.0",
    "http": "0.0.0",
    "irc": "~0.4.1",
    "jsonfile": "^2.3.0",
    "load-grunt-tasks": "~3.4.1",
    "socket.io": "^1.4.6"
  }
}
     */
        /* Chemin du fichier */
        $packageJsonFile = realpath($this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'package.json';

        /* Récupération du fichier bower.json actuel */
        try {
            $packageJsonActualArray = json_decode(file_get_contents($packageJsonFile), true);
        }
        catch (Exception $e) {
            $packageJsonActualArray = array();
        }

        $packageJsonDefaultArray = array(
            'name'         => 'www.iimedias.com',
            'version'      => '0.0.0',
            'description'  => 'Développement de tous les modules iiMedias',
            'main'         => 'index.js',
            'directories'  => array(
                'test' => 'tests',
            ),
            'scripts'      => array(
                'test' => 'echo \"Error: no test specified\" && exit 1',
            ),
            'repository'   => array(
                'type' => 'git',
                'url'  => 'git@gitlab.iimedias.com:iimedias/all.git',
            ),
            'author'       => array(
                'sebii <sebii@sebiiheckel.fr>',
            ),
            'liscense'     => 'BDS-2-Clause',
            'dependencies' => array(
                'colors'               => '^1.1.2',
                'express'              => '^4.15.0',
                'grunt'                => '^1.0.0',
                'grunt-concat-css'     => '^0.3.0',
                'grunt-contrib-concat' => '^1.0.0',
                'grunt-contrib-copy'   => '^1.0.0',
                'grunt-contrib-cssmin' => '^2.0.0',
                'grunt-contrib-jshint' => '^1.0.0',
                'grunt-contrib-less'   => '^1.4.0',
                'grunt-contrib-uglify' => '^2.2.0',
                'grunt-contrib-watch'  => '^1.0.0',
                'http'                 => '0.0.0',
                'irc'                  => '^0.5.0',
                'jsonfile'             => '^2.4.0',
                'load-grunt-tasks'     => '~3.5.0',
                'socket.io'            => '^1.7.0',
            ),
        );

        /* Fusion des valeurs finales de package.json */
        $packageJsonArray                 = array();
        $packageJsonArray['name']         = strtolower(
            (isset($packageJsonActualArray['name']) && trim($packageJsonActualArray['name']) != '') ? $packageJsonActualArray['name'] :
                ($this->getContainer()->hasParameter('app_name') ? $this->getContainer()->getParameter('app_name') : $packageJsonDefaultArray['name'])
        );
        $packageJsonArray['version']      = (isset($packageJsonActualArray['version']) && trim($packageJsonActualArray['version']) != '') ? $packageJsonActualArray['version'] :
            ($this->getContainer()->hasParameter('app_version') ? $this->getContainer()->getParameter('app_version') : $packageJsonDefaultArray['version']);
        $packageJsonArray['description']  = (isset($packageJsonActualArray['description']) && trim($packageJsonActualArray['description']) != '') ? $packageJsonActualArray['description'] :
            ($this->getContainer()->hasParameter('app_description') ? $this->getContainer()->getParameter('app_description') : $packageJsonDefaultArray['description']);
        $packageJsonArray['main']         = (isset($packageJsonActualArray['main']) && trim($packageJsonActualArray['main']) != '') ? $packageJsonActualArray['main'] : $packageJsonDefaultArray['main'];
        $packageJsonArray['authors']      = array();
        $packageJsonArray['liscense']     = (isset($packageJsonActualArray['liscense']) && trim($packageJsonActualArray['liscense']) != '') ? $packageJsonActualArray['liscense'] :
            ($this->getContainer()->hasParameter('app_liscense') ? $this->getContainer()->getParameter('app_liscense') : $packageJsonActualArray['liscense']);
        $packageJsonArray['dependancies'] = array();


        foreach ($packageJsonDefaultArray['authors'] as $authorKey => $authorValue) {
            $bowerJsonArray['authors'][$authorKey] = $authorValue;
        }
        foreach ($packageJsonActualArray['authors'] as $authorKey => $authorValue) {
            $packageJsonArray['authors'][$authorKey] = $authorValue;
        }
        foreach ($packageJsonDefaultArray['dependencies'] as $dependencyKey => $dependencyValue) {
            $packageJsonArray['dependencies'][$dependencyKey] = $dependencyValue;
        }
        foreach ($packageJsonActualArray['dependencies'] as $dependencyKey => $dependencyValue) {
            $packageJsonArray['dependencies'][$dependencyKey] = $dependencyValue;
        }
    }

    protected function generateBowerJsonFile()
    {
        /* Chemin du fichier */
        $bowerJsonFile = realpath($this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'bower.json';

        /* Récupération du fichier bower.json actuel */
        try {
            $bowerJsonActualArray = json_decode(file_get_contents($bowerJsonFile), true);
        }
        catch (Exception $e) {
            $bowerJsonActualArray = array();
        }

        /* Valeurs par défault de bower.json */
        $bowerJsonDefaultArray = array(
            'name'         => 'iiMedias',
            'description'  => 'Un site Symfony utilisant les bundles iiMedias',
            'main'         => 'index.js',
            'authors'      => array(
                'sebii <sebii@sebiiheckel.fr>',
            ),
            'liscense'     => 'BDS-2-Clause',
            'homepage'     => '',
            'ignore'       => array(
                '**/*',
                'node_modules',
                'bower_components',
                'web/bower',
                'test',
                'tests',
            ),
            'dependencies' => array(
                'jquery-ui'               => '^1.12.1',
                'bootstrap'               => '^3.3.6',
                'datatables.net'          => '^1.10.12',
                'datatables.net-bs'       => '^1.10.12',
                'components-font-awesome' => '^4.7.0',
                'fatcow-icons'            => '^1.0.4',
                'flag-icon-css'           => '^2.7.0',
                'handlebars'              => '^4.0.5',
                'select2'                 => '^4.0.3',
                'hammerjs'                => '^2.0.8',
                'pnotify'                 => '^3.0.0',
            ),
        );

        /* Fusion des valeurs finales de bower.json */
        $bowerJsonArray                 = array();
        $bowerJsonArray['name']         = strtolower(
                                            (isset($bowerJsonActualArray['name']) && trim($bowerJsonActualArray['name']) != '') ? $bowerJsonActualArray['name'] :
                                            ($this->getContainer()->hasParameter('app_name') ? $this->getContainer()->getParameter('app_name') : $bowerJsonDefaultArray['name'])
        );
        $bowerJsonArray['description']  = (isset($bowerJsonActualArray['description']) && trim($bowerJsonActualArray['description']) != '') ? $bowerJsonActualArray['description'] :
                                            ($this->getContainer()->hasParameter('app_description') ? $this->getContainer()->getParameter('app_description') : $bowerJsonDefaultArray['description']);
        $bowerJsonArray['main']         = (isset($bowerJsonActualArray['main']) && trim($bowerJsonActualArray['main']) != '') ? $bowerJsonActualArray['main'] : $bowerJsonDefaultArray['main'];
        $bowerJsonArray['authors']      = array();
        $bowerJsonArray['liscense']     = (isset($bowerJsonActualArray['liscense']) && trim($bowerJsonActualArray['liscense']) != '') ? $bowerJsonActualArray['liscense'] :
                                            ($this->getContainer()->hasParameter('app_liscense') ? $this->getContainer()->getParameter('app_liscense') : $bowerJsonDefaultArray['liscense']);
        $bowerJsonArray['homepage']     = (isset($bowerJsonActualArray['homepage']) && trim($bowerJsonActualArray['homepage']) != '') ? $bowerJsonActualArray['homepage'] : $bowerJsonDefaultArray['homepage'];
        $bowerJsonArray['ignore']       = array();
        $bowerJsonArray['dependencies'] = array();
        foreach ($bowerJsonDefaultArray['authors'] as $authorKey => $authorValue) {
            $bowerJsonArray['authors'][$authorKey] = $authorValue;
        }
        foreach ($bowerJsonActualArray['authors'] as $authorKey => $authorValue) {
            $bowerJsonArray['authors'][$authorKey] = $authorValue;
        }
        foreach ($bowerJsonDefaultArray['ignore'] as $ignoreKey => $ignoreValue) {
            $bowerJsonArray['ignore'][$ignoreKey] = $ignoreValue;
        }
        foreach ($bowerJsonActualArray['ignore'] as $ignoreKey => $ignoreValue) {
            $bowerJsonArray['ignore'][$ignoreKey] = $ignoreValue;
        }
        foreach ($bowerJsonDefaultArray['dependencies'] as $dependencyKey => $dependencyValue) {
            $bowerJsonArray['dependencies'][$dependencyKey] = $dependencyValue;
        }
        foreach ($bowerJsonActualArray['dependencies'] as $dependencyKey => $dependencyValue) {
            $bowerJsonArray['dependencies'][$dependencyKey] = $dependencyValue;
        }

        /* Enregistrement du fichier */
        $bowerJsonString = json_encode($bowerJsonArray, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        file_put_contents($bowerJsonFile, $bowerJsonString);
    }

    protected function generateBowerRcFile()
    {
        /* Chemin du fichier */
        $bowerRcFile = realpath($this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . '.bowerrc';

        /* Contenu du fichier */
        $bowerRcArray = array(
            'directory' => 'web/bower',
        );

        /* Enregistrement du fichier */
        $bowerRcString = json_encode($bowerRcArray, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        file_put_contents($bowerRcFile, $bowerRcString);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getAllSubConfigCmd();
        $this->setLocales();
        $this->generateAdminLessFile();
        $this->generateBowerRcFile();
        $this->generateBowerJsonFile();
        $this->generatePackageJsonFile();
        exit();

        MenuElementQuery::addElement('admin', 'dashboard', 'admin.menu.dashboard', 'tachometer', 'iimedias_admin_admin_index', array());

        for ($iMatchesList = 0, $countMatchesList = count($matchesList[0]); $iMatchesList < $countMatchesList; $iMatchesList++) {
            $commandGetConfig = $this->getApplication()->find(trim($matchesList[0][$iMatchesList]));
            $inputGetConfig   = new ArrayInput(array());
            $outputGetConfig  = new BufferedOutput();
            $commandGetConfig->run($inputGetConfig, $outputGetConfig);
        }

        MenuElementQuery::addElement('admin', 'menus', 'admin.menu.menus', 'bars', 'iimedias_admin_admin_index', array());
        MenuElementQuery::addElement('admin', 'locales', 'admin.menu.locales', 'globe', 'iimedias_admin_locale_index', array());
        MenuElementQuery::addElement('admin', 'users', 'admin.menu.users', 'user-circle-o', 'iimedias_admin_admin_index', array());
        MenuElementQuery::addElement('admin', 'options', 'admin.menu.options', 'cog', 'iimedias_admin_admin_optionsmain', array());

        $commandExportToAdminTranslation = $this->getApplication()->find('translations:exporttoadmin');
        $inputExportToAdminTranslation   = new ArrayInput(array());
        $outputExportToAdminTranslation  = new BufferedOutput();
        $commandExportToAdminTranslation->run($inputExportToAdminTranslation, $outputExportToAdminTranslation);
    }

}
