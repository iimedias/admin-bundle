<?php

namespace IiMedias\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class NodeJsGetConfigCommand extends ContainerAwareCommand
{
    private $_libs;
    private $_nodeLibs;
    private $_output;
    private $_outputNodeJs;

    protected function configure()
    {
        $this
            ->setName('nodejs:getconfig')
            ->setDescription('Récupère la configuration pour le serveur nodejs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_output = $output;

        $config = [
            'http'            => [
                'port' => 8080,
            ],
            'sms'             => [
                'free' => [
                    'serviceUrl' => 'https://smsapi.free-mobile.fr/sendmsg',
                    'user'       => '29936672',
                    'pass'       => '6V3n4eUomFJk3O',
                ],
            ],
            'sockets'         => [],
            'ioServerClients' => [],
            'vars'            => [],
        ];

        /* Commandes */
        $commandList  = $this->getApplication()->find('list');
        $inputList    = new ArrayInput(array());
        $outputList   = new BufferedOutput();
        $commandList->run($inputList, $outputList);
        preg_match_all('# nodejs:([a-zA-Z0-9]*):getconfig #', $outputList->fetch(), $matchesList);

        for ($iMatchesList = 0, $countMatchesList = count($matchesList[0]); $iMatchesList < $countMatchesList; $iMatchesList++) {
            $commandGetConfig = $this->getApplication()->find(trim($matchesList[0][$iMatchesList]));
            $inputGetConfig   = new ArrayInput(array());
            $outputGetConfig  = new BufferedOutput();
            $commandGetConfig->run($inputGetConfig, $outputGetConfig);
            $extNodeJsConfig  = json_decode($outputGetConfig->fetch());
            $config[$matchesList[1][$iMatchesList]] = $extNodeJsConfig;
        }
        $output->write(json_encode($config));
    }
}
